from ConvAE_3C import *
from timeit import default_timer as timer
from sklearn import preprocessing,datasets
from sklearn.metrics import precision_recall_fscore_support, accuracy_score,confusion_matrix
import mine_synthetic_abs
#from theano.compile.nanguardmode import NanGuardMode

train_steps = 150
ALP_INI = 0.0005 #learning rate
r_rate = 0.000000001 #regularization strength
S = [(2,2),(2,2),(2,2),(2,2)] #stride for filtering
P = ['full','full','full','full'] #padding size
pool_size = [(2,2),(2,2),(2,2),(2,2)]
filter = [(8,3,3,3),(8,8,3,3),(16,8,3,3),(8,16,3,3)]
de_filter = [(3,8,3,3),(8,8,3,3),(8,16,3,3),(16,8,3,3)]
#filter = [(3,6,3,3),(3,3,3,3),(3,3,3,3),(2,3,3,3)]
#de_filter = [(6,3,3,3),(3,3,3,3),(3,3,3,3),(3,2,3,3)]
image_size = (64,64)
n_class = 3
mlp_extra_feas = 4
LABELS = [0,1,2]
UP_TRAIN=100
lamda = 0.2

def ometrics(y_true, y_pred):
    y_true_ravel = y_true.ravel()
    y_pred_ravel = y_pred.ravel()
    cm = confusion_matrix(y_true_ravel, y_pred_ravel,labels=LABELS)
    #cm1 = [[x for x in reversed(r)] for r in cm]
    for c in cm:
        print(c)
    precision, recall, f_measure, support= precision_recall_fscore_support(y_true_ravel, y_pred_ravel, labels = LABELS)
    accuracy  = accuracy_score(y_true_ravel, y_pred_ravel)
    print("pecision:", ' '.join(['%.3f ' %(k) for k in precision]))
    print("recall:", ' '.join(['%.3f ' %(k) for k in recall]))
    print("f_measure:", ' '.join(['%.3f ' %(k) for k in f_measure]))
    print("accuracy: %.3f" %(accuracy,))

def save_reconstruction_output(output):
    for _,im in enumerate(output):
        cv2.imwrite("recon_output/recon_im"+str(_)+".png",im)
        
#w = (w-f+2*p)/s + 1
def run_cnn(train_x, train_y, valid_x, valid_y, test_x, m_params):
    rng = np.random
    #compute the number of the units for each layer
    #define theano variables
    x = T.tensor4(name = 'x', dtype = theano.config.floatX)
    y = T.ltensor3(name = 'y')
    alp = T.fscalar(name = 'alp')
    n_in = (train_x.shape[1],train_x.shape[2],train_x.shape[3])
    #define convae
    #print("x.shape,filter[0].shape:",train_x.shape,filter[0],x.shape)
    cae = ConAE(input = x,
                y = y,
                filter_shape = filter,
                de_filter_shape = de_filter,
                S = S,
                P = P,
                n_in = n_in,
                n_out = n_class,
                rng = rng,
                pool_size = pool_size
                )
    #loss function
    cost = cae.neg_log_likelihood +  lamda*cae.square_errors + r_rate* cae.L2_reg
    #cost = cae.square_errors
    params = cae.params
    #gradient decent
    gparams = T.grad(cost,params)
    updates = [(param, param - alp * gparam) for param, gparam in zip(params, gparams)]
    shape_func = theano.function(inputs=[x], outputs=[cae.caeout1,cae.caeout2,cae.caeout3,cae.caeout4,\
                                                      cae.decaeout1,cae.decaeout2,cae.decaeout3,cae.decaeout4])
    train_func = theano.function(inputs = [x, y, alp],
                                 #outputs = [cost,cae.errors,cae.y_pred,input_shape,y_p_1d_shape,y_p_shape,y_pred_shape],
                                 outputs = [cost,cae.errors,cae.y_pred],
                                 updates = updates,
                                 #mode=NanGuardMode(nan_is_error=True, inf_is_error=True, big_is_error=True)
                                 )
    validation_func = theano.function(inputs = [x,y],
                                  outputs = [cae.y_pred, cae.errors,cost]
                                  )
    predict_func = theano.function(inputs = [x],
                                  outputs = [cae.y_pred,cae.y_p]
                                  )
    #train and predict
    #batch_size = train_x.shape[0]
    batch_size = 20
    m = train_x.shape[0]
    nb = int(m/batch_size)
    lf = m%batch_size
    h_iter = int(train_steps/2)
    t_h = h_iter
    pow = 2
    n = 0
    alpha = np.float32(ALP_INI)
    y_pred_train = []
    scores = []
    errors = []
    y_preds = []
    y_v_errs = []
    y_err_min = 10.0
    y_preds_best = []
    recon_output = []
    org_y_ravel = []
    pred_y_ravel = []
    pp = 0
    rpar_flag = 0
    #clear history params
    if sys.argv[2] != '1':
        fs = glob.glob("params/*")
        for f in fs:
            remove(f)
    if sys.argv[2] != '1':
        for i in range(train_steps):
            y_pred_train = []
            if i == t_h and n < 1:
                #alpha = alpha - np.float32(0.002)
                alpha = alpha/2
                h_iter = h_iter/pow
                t_h = t_h + h_iter
                #t_h = t_h + 100
                n += 1
            #alpha = alpha - np.float32(0.0001)
            sum = 0
            sum_err = 0
            for j in range(nb):
                #cnnout1,cnnout2,cnnout3,cnnout4,decnnout1,decnnout2,decnnout3,decnnout4 = shape_func(train_x[j*batch_size:(j+1)*batch_size])
                #print(decnnout1.shape,decnnout2.shape,decnnout3.shape,decnnout4.shape)
                c,err,y_pred_b = train_func(train_x[j*batch_size:(j+1)*batch_size],train_y[j*batch_size:(j+1)*batch_size], alpha)
                #print("input_shape,y_p_1d_shape,y_p_shape,y_pred_shape:",input_shape,y_p_1d_shape,y_p_shape,y_pred_shape)
                #recon_output.extend(recon_op)
                sum += c
                sum_err += err
                y_pred_train.extend(y_pred_b)
            print("after train1")
            #for the last batch
            if len(y_pred_train) < len(train_y):
                #c,err,y_pred_b,recon_op= train_func(train_x[len(y_pred_train):],train_y[len(y_pred_train):], alpha)
                c,err,y_pred_b = train_func(train_x[len(y_pred_train):],train_y[len(y_pred_train):], alpha)
                sum += c
                sum_err += err
                y_pred_train.extend(y_pred_b)
                #recon_output.extend(recon_op)
            #print("after train")
            sum /= nb
            sum_err /= nb       
            print("%.5f,%.5f"%(sum,sum_err))
            scores.append(sum)
            errors.append(sum_err)
            #valid change
            y_v_errs[:] = []
            #print("before validation")
            y_pred_v,y_err_v,c_v = validation_func(valid_x,valid_y)
            #print("before acc")
            """acc = accuracy_score(np.array(valid_y).ravel(), np.array(y_pred_v).ravel())
            print("after acc %.5f" %(acc))
            print("combine valid error: %.5f" %(1-acc))"""
            print("combine valid error: %.5f" %(y_err_v))
            print("train_step:",i)
            if y_err_v <= y_err_min:
                y_err_min = y_err_v
                y_v_preds_best = y_pred_v
                print("y_err_min",y_err_min)
                r_params = ([cae.cnlayer1.W.get_value(), cae.cnlayer2.W.get_value(), cae.cnlayer3.W.get_value(),cae.cnlayer4.W.get_value(),
                             cae.decnlayer4.W.get_value(),cae.decnlayer3.W.get_value(), cae.decnlayer2.W.get_value(),cae.decnlayer1.W.get_value(), cae.lrlayer.W.get_value()],
                            [cae.cnlayer1.b.get_value(), cae.cnlayer2.b.get_value(), cae.cnlayer3.b.get_value(), cae.cnlayer4.b.get_value(),
                             cae.decnlayer4.b.get_value(), cae.decnlayer3.b.get_value(), cae.decnlayer2.b.get_value(),cae.decnlayer1.b.get_value(), cae.lrlayer.b.get_value()])
                rpar_flag = 1
                if y_err_min < 0.12:
                    fn = "params/param" + str(pp) + ".pkl"
                    f = open(fn,"wb")
                    pickle.dump(r_params,f)
                pp += 1

                if y_err_min < 0.01 and sum_err < 0.001:
                    break
            #print("end of each epoch")
            
        #print the confusion matrix for training procudure
        #save_reconstruction_output(recon_output[:100])
        print("the confusion matrix of traning results is: \n")
        last = len(y_pred_train)
        ometrics(train_y, np.array(y_pred_train))
        print("the combined confusion matrix of validation data is: \n")
        ometrics(valid_y,np.array(y_v_preds_best))
        #print(len(valid_y),np.array(ims_syn_test_org_pieces).shape)
        if len(y_v_preds_best) == 0:
            print("sum_err > 0.01, return")
            return 
        else:
            show_visualized_results(y_v_preds_best, ims_syn_test_org_pieces, valid_org_img_xy, 0, len(y_v_preds_best), 'results_syn')
            print("show valid results done")
    else:
        (W1,b1) = m_params
        W11, W12, W13, W14, W21, W22, W23, W24, W31 = W1
        b11, b12, b13, b14, b21, b22, b23, b24, b31= b1
        cae.cnlayer1.W.set_value(W11)
        cae.cnlayer1.b.set_value(b11)
        cae.cnlayer2.W.set_value(W12)
        cae.cnlayer2.b.set_value(b12)
        cae.cnlayer3.W.set_value(W13)
        cae.cnlayer3.b.set_value(b13)
        cae.cnlayer4.W.set_value(W14)
        cae.cnlayer4.b.set_value(b14)
        cae.decnlayer4.W.set_value(W21)
        cae.decnlayer4.b.set_value(b21)
        cae.decnlayer3.W.set_value(W22)
        cae.decnlayer3.b.set_value(b22)
        cae.decnlayer2.W.set_value(W23)
        cae.decnlayer2.b.set_value(b23)
        cae.decnlayer1.W.set_value(W24)
        cae.decnlayer1.b.set_value(b24)
        cae.lrlayer.W.set_value(W31)
        cae.lrlayer.b.set_value(b31)

    #testing on multiple difference images
    y_preds = []
    y_probs = []
    if sys.argv[2] != '1' and rpar_flag == 1:
        (W1,b1) = r_params
        W11, W12, W13, W14, W21, W22, W23, W24, W31 = W1
        b11, b12, b13, b14, b21, b22, b23, b24, b31= b1
        cae.cnlayer1.W.set_value(W11)
        cae.cnlayer1.b.set_value(b11)
        cae.cnlayer2.W.set_value(W12)
        cae.cnlayer2.b.set_value(b12)
        cae.cnlayer3.W.set_value(W13)
        cae.cnlayer3.b.set_value(b13)
        cae.cnlayer4.W.set_value(W14)
        cae.cnlayer4.b.set_value(b14)
        cae.decnlayer4.W.set_value(W21)
        cae.decnlayer4.b.set_value(b21)
        cae.decnlayer3.W.set_value(W22)
        cae.decnlayer3.b.set_value(b22)
        cae.decnlayer2.W.set_value(W23)
        cae.decnlayer2.b.set_value(b23)
        cae.decnlayer1.W.set_value(W24)
        cae.decnlayer1.b.set_value(b24)
        cae.lrlayer.W.set_value(W31)
        cae.lrlayer.b.set_value(b31)
    #print(np.array(mine_org_pieces[ISIZE_BEGIN:ISIZE_END]).shape)
    y_pred_tt = []
    y_p_tt = []
    dis_size = 300
    for k in range(0,len(test_x),dis_size):
        dis_end  = k + dis_size
        if dis_end >= len(test_x):
            continue
        y_pred_t, y_p_t = predict_func(test_x[k:dis_end])
        y_pred_tt.extend(y_pred_t)
        y_p_tt.extend(y_p_t)
    if len(y_pred_tt) < len(test_x):
        y_pred_t, y_p_t = predict_func(test_x[len(y_pred_tt):])
        y_pred_tt.extend(y_pred_t)
        y_p_tt.extend(y_p_t)
    #y_pred_tt,y_p_tt = predict_func(test_x)
    
        
    """f = open("prob.txt","w")
    for probs in y_probs[-1]:
        f.write(' '.join(["%.5f"%(x) for x in probs])+'\n')"""
    if sys.argv[2] != '1':
        return np.array(y_pred_tt),r_params,scores,errors
    else:
        return np.array(y_preds_tt),[],[],[]

def show_performance(scores, errors):
    print(len(scores),len(errors))
    accs = [1-x for x in errors]
    fig = plt.figure()
    plt.plot(scores)
    plt.xlabel('training step')
    plt.ylabel('loss value')
    fn = "figures_mine/scores.png"
    fig.savefig(fn)
    plt.close(fig)
    fig = plt.figure()
    plt.plot(accs)
    plt.xlabel('training step')
    plt.ylabel('accuracy')
    fn = "figures_mine/accuracy.png"
    fig.savefig(fn)
    plt.close(fig)
    
def save_results(scores, errors):
    accs = [1-x for x in errors]
    f = open("scores.pkl", "wb")
    pickle.dump(scores,f)
    f.close()
    f = open("accuracy.pkl", "wb")
    pickle.dump(accs,f)
    f.close()
    
if __name__ == '__main__':
    #start time
    start = timer()

    #load data
    datasets = load_mine_data()
    #train dataset
    train_X,train_Y = datasets[0]

    #valid data
    valid_X,valid_Y = datasets[1]
    
    #test data
    test_X = datasets[2]
        
    
    #reshape
    train_X = train_X.transpose(0,-1, 1, 2).astype(theano.config.floatX)
    valid_X = valid_X.transpose(0,-1,1,2).astype(theano.config.floatX)
    test_X = test_X.transpose(0,-1,1,2).astype(theano.config.floatX)
    
    print("train_X transpose shape:",train_X.shape)
    print("valid_X traonspose shape:", valid_X.shape)
    print("test_X transpose shape:", test_X.shape)
    #print("valid dist feas:",valid_dist_X.shape)
    #load params
    m_params = []
    if sys.argv[2] == '1':
        m_params = load_params()
    
    y_preds,m_params,scores, errors = run_cnn(train_X, train_Y, valid_X, valid_Y, test_X,  m_params)
    #y_preds = np.array(y_preds,np.int32)

    #save the results
    if len(y_preds) != 0:
        show_visualized_results(y_preds,mine_org_pieces,test_org_img_xy,0,len(y_preds))
    if sys.argv[2] != '1' and len(y_preds) != 0:
        save_params(m_params)
        save_results(scores,errors)
    #show_performance(scores, errors)
    #end time
    end = timer()
    print("the elapsed time is: %.2f s" %(end - start,))



    
    
