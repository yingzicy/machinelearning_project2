import numpy as np
import theano
import theano.tensor as T
from sklearn import datasets,cross_validation,preprocessing
from utils import *

class LogisticRegression(object):
    
    def __init__(self, input, n_in, n_out, rng):
        """initialize params for LR
        input: input data batch 
        n_in: the number of features
        n_out: the number of classes
        """
        W_value = np.asarray(
                rng.uniform(
                    low = -np.sqrt(6. / (n_in[0] + n_out)),
                    high = np.sqrt(6. / (n_in[0] + n_out)),
                    size = (n_in[0], n_out)
                    #size = (n_out, n_in[0]*n_in[1]*n_in[2])
                    ),
                dtype = theano.config.floatX
                )
        b_value = np.asarray(
                rng.uniform(
                    low = -np.sqrt(6. / (n_in[0] + n_out)),
                    high = np.sqrt(6. / (n_in[0] + n_out)),
                    size = (n_out)
                    #size = (n_out,n_in[1],n_in[2])
                    ),
                dtype = theano.config.floatX
                )
        self.W = theano.shared(value = W_value, name = 'W', borrow = True)
        self.b = theano.shared(value = b_value, name = 'b', borrow = True)
        #combine the params W and b
        self.params = [self.W, self.b]
        input_2d = T.reshape(input,(input.shape[0]*input.shape[1]*input.shape[2],input.shape[3]))
        self.y_p_1d = T.nnet.softmax(T.dot(input_2d,self.W) + self.b)
        self.y_p = T.reshape(self.y_p_1d, (input.shape[0],input.shape[1],input.shape[2],n_out))
        self.y_pred = T.argmax(self.y_p, axis = 3)
        self.L2_reg = (self.W **2).sum() + (self.b **2).sum()
        self.input = input
        self.input_shape = input.shape
        self.y_p_1d_shape = self.y_p_1d.shape
        self.y_p_shape = self.y_p.shape
        self.y_pred_shape = self.y_pred.shape

    def negative_log_likelihood(self, y_2d):
        """ compute the mean of the negative_log_likelihood(lost function)
        """
        #y_1d = T.reshape(y_2d,(y_2d.shape[0]*y_2d.shape[1]*y_2d.shape[2]))
        y_1d = y_2d.flatten(1)
        return -T.mean(T.log(self.y_p_1d)[T.arange(y_1d.shape[0]),y_1d])

    def square_errors(self, X):
        eps = 1e-9
        X_shape1 = (X.shape[0]*X.shape[2]*X.shape[3],X.shape[1])
        Y = T.reshape(X, X_shape1)
        Y1 = T.reshape(self.input,X_shape1)
        return T.mean(T.sqrt(T.maximum(T.sum(T.sqr(Y-Y1),axis=1),eps)))
    
    def errors(self, y):
        """compute the accuracy of the testing funciton
        """
        #check the shape
        if y.ndim != self.y_pred.ndim:
            raise TypeError('y shape should be the same as self.y_pred')
        return T.mean(T.neq(self.y_pred, y))
