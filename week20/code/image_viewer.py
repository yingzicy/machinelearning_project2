import numpy as np
import os, sys
import pickle
import cv2
from os import listdir
from os.path import join,isfile
from tkinter import *
from tkinter import messagebox
from PIL import Image, ImageTk

W = (64,64)
img_list = []
current = 0
img_list_info = []
B_SIZE = 1

def show_position(x, y):
    mousePosition = StringVar()
    mousePosition.set( "("+str(x)+","+str(y)+")")

def comp_positions(x, y):
    im_shape = img_list_info[current]["img_shape"]
    slice_pos = (int(x/W[0]),int(y/W[1]))
    print("slice_pos",slice_pos)
    label_pos = slice_pos[0]*im_shape[0] + slice_pos[1]
    return slice_pos,label_pos

def chg_slice_content(x, y, type):
    BLUE = [255,0,0,255]
    RED = [0,0,255,255]
    im = cv2.imread(img_list[current][0],cv2.IMREAD_UNCHANGED)
    t_s = x - x%W[0]
    l_s = y - y%W[1]
    b_s = t_s + W[0]
    r_s = l_s + W[1]
    t = t_s + B_SIZE
    b = b_s - B_SIZE
    l = l_s +  B_SIZE
    r = r_s - B_SIZE
    #add border for each piece
    if type == 0:
        im_core = im[t:b,l:r,:]
        im_piece = cv2.copyMakeBorder(im_core,B_SIZE,B_SIZE,B_SIZE,B_SIZE,cv2.BORDER_CONSTANT,value=RED)
        im[t_s:b_s,l_s:r_s] = im_piece
        #print("2",inx)
    elif type == 1:
        im_core = im[t:b,l:r,:]
        im_piece = cv2.copyMakeBorder(im_core,B_SIZE,B_SIZE,B_SIZE,B_SIZE,cv2.BORDER_CONSTANT,value=BLUE)
        im[t_s:b_s,l_s:r_s,:] = im_piece
        #print("1",inx)
    else:
        site = img_list_info[current]["site"]
        im_tar_name = img_list[current][0].split("/")[1]
        print(im_tar_name)
        im_org_name = [i["org_name"] for i in img_list_info if i["target_name"] == im_tar_name]
        print(join("../data",site,im_org_name[0]))
        im_org = cv2.imread(join("../data",site,im_org_name[0]), cv2.IMREAD_UNCHANGED)
        im_piece = im_org[t_s:b_s,l_s:r_s,:]
        if b_s > im_org.shape[0]:
            zos = np.zeros((W[0]-im_piece.shape[0],im_piece.shape[1],4))
            im_piece = np.vstack((im_piece,zos))
        if r_s > im_org.shape[1]:
            zos = np.zeros((im_piece.shape[0],W[1]-im_piece.shape[1],4))
            im_piece = np.hstack((im_piece,zos))        
        im[t_s:b_s,l_s:r_s,:] = im_piece[:,:,0:3]
    cv2.imwrite(img_list[current][0],im)
    
def left_click_hum_chg(event):
    y = event.x
    x = event.y
    slice_pos, label_pos = comp_positions(x, y)
    img_list_info[current]["img_labels"][label_pos] = 0
    print(label_pos,img_list_info[current]["img_labels"][label_pos])
    chg_slice_content(x, y, 0)
    im = img_list[current][0]
    im_pi = ImageTk.PhotoImage(file=im)
    label1.configure(image=im_pi)
    label1.image=im_pi
    
def right_click_nat_chg(event):
    y = event.x
    x = event.y 
    slice_pos, label_pos = comp_positions(x, y)
    img_list_info[current]["img_labels"][label_pos] = 1
    chg_slice_content(x, y, 1)
    im = img_list[current][0]
    im_pi = ImageTk.PhotoImage(file=im)
    label1.configure(image=im_pi)
    label1.image=im_pi
    
def double_lclick_no_chg(event):
    y = event.x
    x = event.y
    slice_pos, label_pos = comp_positions(x, y)
    img_list_info[current]["img_labels"][label_pos] = 2
    chg_slice_content(x, y, 2)
    im = img_list[current][0]
    im_pi = ImageTk.PhotoImage(file=im)
    label1.configure(image=im_pi)
    label1.image=im_pi
    
def get_img_list():
    result_dir = 'results'
    median_dir = 'figures_mine'
    #read median images
    f_meds = [f for f in listdir(median_dir) if isfile(join(median_dir,f)) and f.endswith('.png')]
    f_meds.sort()
    ims_meds = {}
    for f in f_meds:
        nim = f.split("_median",1)[0]
        #im = Image.open(join(median_dir,f))
        im = join(median_dir,f)
        ims_meds[nim] = im
    f_res = [f for f in listdir(result_dir)];
    f_res.sort();
    for f in f_res:
        try:
            #image = Image.open(join(result_dir,f))
            image = join(result_dir,f)
            image_med = ims_meds[f.split("$",1)[0]]
            img_list.append((image,image_med))
        except Exception as ex:
            pass
        
def read_img_list_info():
    #fields:site/org_name/target_name/img_no/img_shape/img_labels
    with open("ims_list_info.pkl","rb") as f:
        img_list_info[:] = pickle.load(f)
    f.close()

def save_img_list_info(event):
    print("saving ims list info...")
    #fields:site/org_name/target_name/img_no/img_shape/img_labels
    with open("ims_list_info_new.pkl","wb") as f:
        pickle.dump(img_list_info,f)
    f.close()
    
def viewer_func():
    #global label1,label2
    prev_img = ImageTk.PhotoImage(file="prev_arrow.png")
    next_img = ImageTk.PhotoImage(file="next_arrow.png")
    exit_img = ImageTk.PhotoImage(file="exit.png")
    b_prev = Button(frame,command=prev)
    b_prev.config(image=prev_img)
    b_prev.image = prev_img
    b_prev.pack()
    b_next = Button(frame,command=next)
    b_next.config(image=next_img)
    b_next.image = next_img
    b_next.pack()
    b_exit = Button(frame,command=root.quit)
    b_exit.config(image=exit_img)
    b_exit.image=exit_img
    b_exit.pack()
    b_exit.bind("<Button-1>",save_img_list_info)
    
def prev():
    global current
    if not (0 <= current-1 < len(img_list)):
        messagebox.showinfo('End','No more image.')
        return
    current -= 1
    im,im_med = img_list[current]
    im_pi = ImageTk.PhotoImage(file=im)
    im_med_pi = ImageTk.PhotoImage(file=im_med)
    print("prev",im,im_med)
    label1.configure(image=im_pi)
    label1.image=im_pi
    label2.configure(image=im_med_pi)
    label.image=im_med_pi

def next():
    global current
    if not (0 <= current+1 < len(img_list)):
        messagebox.showinfo('End','No more image.')
        return
    current += 1
    im,im_med = img_list[current]
    im_pi = ImageTk.PhotoImage(file=im)
    im_med_pi = ImageTk.PhotoImage(file=im_med)
    label1.configure(image=im_pi)
    label1.image=im_pi
    label2.configure(image=im_med_pi)
    label2.image=im_med_pi
    
get_img_list()
read_img_list_info()
root = Tk()
root.title('image viewer')
frame = Frame(root)
frame.pack()
l = Label(frame,text="left_click:hum change;right_click:nat change;double_left_click:no change")
l.pack()
image,image_med = img_list[current]
image_pi = ImageTk.PhotoImage(file = image)
image_med_pi = ImageTk.PhotoImage(file = image_med)
label1 = Label(frame, image=image_pi)
label1.pack()
label1.bind("<Button-1>", left_click_hum_chg)
label1.bind("<Button-2>", right_click_nat_chg)
label1.bind("<Double-1>", double_lclick_no_chg)
label2 = Label(frame, image=image_med_pi)
label2.pack()
viewer_func()
root.mainloop() # wait until user clicks the window
