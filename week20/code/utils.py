from mine_synthetic_abs import *
from mine_test_ims_create import *
import csv

ISIZE_BEGIN = 0
ISIZE_END = 253
B_SIZE = 1 #border size

#load training and testing datasets from local
def load_mine_data():
    datasets = []
    run_mine_syn()
    run_mine_diff_out()
    #train dataset
    X_ = [i[0] for i in ims_blend_diff_pieces]
    X = np.array(X_)
    Y = np.array([np.int(i[1]) for i in ims_blend_diff_pieces])
    print(X.shape,Y.shape)
    datasets.append((X,Y))
    test_X = np.array([np.array([i[0] for i in j]) for j in mine_diff_pieces])
    datasets.append((test_X))
    valid_X = np.array([np.array([i[0] for i in j]) for j in ims_syn_test_pieces])
    valid_Y = np.array([np.array([i[1] for i in j]) for j in ims_syn_test_pieces])
    datasets.append((valid_X,valid_Y))
    print(test_X.shape)
    dist_train_feas_norm = np.array(dist_ims_feas).astype(np.int)
    dist_valid_feas_norm = []
    for dist_feas in dist_ims_test_feas:
        dist_valid_feas_norm.append(np.array(dist_feas).astype(np.int))
    dist_test_feas_norm = []
    for dist_feas in test_dist_feas:
        dist_test_feas_norm.append(np.array(dist_feas).astype(np.int)) 
    print("lenght of train and test features",len(dist_valid_feas_norm),len(dist_test_feas_norm))
    return datasets,dist_train_feas_norm,dist_valid_feas_norm,dist_test_feas_norm

#output the testing results, that is ,the whole image
def write_results(ims, des, start, target_ims_names):
    fp = des
    fpp = des + "/*"
    fs = glob.glob(fpp)
    for f in fs:
        remove(f)
    #print(h,w)
    n = start
    for _,im in enumerate(ims):
        s = np.array(im).shape
        h = s[0]*W[0]
        w = s[1]*W[1]
        res_img = np.zeros((h,w,3))
        for i in range(len(im)):
            for j in range(len(im[i])):
                t = i*W[0]
                b = (i+1)*W[0]
                l = j*W[1]
                r = (j+1)*W[1]
                res_img[t:b,l:r,:] = im[i][j][:,:,0:3]
        if len(target_ims_names) == 0:
            if n < 10:
                fn = fp + "/ims_result" + str(0) + str(n) + ".png"
            else:
                fn = fp + "/ims_result" + str(n) + ".png"
        else:
            fn = join(fp,target_ims_names[_])
        cv2.imwrite(fn,res_img)
        n += 1
        
def create_target_ims_names(test_labels):
    target_ims_names = []
    labels = []
    for _,test_label in enumerate(test_labels):
        label = [0,0,0]
        t = all_sites_org_ims_names[_]
        cls,counts = np.unique(test_label,return_counts = True)
        for i,j in zip(cls,counts):
            label[i] = j
        n_t = t[0]+"$"
        if label[0] != 0:
            n_t += "Hum"
        if label[1] != 0:
            n_t += "Nat"
        if label[2] != 0:
            n_t += "Non"
        n_t += "$"+str(t[-1]) + ".png"
        target_ims_names.append(n_t)
        labels.append(label)
    return target_ims_names,labels
        
#visualize the testing results
def show_visualized_results(test_labels, org_pieces , start, end, des = 'results'):
    BLUE = [255,0,0,255]
    RED = [0,0,255,255]
    #print(np.asarray(test_label).T)
    k = 0
    #save images list informatioin
    target_ims_names = []
    if des == "results": 
        target_ims_names,label_counts = create_target_ims_names(test_labels)
        save_ims_info(test_labels,org_pieces,target_ims_names,label_counts)
    for test_im_pieces in org_pieces[start:end]:
        test_label = test_labels[k]
        if len(test_im_pieces) == 0: #blank images occur
            continue
        for i in range(len(test_im_pieces)):
            for j in range(len(test_im_pieces[i])):
                im = test_im_pieces[i][j]
                inx = i*len(test_im_pieces[i]) + j
                t = B_SIZE
                b = W[0]-B_SIZE
                l = B_SIZE
                r = W[1] - B_SIZE
                #add border for each piece
                if test_label[inx] == 1:
                    im_core = im[t:b,l:r,:]
                    im = cv2.copyMakeBorder(im_core,B_SIZE,B_SIZE,B_SIZE,B_SIZE,cv2.BORDER_CONSTANT,value=BLUE)
                    #print("1",inx)
                elif test_label[inx] == 0:
                    im_core = im[t:b,l:r,:]
                    im = cv2.copyMakeBorder(im_core,B_SIZE,B_SIZE,B_SIZE,B_SIZE,cv2.BORDER_CONSTANT,value=RED)
                    #print("2",inx)
                test_im_pieces[i][j] = im
        k += 1
         
    write_results(org_pieces[start:end],des,start,target_ims_names)

def load_params():
    #f = open("params.pkl","rb")
    f = open(sys.argv[3],"rb")
    #params = pickle.load(f,encoding = 'latin1')
    params = pickle.load(f)
    f.close()
    return params

def save_params(params):
    f = open("params.pkl","wb")
    pickle.dump(params,f)
    f.close()

def save_ims_info(test_labels,org_pieces,target_ims_names,label_counts):
    #write ims detailed info as a pkl file
    ims_list_info = []
    for _,test_label in enumerate(test_labels):
        t = all_sites_org_ims_names[_]
        t_t = {'site':t[0],'org_name':t[1],'target_name':target_ims_names[_],'img_no':t[-1],'img_shape':np.array(org_pieces[_]).shape[0:2],'img_labels':test_label}
        #t_t = [t[0],t[1],target_ims_names[_],t[-1],np.array(org_pieces[_]).shape[0:2],test_label]
        #t_t = t + (target_ims_names[_],np.array(org_pieces[_]).shape[0:2],test_label)
        ims_list_info.append(t_t)
    with open("ims_list_info.pkl","wb") as f:
        pickle.dump(ims_list_info,f)
    f.close()

    #write ims info as a cvs file
    with open('ims_list_info.csv', 'w') as csvfile:
        fieldnames = ['site', 'org_name', 'target_name', 'hum_count', 'nat_count', 'non_count']
        #imswriter = csv.DictWriter(csvfile, fieldnames=fieldnames)
        imswriter = csv.writer(csvfile, delimiter=',', quotechar='|', quoting=csv.QUOTE_MINIMAL)
        imswriter.writerow(fieldnames)
        for _,test_label in enumerate(test_labels):
            t = all_sites_org_ims_names[_]
            #t_t = {fieldnames[0]:t[0],fieldnames[1]:t[1],fieldnames[2]:target_ims_names[_],fieldnames[3]:label_counts[_][0], fieldnames[4]:label_counts[_][1], fieldnames[5]:label_counts[_][2]}
            t_t = [t[0],t[1],target_ims_names[_],label_counts[_][0], label_counts[_][1], label_counts[_][2]]
            #t_t = list(t[0:2]) + [target_ims_names[_]] + label_counts[_]
            imswriter.writerow(t_t)
    csvfile.close()
