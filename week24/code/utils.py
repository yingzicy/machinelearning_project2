from mine_synthetic_abs import *
from mine_test_ims_create import *
import csv

ISIZE_BEGIN = 0
ISIZE_END = 50
B_SIZE = 1 #border size

#load training and testing datasets from local
def load_mine_data():
    datasets = []
    run_mine_syn()
    run_mine_diff_out()
    #train dataset
    X = np.array([np.array(i[0]) for i in ims_blend_diff_pieces])
    Y = np.array([np.array(i[1]) for i in ims_blend_diff_pieces])
    datasets.append((X,Y))
    valid_X = np.array([np.array(i[0]) for i  in ims_syn_test_pieces])
    valid_Y = np.array([np.array(i[1]) for i  in ims_syn_test_pieces])
    datasets.append((valid_X,valid_Y))
    test_X = np.array([np.array(i[0]) for i in mine_diff_pieces])
    datasets.append((test_X))
    print("train_X shape and train_Y shape:",X.shape,Y.shape)
    print("valid_X shape and valid_Y shape:",valid_X.shape, valid_Y.shape);
    print("test_X shape:",test_X.shape)
    return datasets

#output the testing results, that is ,the whole image
def write_results(ims, des, start):
    fp = des
    fpp = des + "/*"
    fs = glob.glob(fpp)
    for f in fs:
        remove(f)
    #print(h,w)
    n = start
    for _,im in enumerate(ims):
        if len(im) == 0: #blank images occur
            continue
        if n < 10:
            fn = fp + "/ims_result" + str(0) + str(n) + ".png"
        else:
            fn = fp + "/ims_result" + str(n) + ".png"
        cv2.imwrite(fn,im)
        n += 1
        
def create_target_ims_names(test_labels):
    target_ims_names = []
    labels = []
    for _,test_label in enumerate(test_labels):
        test_label1 = test_label.flatten()
        label = [0,0,0]
        t = all_sites_org_ims_names[_]
        cls,counts = np.unique(test_label1,return_counts = True)
        for i,j in zip(cls,counts):
            label[i] = j
        n_t = t[0]+"$"
        if label[0] != 0:
            n_t += "Hum"
        if label[1] != 0:
            n_t += "Nat"
        if label[2] != 0:
            n_t += "Non"
        n_t += "$"+str(t[-1]) + ".png"
        target_ims_names.append(n_t)
        #labels.append(label)
    #return target_ims_names,labels
    return target_ims_names
        
#visualize the testing results
def show_visualized_results(test_labels, org_pieces , start, end, des = 'results'):
    BLUE = [255,0,0,255]
    RED = [0,0,255,255]
    #print(np.asarray(test_label).T)
    #save images list informatioin
    target_ims_names = []
    """if des == "results": 
        target_ims_names = create_target_ims_names(test_labels)"""
        #save_ims_info(test_labels,org_pieces,target_ims_names,label_counts)
    for _,im in enumerate(org_pieces[start:end]):
        test = test_labels[_]
        if len(im) == 0: #blank images occur
            continue
        for ii in range(W[0]):
            for jj in range(W[1]):
                if im[ii,jj,3] == 0:
                    continue
                if test[ii,jj] == 0:
                    im[ii,jj] = RED
                elif test[ii,jj] == 1:
                    im[ii,jj] = BLUE
                #test_im_pieces[i][j] = im
         
    write_results(org_pieces[start:end],des,start)

def load_params():
    #f = open("params.pkl","rb")
    f = open(sys.argv[3],"rb")
    #params = pickle.load(f,encoding = 'latin1')
    params = pickle.load(f)
    f.close()
    return params

def save_params(params):
    f = open("params.pkl","wb")
    pickle.dump(params,f)
    f.close()

def save_ims_info(test_labels,org_pieces,target_ims_names,label_counts):
    #write ims detailed info as a pkl file
    ims_list_info = []
    for _,test_label in enumerate(test_labels):
        t = all_sites_org_ims_names[_]
        t_t = {'site':t[0],'org_name':t[1],'target_name':target_ims_names[_],'img_no':t[-1],'img_shape':np.array(org_pieces[_]).shape[0:2],'img_labels':test_label}
        ims_list_info.append(t_t)
    with open("ims_list_info.pkl","wb") as f:
        pickle.dump(ims_list_info,f)
    f.close()

    #write ims info as a cvs file
    with open('ims_list_info.csv', 'w') as csvfile:
        fieldnames = ['site', 'org_name', 'target_name', 'hum_count', 'nat_count', 'non_count']
        imswriter = csv.writer(csvfile, delimiter=',', quotechar='|', quoting=csv.QUOTE_MINIMAL)
        imswriter.writerow(fieldnames)
        for _,test_label in enumerate(test_labels):
            t = all_sites_org_ims_names[_]
            t_t = [t[0],t[1],target_ims_names[_],label_counts[_][0], label_counts[_][1], label_counts[_][2]]
            imswriter.writerow(t_t)
    csvfile.close()
