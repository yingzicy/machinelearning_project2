from LR import *
import theano.tensor.signal.downsample
from theano.sandbox.cuda.dnn import dnn_conv
from theano import function,printing

#define convlution network layer
class ConvLayer(object):
    def __init__(self, input, filter_shape, rng, S , P = 'full', pool_size = (2,2), activation = T.nnet.relu):
        #assert input.shape[1] == filter_shape[1]
        e = np.sqrt( 1. / (filter_shape[0] + filter_shape[1]))
        W_value = rng.uniform(low = -e, high = e, size = filter_shape)
        b_value = rng.uniform(low = -e, high = e, size = ((filter_shape[0],)))
        self.W = theano.shared(value = np.asarray(W_value, dtype = theano.config.floatX), name = 'W', borrow = True)
        self.b = theano.shared(value = np.asarray(b_value, dtype = theano.config.floatX), name = 'W', borrow = True)
        self.params = [self.W, self.b]
        self.L2_reg = T.sum(self.W **2) + T.sum(self.b **2)
        
        #conv(X,w,b,activation)
        # co = dnn_conv(X, w, border_mode=int(np.floor(w.get_value().shape[-1]/2.)))
        s = int(np.floor(self.W.get_value().shape[-1]/2.))
        co = T.nnet.conv2d(input,self.W,border_mode=P)[:,:,s:-s,s:-s]
        #co = T.nnet.conv2d(input, self.W, sub_sample = S, border_mode = P) #convolution oper
        if self.b is not None:
            co += self.b.dimshuffle('x',0,'x','x')
        co = activation(co)

        #pool(conv(X,w,b,activation),pool_size)
        self.out = T.signal.downsample.max_pool_2d(co, pool_size) #pooling oper

class DeconvLayer(object):
    def __init__(self, X, filter_shape, rng, S = (1,1), P = 'full', pool_size = (2,2), activation = T.nnet.relu):
        #assert X.shape[1] == filter_shape[1]
        e = np.sqrt( 1. / (filter_shape[0] + filter_shape[1]))
        W_value = rng.uniform(low = -e, high = e, size = filter_shape)
        b_value = rng.uniform(low = -e, high = e, size = ((filter_shape[0],)))
        self.W = theano.shared(value = np.asarray(W_value, dtype = theano.config.floatX), name = 'W', borrow = True)
        self.b = theano.shared(value = np.asarray(b_value, dtype = theano.config.floatX), name = 'W', borrow = True)
        self.params = [self.W, self.b]
        self.L2_reg = T.sum(self.W **2) + T.sum(self.b **2)
        
        #depool(X)
        """
        output_shape = [
        X.shape[1],
        X.shape[2]*factor,
        X.shape[3]*factor
        ]
        stride = X.shape[2]
        offset = X.shape[3]
        in_dim = stride * offset
        out_dim = in_dim * factor * factor
        
        upsamp_matrix = T.zeros((in_dim, out_dim))
        rows = T.arange(in_dim)
        cols = rows*factor + (rows/stride * factor * offset)
        upsamp_matrix = T.set_subtensor(upsamp_matrix[rows, cols], 1.)
        
        flat = T.reshape(X, (X.shape[0], output_shape[0], X.shape[2] * X.shape[3]))

        up_flat = T.dot(flat, upsamp_matrix)
        upsamp = T.reshape(up_flat, (X.shape[0], output_shape[0],
                                 output_shape[1], output_shape[2]))
        """
        factor = pool_size[0]
        output_shape = [X.shape[1],X.shape[2]*factor,X.shape[3]*factor]
        stride = X.shape[2]
        offset = X.shape[3]
        in_dim = stride * offset
        out_dim = in_dim * factor * factor
        upsamp_matrix = T.zeros((in_dim,out_dim))
        rows = T.arange(in_dim)
        cols = rows*factor + (T.cast(rows/stride,'int32')*factor*offset)
        upsamp_matrix = T.set_subtensor(upsamp_matrix[rows,cols],1.)

        flat = T.reshape(X,(X.shape[0],output_shape[0],X.shape[2]*X.shape[3]))
        up_flat = T.dot(flat,upsamp_matrix)
        depo = T.reshape(up_flat,(X.shape[0],output_shape[0],output_shape[1],output_shape[2]))

        #deconv(depool(X),w,b)
        s = int(np.floor(self.W.get_value().shape[-1]/2.))
        deco = T.nnet.conv2d(depo,self.W,border_mode = 'full')[:,:,s:-s,s:-s]
        if self.b is not None:
            deco += self.b.dimshuffle('x',0,'x','x')
        self.out = activation(deco)
        

#define convolution neural network
class ConAE(object):
    """includes conv layer
    conv layer includes conv, pooling, flatting steps"""
    def __init__(self, input, y, filter_shape, de_filter_shape,S, P, n_in, n_out, rng, pool_size):
        #create convlayer instance1
        self.cnlayer1 = ConvLayer(
            input = input,
            filter_shape = filter_shape[0],
            rng = rng,
            S = S[0],
            P = P[0],
            pool_size = pool_size[0],
            activation = T.nnet.relu
        )
        #create convlayer instance2
        self.cnlayer2 = ConvLayer(
            input = self.cnlayer1.out,
            filter_shape = filter_shape[1],
            rng = rng,
            S = S[1],
            P = P[1],
            pool_size = pool_size[1],
            activation = T.nnet.relu
        )
        
        #create convlayer instance3
        self.cnlayer3 = ConvLayer(
            input = self.cnlayer2.out,
            filter_shape = filter_shape[2],
            rng = rng,
            S = S[2],
            P = P[2],
            pool_size = pool_size[2],
            activation = T.nnet.relu
        )

        #create convlayer instance4
        self.cnlayer4 = ConvLayer(
            input = self.cnlayer3.out,
            filter_shape = filter_shape[3],
            rng = rng,
            S = S[3],
            P = P[3],
            pool_size = pool_size[3],
            activation = T.nnet.relu
        )
        #create deconvlayer instance4
        self.decnlayer4 = DeconvLayer(
            X = self.cnlayer4.out,
            filter_shape = de_filter_shape[3],
            rng = rng,
            S = S[3],
            P = P[3],
            pool_size = pool_size[3],
            activation = T.nnet.relu
        )
         
        #create deconvlayer instance3
        self.decnlayer3 = DeconvLayer(
            X = self.decnlayer4.out,
            filter_shape = de_filter_shape[2],
            rng = rng,
            S = S[2],
            P = P[2],
            pool_size = pool_size[2],
            activation = T.nnet.relu
        )
        
        #create deconvlayer instance2
        self.decnlayer2 = DeconvLayer(
            X = self.decnlayer3.out,
            filter_shape = de_filter_shape[1],
            rng = rng,
            S = S[1],
            P = P[1],
            pool_size = pool_size[1],
            activation = T.nnet.relu
        )
         
        #create deconvlayer instance1
        self.decnlayer1 = DeconvLayer(
            X = self.decnlayer2.out,
            filter_shape = de_filter_shape[0],
            rng = rng,
            S = S[0],
            P = P[0],
            pool_size = pool_size[0],
            activation = T.nnet.relu
        )
        #softmax
        in_shape = self.decnlayer1.out.shape
        in_shape1 = (in_shape[0],in_shape[2],in_shape[3],in_shape[1])
        self.lrlayer = LogisticRegression(
            input = T.reshape(self.decnlayer1.out,in_shape1),
            n_in = n_in,
            n_out = n_out,
            rng = rng
            )
        
        self.params = self.cnlayer1.params+self.cnlayer2.params+self.cnlayer3.params+self.cnlayer4.params+ \
                      self.decnlayer4.params+self.decnlayer3.params+self.decnlayer2.params+self.decnlayer1.params+self.lrlayer.params
        
        self.L2_reg = self.cnlayer1.L2_reg+self.cnlayer2.L2_reg+self.cnlayer3.L2_reg+self.cnlayer4.L2_reg+ \
                      self.decnlayer4.L2_reg+self.decnlayer3.L2_reg+self.decnlayer2.L2_reg+self.decnlayer1.L2_reg+self.lrlayer.L2_reg
        
        self.y_pred = self.lrlayer.y_pred
        self.errors = self.lrlayer.errors(y)
        self.caeout1 = self.cnlayer1.out
        self.caeout2 = self.cnlayer2.out
        self.caeout3 = self.cnlayer3.out
        self.caeout4 = self.cnlayer4.out
        self.decaeout1 = self.decnlayer4.out
        self.decaeout2 = self.decnlayer3.out
        self.decaeout3 = self.decnlayer2.out
        self.decaeout4 = self.decnlayer1.out
        self.neg_log_likelihood = self.lrlayer.negative_log_likelihood(y)
        self.input = input
        self.y_p = self.lrlayer.y_p
        self.input_shape = self.lrlayer.input_shape
        self.y_p_1d_shape = self.lrlayer.y_p_1d_shape
        self.y_p_shape = self.lrlayer.y_p_shape
        self.y_pred_shape = self.lrlayer.y_pred_shape
