from six.moves import cPickle
import sys
from os import listdir,remove,makedirs,walk
from os.path import isfile,join,exists
import numpy as np
import numpy.random as rng
import cv2,glob,pickle,copy,json
from datetime import datetime
from shutil import copyfile
from site_date_distribution import comp_distribution
#from mine_images_median_diff_abs import comp_diff_val_distribution,comp_ims_median
#from mine_images_median_diff_abs import *

ims_blend_diff_pieces = []
dist_ims_feas = []
ims_syn_test_org_pieces = []
ims_syn_test_pieces = []
dist_ims_test_feas = []
W = (256,256)
NC_THD = 15
IMG_SHAPE = (256,256)
HUM_RATIO = 0.05
SS = 0
E = 0
T_THD = 6

#load patchs for blending
def load_add_ims(fpath):
    add_ims = []
    add_fp = "hum_nat/add_targets"
    add_eff_dirs = []
    if fpath.startswith('all'):
        add_dirs = [f for f in next(walk(add_fp))[1]]
        add_dirs.sort()
        for dir in add_dirs:
            max_diff, diff_date_counts, diff_date_names = comp_distribution('../org_json',dir)
            if max_diff < T_THD:
                print(dir,"max_diff <T_THD for load_add_ims")
                continue
            add_im = []
            add_fpp = add_fp+'/'+dir
            add_fs = [f for f in listdir(add_fpp) if isfile(join(add_fpp,f)) and (f.endswith('.tif') or f.endswith('.png'))]
            add_fs.sort()
            for add_f in add_fs:
                add_fn = join(add_fpp,add_f)
                add_im.append(cv2.imread(add_fn, cv2.IMREAD_UNCHANGED))
            add_ims.append(add_im)
            add_eff_dirs.append(dir)
    else:
        add_dirs = [f for f in next(walk(add_fp))[1] if f.startswith('add') and f.endswith(fpath.split('_',1)[0])]
        add_dirs.sort()
        add_fpp = add_fp+'/'+ add_dirs[0]       
        add_fs = [f for f in listdir(add_fpp) if isfile(join(add_fpp,f)) and (f.endswith(".tif") or f.endswith(".png"))]
        add_fs.sort()
        add_im = []
        for f in add_fs:
            fn = join(add_fpp,f)
            add_im.append(cv2.imread(fn, cv2.IMREAD_UNCHANGED))
        add_ims.append(add_im)
    return add_ims,add_eff_dirs

#write location features for all human change images of each site
def write_loc_features(fpath, dir, hum_chg_locs):
    fn = dir
    print("write loc features:",fn)
    f = open(join(fpath,fn+".pkl"),"wb")
    pickle.dump(hum_chg_locs,f,protocol=0)
    f.close()
    
#generate natural change image for the first half images
def images_hum(dir, ims, add_ims):
    rng = np.random
    #mid = int(len(ims)/2)
    c = 0
    hum_chg_locs = []
    r_ival = max([max(im.shape[0],im.shape[1]) for im in add_ims])
    for im in ims:
        hum_chg_loc = []
        rn = int(im.shape[0]/r_ival)
        cn = int(im.shape[1]/r_ival)
        cols = [rng.permutation(cn) for i_ in range(rn)]
        cn1 = int(1*cn/2)
        for i_ in range(rn):
            for j_ in range(cn1):
                inx = (i_*cn1 + j_)%len(add_ims)
                h = add_ims[inx].shape[0]-1
                w = add_ims[inx].shape[1]-1
                t = i_ * r_ival
                l = cols[i_][j_] * r_ival
                b = t + h
                r = l + w
                if b > im.shape[0] or  r > im.shape[1]:
                    continue
                if 0 not in im[t:b,l:r,3]:
                    for ii in range(b-t+1):
                        for jj in range(r-l+1):
                            if add_ims[inx][ii,jj,3] != 0:
                                im[t+ii,l+jj,0:3] = add_ims[inx][ii,jj,0:3]
                    loc = (t,l,h,w)
                    hum_chg_loc.append((loc,inx))
        hum_chg_locs.append(hum_chg_loc)
    write_loc_features('features', dir , hum_chg_locs)    

#generate human change images for the other half images
def images_nat(ims):
    for im in ims:
        ims_blend_nat.append(im)
    for im in ncs:
        ims_blend_nc.append(im)
        
def write_blend_json_ims(root_src, root_des, js, tp):
    if not exists(root_des):
        makedirs(root_des)
    fns = root_des + "/*"
    fs = glob.glob(fns)
    for f in fs:
        if tp == 1 and '_hum' in f:
            remove(f)
        elif tp == 2 and '_nat' in f:
            remove(f)
        elif tp == 3 and '_nc' in f:
            remove(f)
        elif tp == 0:
            remove(f)
    for _,f in enumerate(js):
        rs = join(root_src,f)+'.json'
        if any([join(root_src,t) for t in listdir(root_src) if join(root_src,t) == rs]) == False:
            print("no such json file",rs)
            return
        fields = f.split('_org')
        if tp == 1:
            fn = join(root_des,fields[0]) + "_hum" + fields[1].replace('.tif','') + ".json"
            copyfile(rs,fn)
        if tp == 2:
            fn = join(root_des,fields[0]) + "_nat" + fields[1].replace('.tif','') + ".json"
            copyfile(rs,fn)
        if tp == 3:
            fn = join(root_des,fields[0]) + "_nc" + fields[1].replace('.tif','') + ".json"
            copyfile(rs,fn)
        if tp == 0:
            fn = join(root_des,f) + ".json"
            copyfile(rs,fn)
            
def write_blend_ims(fpath,dir,ims, fs, tp):
    if not exists(fpath):
        makedirs(fpath)
    fns = fpath + "/*"
    fs1 = glob.glob(fns)
    for f in fs1:
        if tp == 1 and '_hum' in f:
            remove(f)
        elif tp == 2 and '_nat' in f:
            remove(f)
        elif tp == 3 and '_nc' in f:
            remove(f)
    for _,im in enumerate(ims):
        fields = fs[_].split('_org')
        if tp == 1:
            fn = join(fpath,fields[0]) + "_hum" + fields[1].replace('.tif','') + ".png"
            cv2.imwrite(fn,im)
        if tp == 2:
            fn = join(fpath,fields[0]) + "_nat" + fields[1].replace('.tif','') + ".png"
            cv2.imwrite(fn,im)
        if tp == 3:
            fn = join(fpath,fields[0]) + "_nc" + fields[1].replace('.tif','') + ".png"
            cv2.imwrite(fn,im)
        
#blend the original image with cloud image
def images_blend(fpath, add_ims, eff_dirs):
    #read directory names under 'hum_nat'
    add_fp = 'hum_nat'
    add_fp1 = join(add_fp,'org_ims/hum')
    add_fp2 = join('../org_json')
    add_fp3 = '../org_data'
    if fpath.startswith('all'):
        hum_dirs = [f for f in next(walk(add_fp1))[1]]
        hum_dirs.sort()
        nat_dirs = [f for f in next(walk(add_fp2))[1]]
        nat_dirs.sort()
        nc_dirs = [f for f in next(walk(add_fp2))[1]]
        nc_dirs.sort()
    else:
        hum_dirs = [f for f in next(walk(add_fp1))[1] if f.endswith(fpath.split('_',1)[0])]
        hum_dirs.sort()
        nat_dirs = [f for f in next(walk(add_fp2))[1] if f.endswith(fpath.split('_',1)[0])]
        nat_dirs.sort()
        nc_dirs = [f for f in next(walk(add_fp2))[1] if f.endswith(fpath.split('_',1)[0])]
        nc_dirs.sort()
        
    #blend each image of each directory and save to directories under 'syn'
    syn_fp = 'syn'
    #print("blend hum dirs:",hum_dirs)
    for _,dir in enumerate(eff_dirs):
        fp = join(add_fp1,dir)
        hum_fns = [f for f in listdir(fp) if isfile(join(fp,f)) and (f.endswith('.tif') or f.endswith('.png'))]
        hum_fns.sort()
        #print("hum files:",hum_fns)
        hum_ims = [cv2.imread(join(fp,f),cv2.IMREAD_UNCHANGED) for f in hum_fns if f.endswith('.tif') or f.endswith('.png')]
        images_hum(dir, hum_ims, add_ims[_])
        write_blend_ims(syn_fp + '/' + dir,dir,hum_ims,hum_fns,1)

    #nat images
    for _,dir in enumerate(eff_dirs):
        nat_ims = []
        nat_fns = []
        fp = join(add_fp2,dir)
        n_fns = [f for f in listdir(fp) if isfile(join(fp,f)) and f.endswith('.json')]
        n_fns.sort()
        if len(n_fns) == 0:
            continue
        for _,j_f in enumerate(n_fns):
            with open(join(fp,j_f)) as ff:
                d = json.load(ff)
            ff.close()
            if d["properties"]["cloud_cover"] > 0.5 and dir != 'mining' or d["properties"]["cloud_cover"] > 0.8 and dir == 'mining':
                i_f = join(join(add_fp3,dir),j_f.split('.',1)[0]+".tif")
                print(">0.8",i_f)
                nat_ims.append(cv2.imread(i_f,cv2.IMREAD_UNCHANGED))
                nat_fns.append(j_f.split(".",1)[0])
        write_blend_ims(syn_fp + '/' + dir,dir,nat_ims,nat_fns,2)
    #no change images
    for _,dir in enumerate(eff_dirs):
        nc_ims = []
        nc_fns = []
        fp = join(add_fp2,dir)
        n_fns = [f for f in listdir(fp) if isfile(join(fp,f)) and f.endswith('.json')]
        n_fns.sort()
        if len(n_fns) == 0:
            continue
        for _,j_f in enumerate(n_fns):
            with open(join(fp,j_f)) as ff:
                d = json.load(ff)
            ff.close()
            if d["properties"]["cloud_cover"] == 0:
                i_f = join(join(add_fp3,dir),j_f.split('.',1)[0]+".tif")
                nc_ims.append(cv2.imread(i_f,cv2.IMREAD_UNCHANGED))
                nc_fns.append(j_f.split(".",1)[0])
        write_blend_ims(syn_fp + '/' + dir,dir,nc_ims,nc_fns,3)
    #copy json files to directory test_diff/json_train
    add_fp5 = 'test_diff/org_ims'
    dirs = next(walk(add_fp5))[1]
    for _,dir in enumerate(dirs):
        fp = join(add_fp5,dir)
        fns = [f.split('.',1)[0] for f in listdir(fp) if isfile(join(fp,f)) and (f.endswith('.tif') or f.endswith('.png'))]
        fns.sort()
        write_blend_json_ims(join(add_fp2,dir),"test_diff/json_test/"+dir,fns,0)
    
    
#read blended images 
def read_blend(fpath):
    #syn single site or all sites
    ims_blend_hums = []
    ims_blend_nats = []
    ims_blend_ncs = []
    ims_blend_hum_fns = []
    ims_blend_nat_fns = []
    ims_blend_nc_fns = []
    if fpath.endswith('syn'):
        print("generating synthetic images for all sites or single site......")
        add_ims, add_eff_dirs = load_add_ims(fpath)
        images_blend(fpath, add_ims, add_eff_dirs)
    else:
        if fpath.startswith('all'):
            dirs = next(walk('syn'))[1]
            dirs.sort()
        else:
            dirs = [fpath.split('_',1)[0]]
        if len(dirs) == 0:
            print("please create synthetic images first!")
            return
        for dir in dirs:
            ims_blend_hum = []
            ims_blend_nat = []
            ims_blend_nc = []
            ims_blend_hfns = []
            ims_blend_nfns = []
            ims_blend_cfns = []
            fp = 'syn/'+dir
            fs = [f for f in listdir(fp) if isfile(join(fp,f)) and (f.endswith('.tif') or f.endswith('.png'))]
            fs.sort()
            if len(fs) == 0:
                print("no synthetic images for site",dir)
            for f in fs:
                fn = join(fp,f)
                im = cv2.imread(fn, cv2.IMREAD_UNCHANGED).astype(np.float)
                if '_nat' in f:
                    ims_blend_nat.append(im)
                    ims_blend_nfns.append(f)
                elif '_hum' in f:
                    ims_blend_hum.append(im)
                    ims_blend_hfns.append(f)
                else:
                    ims_blend_nc.append(im)
                    ims_blend_cfns.append(f)
            ims_blend_hums.append(ims_blend_hum)
            ims_blend_nats.append(ims_blend_nat)
            ims_blend_ncs.append(ims_blend_nc)
            ims_blend_hum_fns.append(ims_blend_hfns)
            ims_blend_nat_fns.append(ims_blend_nfns)
            ims_blend_nc_fns.append(ims_blend_cfns)
    return (ims_blend_hums,ims_blend_nats,ims_blend_ncs),(ims_blend_hum_fns,ims_blend_nat_fns,ims_blend_nc_fns)
           
            
def write_blend_diff(fpath, ims_blend_diffs,ims_blend_diff_fns):
    fp = fpath
    fpp =fpath + "/*"
    if not exists(fpath):
        makedirs(fpath)
    fs = glob.glob(fpp)
    for f in fs:
        remove(f)
    ims_blend_diff_hum,ims_blend_diff_nat,ims_blend_diff_nc = ims_blend_diffs
    ims_blend_diff_hum_fns,ims_blend_diff_nat_fns,ims_blend_diff_nc_fns = ims_blend_diff_fns
    for _,im in enumerate(ims_blend_diff_hum):
        #fields = ims_blend_diff_hum_fns[_].split('_hum')
        #fn = join(fp,fields[0]) +"_hum" + fields[1] + ".png"
        cv2.imwrite(join(fp,ims_blend_diff_hum_fns[_]),im)
    for _,im in enumerate(ims_blend_diff_nat):
        cv2.imwrite(join(fp,ims_blend_diff_nat_fns[_]),im)
    for _,im in enumerate(ims_blend_diff_nc):
        cv2.imwrite(join(fp,ims_blend_diff_nc_fns[_]),im)

def write_test_diff(fp, ims_tests):
    fpp =fp + "/*"
    if not exists(fp):
        makedirs(fp)
    fs = glob.glob(fpp)
    for f in fs:
        remove(f)
    for _,im in enumerate(ims_tests):
        if _ < 10:
            fn = join(fp,'ims_blend_diff0') + str(_) + ".png"
        else:
            fn = join(fp,'ims_blend_diff') + str(_) + ".png"
        cv2.imwrite(fn,im)

#normalize each difference image
def norm_img_diff(ims_blend_diff_hn):
    m = 300
    M = -300
    for im in ims_blend_diff_hn:
        if im == None or len(im) == 0:
            continue
        alpha = np.tile(im[:,:,3].ravel().astype(np.object),3)
        rgb = im[:,:,0:3].ravel().astype(np.object)
        rgb[alpha==0] = None
        rgb = list(filter(None.__ne__,rgb))
        if len(rgb) == 0:
            continue
        m_tmp= min(rgb)
        M_tmp = max(rgb)
        if m_tmp < m:
            m = m_tmp
        if M_tmp > M:
            M = M_tmp
    print(m,M)
    for im in ims_blend_diff_hn:
        if im == None or len(im) == 0:
            continue
        for i in range(im.shape[0]):
            for j in range(im.shape[1]):
                #for k in range(im.shape[2]):
                if im[i,j,3] != 0:
                    im[i,j,0:3] = (im[i,j,0:3] - m)/(M - m) * 255
                    
#compute the difference bewteen median and individual image for each image row
def images_difference_abs(fpath, ims_med, ims_blends, ims_sit_blends_fns):
    ims_blend_hum,ims_blend_nat,ims_blend_nc = ims_blends
    ims_blend_diff_hum = []
    ims_blend_diff_nat = []
    ims_blend_diff_nc = []
    #print(ims_sit_blends_fns)
    for im in ims_blend_hum:
        imc = copy.deepcopy(im)
        for i in range(imc.shape[0]):
            for j in range(imc.shape[1]):
                if imc[i,j,3] != 0:
                    imc[i,j,0:3] = np.abs(imc[i,j,0:3] - ims_med[i,j,0:3])
        ims_blend_diff_hum.append(imc)
    for im in ims_blend_nat:
        imc = copy.deepcopy(im)
        for i in range(imc.shape[0]):
            for j in range(imc.shape[1]):
                if imc[i,j,3] != 0:
                    imc[i,j,0:3] = np.abs(imc[i,j,0:3] - ims_med[i,j,0:3])
        ims_blend_diff_nat.append(imc)
    for im in ims_blend_nc:
        imc = copy.deepcopy(im)
        for i in range(imc.shape[0]):
            for j in range(imc.shape[1]):
                if imc[i,j,3] != 0:
                    imc[i,j,0:3] = np.abs(imc[i,j,0:3] - ims_med[i,j,0:3])
        ims_blend_diff_nc.append(imc)
    ims_blend_diff_tmp = list(ims_blend_diff_hum)
    ims_blend_diff_tmp.extend(ims_blend_diff_nat)
    ims_blend_diff_tmp.extend(ims_blend_diff_nc)
    norm_img_diff(ims_blend_diff_tmp)
    write_blend_diff(fpath, (ims_blend_diff_hum,ims_blend_diff_nat,ims_blend_diff_nc),ims_sit_blends_fns)

#read location of human changes for all sites
def read_loc_features(fpath):
    fs = [f for f in listdir(fpath) if isfile(join(fpath,f)) and f.endswith('.pkl')]
    fs.sort()
    hum_chg_locs = []
    for f in fs:
        fn = open(join(fpath,f),"rb")
        hum_chg_loc = pickle.load(fn)
        hum_chg_locs.append(hum_chg_loc)
        fn.close()
    return hum_chg_locs
              
#load difference images for single or all sites
def read_difference_all(fpath):
    dirs = next(walk(fpath))[1]
    dirs.sort()
    ims_blend_diff_hums = []
    ims_blend_diff_nats = []
    ims_blend_diff_ncs = []
    ims_median_ims = []
    hum_chg_locs = []
    for dir in dirs:
        """max_diff, diff_date_counts, diff_date_names = comp_distribution('../org_json',dir)
        if max_diff < T_THD:
            continue"""
        ims_median_ims.append(cv2.imread(join("figures_mine",dir+"_median.png"),cv2.IMREAD_UNCHANGED))
        fp = join(fpath,dir)
        fs = [f for f in listdir(fp) if isfile(join(fp,f)) and (f.endswith('.tif') or f.endswith('.png'))];
        fs.sort()
        ims_blend_diff_hum_ = []
        ims_blend_diff_nat_ = []
        ims_blend_diff_nc_ = []
        for f in fs:
            fn = join(fp,f)
            im = cv2.imread(fn, cv2.IMREAD_UNCHANGED)
            if '_hum' in f:
                ims_blend_diff_hum_.append(im)
            elif '_nat' in f:
                ims_blend_diff_nat_.append(im)
            else:
                ims_blend_diff_nc_.append(im)
        ims_blend_diff_hums.append(ims_blend_diff_hum_)
        ims_blend_diff_nats.append(ims_blend_diff_nat_)
        ims_blend_diff_ncs.append(ims_blend_diff_nc_)
    hum_chg_locs = read_loc_features('features')
    return (ims_blend_diff_hums,ims_blend_diff_nats,ims_blend_diff_ncs),ims_median_ims,hum_chg_locs

#extract feas from each json file
def get_feas_from_json(d):
    #f1(1/2/3/4,spring/sum/autum/winter),f2(1/2/3,morning/afternoon/night),f3(1/2/3/4,N-E,E-S,S-W,W-N),f4(1/2/3,clear/betw/cloudy)
    d_str = d["properties"]["acquired"].split('.')
    if len(d_str) <= 1:
        date = datetime.strptime(d["properties"]["acquired"],"%Y-%m-%dT%H:%M:%SZ")
    else:
        date = datetime.strptime(d["properties"]["acquired"],"%Y-%m-%dT%H:%M:%S.%fZ")
    #date = datetime.strptime(d["properties"]["acquired"][0:10],"%Y-%m-%d")
    if 3<=date.month<=5:
        f1 = 1
    elif 6<=date.month <=8:
        f1 = 2
    elif 9<=date.month <=11:
        f1 = 3
    else:
        f1 = 4
    time = date
    #time = datetime.strptime(d["properties"]["acquired"][11:19],"%H:%M:%S")
    if time.hour < 11:
        f2 = 1
    elif time.hour < 17:
        f2 = 2
    else:
        f2 = 3
    sun = d["properties"]["sun_azimuth"]
    if sun < 90:
        f3 = 1
    elif sun < 180:
        f3 = 2
    elif sun < 270:
        f3 = 3
    else:
        f3 =4
    cloud = d["properties"]["cloud_cover"]
    if cloud < 0.1:
        f4 = 1
    elif cloud < 0.5:
        f4 = 2
    else:
        f4 = 3
    feas = [f1,f2,f3,f4]
    return feas

#get extra features for revelent directories    
def get_feas(fpath,dirs,tp):
    dist_extra_feas = []
    fp1 = '../org_json'
    for _,dir in enumerate(dirs):
        fp = join(fpath,dir)
        if tp == 0:
            fns = [f.replace('_hum','_org') for f in listdir(fp) if isfile(join(fp,f)) and '_hum' in f]
        elif tp == 1:
            fns = [f.replace('_nat','_org') for f in listdir(fp) if isfile(join(fp,f))  and '_nat' in f]
        else:
            fns = [f.replace('_nc','_org') for f in listdir(fp) if isfile(join(fp,f)) and '_nc' in f]
        fns.sort()
        #print("json file names",fns)
        dist_fea = []
        for f in fns:
            f = f.replace('.png','.json')
            fn = join(fp1,dir,f)
            with open(fn) as j_f:    
                d = json.load(j_f)
            feas = get_feas_from_json(d)
            dist_fea.append(feas)
        dist_extra_feas.append(dist_fea)
    return dist_extra_feas

#read extra features from json files
def read_extra_feas(fpath):
    dirs = [f for f in next(walk(fpath))[1]]
    dirs.sort()
    dist_feas_hums = get_feas(fpath,dirs,0)
    dist_feas_nats = get_feas(fpath,dirs,1)
    dist_feas_ncs = get_feas(fpath,dirs,2)
    return (dist_feas_hums,dist_feas_nats,dist_feas_ncs)

#read difference images for all or single sites 
def read_difference_abs(fpath):
    hum_chg_locs = []
    ims_blend_diffs = ()
    ims_median_ims = []
    dist_extra_feas = []
    if fpath.endswith('diff'):
        ims_blends,ims_blend_fns = read_blend(fpath)
        ims_blend_hums,ims_blend_nats,ims_blend_ncs = ims_blends
        ims_blend_hum_fns,ims_blend_nat_fns,ims_blend_nc_fns = ims_blend_fns
        print("len of hums,nats,ncs for all sites",len(ims_blend_hums),len(ims_blend_nats),len(ims_blend_ncs))
        if fpath.startswith('all'):
            dirs = next(walk('syn'))[1] #for training
            dirs.sort()
        else:
            dirs = [fpath.split('_',1)[0]]
        #generate difference images for training
        i = 0
        for _,dir in enumerate(dirs):
            max_diff, diff_date_counts, diff_date_names = comp_distribution('../org_json',dir)
            if max_diff < T_THD:
                continue
            print(dir,len(ims_blend_hums[i]),len(ims_blend_nats[i]),len(ims_blend_ncs[i]))
            print(dir,len(ims_blend_hum_fns[i]),len(ims_blend_nat_fns[i]),len(ims_blend_nc_fns[i]))
            ims_sit_blends = (ims_blend_hums[i],ims_blend_nats[i],ims_blend_ncs[i])
            ims_sit_blends_fns = (ims_blend_hum_fns[i],ims_blend_nat_fns[i],ims_blend_nc_fns[i])
            i += 1
            ims_med = cv2.imread("figures_mine/"+dir+"_median.png",cv2.IMREAD_UNCHANGED)
            if ims_med == None:
                print("please create median image for",dir)
                return 
                #ims_med = read_blend_med(ims_blend_hum[_])
                #cv2.imwrite("figures_mine/"+dir+"_median.png",ims_med)
            images_difference_abs('syn_diff/diff_ims/'+dir, ims_med, ims_sit_blends,ims_sit_blends_fns)
    else:
        ims_blend_diffs,ims_median_ims,hum_chg_locs = read_difference_all("syn_diff/diff_ims")
        dist_extra_feas = read_extra_feas('syn')
        
    return ims_blend_diffs,ims_median_ims,hum_chg_locs,dist_extra_feas

#check if human change occurred in add_ims
def ishum(add_ims,row,item):
    for r in row:
        inx = r[1]
        (t,l,h,w) = r[0]
        b = t+h
        r = l+w
        x = item[0]
        y = item[1]
        if t<=x<=b and l<=y<=r and add_ims[inx][x-t,y-r,3] != 0:
            return 0
    return 2

#divide each image here    
def images_diff_hum_sub(ims_blend_diff_hum, ims_median,add_ims, dist_extra_feas_hum, hum_chg_locs,ims_blend_orgs):
    diff_pieces = []
    org_pieces = []
    s_r = W[0]
    s_c = W[1]
    for _,im_diff in enumerate(ims_blend_diff_hum):
        m_org_2d_pieces = []
        k = 0
        row = hum_chg_locs[_]
        row = sorted(row, key=lambda x: x[0][1])
        row0 = [(x[0][0],x[0][1]) for x in row]
        im_org = ims_blend_orgs[_]
        labels = []
        for k1 in range(0,im_diff.shape[0],s_r):
            for k2 in range(0,im_diff.shape[1],s_c):
                b = k1 + W[0]
                r = k2 + W[1]
                im_win = im_diff[k1:b,k2:r,0:3]
                im_win1 = im_org[k1:b,k2:r,0:3]
                im_win2 = ims_median[k1:b,k2:r,0:3]
                im_org_win = im_org[k1:b,k2:r,:]
                if b > im_diff.shape[0]:
                    zos = np.zeros((W[0]-im_org_win.shape[0],im_org_win.shape[1],4))
                    im_org_win = np.vstack((im_org_win,zos))
                    im_win = np.vstack((im_win,zos[:,:,0:3]))
                    im_win1 = np.vstack((im_win1,zos[:,:,0:3]))
                    im_win2 = np.vstack((im_win2,zos[:,:,0:3]))
                if r > im_diff.shape[1]:
                    zos = np.zeros((im_org_win.shape[0],W[1]-im_org_win.shape[1],4))
                    im_org_win = np.hstack((im_org_win,zos))
                    im_win = np.hstack((im_win,zos[:,:,0:3]))
                    im_win1 = np.hstack((im_win1,zos[:,:,0:3]))
                    im_win2 = np.hstack((im_win2,zos[:,:,0:3]))
                labels = []
                for i in range(k1,b):
                    label =[]
                    for j in range(k2,r):
                        l = ishum(add_ims,row,(i,j))
                        label.append(l)
                    labels.append(label)
                for i in range(im_org_win.shape[0]):
                    for j in range(im_org_win.shape[1]):
                        if im_org_win[i][j][3] == 0:
                            im_org_win[i][j] = [0,0,0,0]
                            im_win[i][j] = [0,0,0]
                            im_win1[i][j] = [0,0,0]
                            im_win2[i][j] = [0,0,0]
                    #im_win_tot = np.vstack((im_win,im_win1,im_win2))
                #im_win_tot = np.dstack((im_win,im_win1))
                im_win_tot = im_win1
                im_piece = (im_win_tot,labels)
                ims_blend_diff_pieces.append(im_piece)
                    
                  
def images_diff_nn_sub(ims_blend_diff_nn,ims_median,dist_extra_feas_nn,ims_blend_orgs,tp,aa):
    if tp == 0:
        cs = 1 #nat change
    else:
        cs = 2 #no change        
    for _, im_diff in enumerate(ims_blend_diff_nn):
        im_org = ims_blend_orgs[_]
        for k1 in range(0,im_diff.shape[0],W[0]):
            m_org_pieces = []
            for k2 in range(0,im_diff.shape[1],W[1]):
                b = k1 + W[0]
                r = k2 + W[1]
                im_win = im_diff[k1:b,k2:r,0:3]
                im_win1 = im_org[k1:b,k2:r,0:3]
                im_win2 = ims_median[k1:b,k2:r,0:3]
                im_org_win = im_org[k1:b,k2:r,:]
                if b > im_diff.shape[0]:
                    zos = np.zeros((W[0]-im_org_win.shape[0],im_org_win.shape[1],4))
                    im_org_win = np.vstack((im_org_win,zos))
                    im_win = np.vstack((im_win,zos[:,:,0:3]))
                    im_win1 = np.vstack((im_win1,zos[:,:,0:3]))
                    im_win2 = np.vstack((im_win2,zos[:,:,0:3]))
                if r > im_diff.shape[1]:
                    zos = np.zeros((im_org_win.shape[0],W[1]-im_org_win.shape[1],4))
                    im_org_win = np.hstack((im_org_win,zos))
                    im_win = np.hstack((im_win,zos[:,:,0:3]))
                    im_win1 = np.hstack((im_win1,zos[:,:,0:3]))
                    im_win2 = np.hstack((im_win2,zos[:,:,0:3]))
                labels = []
                for i in range(im_org_win.shape[0]):
                    label = []
                    for j in range(im_org_win.shape[1]):
                        if im_org_win[i][j][3] == 0:
                            label.append(2)
                            im_org_win[i][j] = [0,0,0,0]
                            im_win[i][j] = [0,0,0]
                            im_win1[i][j] = [0,0,0]
                            im_win2[i][j] = [0,0,0]
                        else:
                            label.append(cs)
                    labels.append(label)
                #im_win_tot = np.vstack((im_win,im_win1,im_win2))
                #im_win_tot = np.dstack((im_win,im_win1))
                im_win_tot = im_win1
                im_piece = (im_win_tot,labels)
                ims_blend_diff_pieces.append(im_piece)
                    
def read_ims_info():
    ims_list_info = []
    with open("ims_list_info_chg.pkl","rb") as f:
        ims_list_info = pickle.load(f)
    f.close()
    return ims_list_info

def read_valid_ims():
    valid_diff = []
    valid_json = []
    valid_data = []
    valid_median = []
    labels = []
    dir_diff = "../org_diff"
    dir_json = "../org_json"
    valid_dir = "valid_diff"
    valid_info = [(cv2.imread(join(valid_dir,f),cv2.IMREAD_UNCHANGED),f) for f in listdir(valid_dir) if f.endswith('.tif') or f.endswith('.png')]
    for r_im in valid_info:
        valid_data.append(r_im[0])
        f = r_im[1]
        if '_hum' in f:
            d_name = f.replace('_hum','_org')
            site = f.split('_hum')[0]
            labels.append(0)
        if '_nat' in f:
            d_name = f.replace('_nat','_org')
            site = f.split('_nat')[0]
            labels.append(1)
        if '_nc' in f:
            d_name = f.replace('_nc','_org')
            site = f.split('_nc')[0]
            labels.append(2)
        diff_name = d_name.replace('.png','.tif')
        im_diff = cv2.imread(join(dir_diff,site,diff_name),cv2.IMREAD_UNCHANGED)
        valid_diff.append(im_diff)
        im_median = cv2.imread(join("figures_mine",site+"_median.png"),cv2.IMREAD_UNCHANGED)
        valid_median.append(im_median)
        json_name = d_name.replace('.png','.json')
        with open(join(dir_json,site,json_name)) as j_f:
            d = json.load(j_f)
            valid_json.append(d)
    return valid_data,valid_diff,valid_json,valid_median,labels

def images_diff_validate():
    valid_data,valid_diff,valid_json,valid_median,labels1 = read_valid_ims()
    for _, im_diff in enumerate(valid_diff):
        s_r = W[0]
        s_c = W[1]
        im_org = valid_data[_]
        im_median = valid_median[_]
        for k1 in range(0,im_diff.shape[0],s_r):
            for k2 in range(0,im_diff.shape[1],s_c):
                b = k1 + W[0]
                r = k2 + W[1]
                im_win = im_diff[k1:b,k2:r,0:3]
                im_win1 = im_org[k1:b,k2:r,0:3]
                im_win2 = im_median[k1:b,k2:r,0:3]
                im_org_win = im_org[k1:b,k2:r,:]
                if b > im_diff.shape[0]:
                    zos = np.zeros((W[0]-im_org_win.shape[0],im_org_win.shape[1],4))
                    im_org_win = np.vstack((im_org_win,zos))
                    im_win = np.vstack((im_win,zos[:,:,0:3]))
                    im_win1 = np.vstack((im_win1,zos[:,:,0:3]))
                    im_win2 = np.vstack((im_win2,zos[:,:,0:3]))
                if r > im_diff.shape[1]:
                    zos = np.zeros((im_org_win.shape[0],W[1]-im_org_win.shape[1],4))
                    im_org_win = np.hstack((im_org_win,zos))
                    im_win = np.hstack((im_win,zos[:,:,0:3]))
                    im_win1 = np.hstack((im_win1,zos[:,:,0:3]))
                    im_win2 = np.hstack((im_win2,zos[:,:,0:3]))
                #im_win_tot = np.vstack((im_win,im_win1,im_win2))
                ims_syn_test_org_pieces.append(im_org_win)
                labels = []
                for i in range(im_org_win.shape[0]):
                    label = []
                    for j in range(im_org_win.shape[1]):
                        if im_org_win[i][j][3] == 0:
                            label.append(2)
                            im_org_win[i][j] = [0,0,0,0]
                            im_win[i][j] = [0,0,0]
                            im_win1[i][j] = [0,0,0]
                            im_win2[i][j] = [0,0,0]
                        else:
                            label.append(labels1[_])
                    labels.append(label)
                    #im_win_tot = np.vstack((im_win,im_win1,im_win2))
                #im_win_tot = np.dstack((im_win,im_win1))
                im_win_tot = im_win1
                im_piece = (im_win_tot,labels)
                ims_syn_test_pieces.append(im_piece)
            
#break the each difference image into pieces with equal size and label them
def images_diff_label(add_ims,add_dirs,ims_blend_diffs,ims_median_ims,dist_extra_feas, hum_chg_locs):
    #global n1,n2,n3
    n1 = 0
    n2 = 0
    n3 = 0
    rng = np.random
    ims_blend_diff_hums,ims_blend_diff_nats,ims_blend_diff_ncs = ims_blend_diffs
    dist_extra_feas_hums, dist_extra_feas_nats, dist_extra_feas_ncs = dist_extra_feas
    ims_blends,ims_blend_fns = read_blend('all')
    ims_blend_hums,ims_blend_nats,ims_blend_ncs = ims_blends
    #ims_blend_hums,ims_blend_nats,ims_blend_ncs,ims_blend_hum_fns,ims_blend_nat_fns,ims_blend_nc_fns = read_blend('all')
    for _,ims_blend_diff_hum in enumerate(ims_blend_diff_hums):
        #print(ims_blend_fns[0][_][0],len(dist_extra_feas_hums[_]),len(ims_blend_diff_hum),len(hum_chg_locs[_]))
        images_diff_hum_sub(ims_blend_diff_hum,ims_median_ims[_],add_ims[_],dist_extra_feas_hums[_],hum_chg_locs[_],ims_blend_hums[_])
    for _,ims_blend_diff_nat in enumerate(ims_blend_diff_nats):
        images_diff_nn_sub(ims_blend_diff_nat,ims_median_ims[_],dist_extra_feas_nats[_],ims_blend_nats[_],0,_)
    for _,ims_blend_diff_nc in enumerate(ims_blend_diff_ncs):
        images_diff_nn_sub(ims_blend_diff_nc,ims_median_ims[_],dist_extra_feas_ncs[_],ims_blend_ncs[_],1,_)
    #print("class0/1/2(hum change/nat change/no change):",n1,n2,n3)
    images_diff_validate()
    print(len(ims_blend_diff_pieces),len(ims_syn_test_pieces))
    #print(np.array(ims_syn_test_org_pieces).shape, np.array(dist_ims_test_feas).shape, np.array(ims_syn_test_pieces).shape)
    #print(np.array([p[0] for p in ims_blend_diff_pieces]).shape,np.array(ims_syn_test_pieces[0][0][0]).shape);
    #print("class0/1/2(hum change/nat change/no change):",n1,n2,n3)
    
#write all difference pieces to the disk
def images_diff_pieces_write(ims_pieces = ims_blend_diff_pieces):
    #remove history files from the folder first
    ps = glob.glob("mine_pieces/*")
    for f in ps:
        remove(f)
        
    for _,im in enumerate(ims_pieces):
        #if im[1] ==1:
        fn = 'mine_pieces/image' + str(_) + '.png'
        cv2.imwrite(fn,im[0])
    
def run_mine_syn():
    if sys.argv[1].endswith("syn"):
        read_blend(sys.argv[1])
    elif sys.argv[1].endswith("diff"):
        read_difference_abs(sys.argv[1])
    else:
        add_ims, add_dirs = load_add_ims(sys.argv[1])
        ims_blend_diffs,ims_median_ims,hum_chg_locs,dist_extra_feas = read_difference_abs(sys.argv[1])
        images_diff_label(add_ims, add_dirs,ims_blend_diffs, ims_median_ims,dist_extra_feas, hum_chg_locs)
        #images_diff_pieces_write(ims_blend_diff_pieces)
    
if __name__ == '__main__':
    run_mine_syn()         




