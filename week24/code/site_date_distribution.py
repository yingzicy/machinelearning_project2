import numpy as np
import sys
from os import listdir,remove,makedirs,walk
from os.path import isfile,join,exists
import json
import matplotlib.pyplot as plt

def comp_distribution(data_dir,dir):
    fs = [f for f in listdir(join(data_dir,dir)) if f.endswith('.json')]
    fn_ym_ims = []
    for f in fs:
        with open(join(data_dir,dir,f)) as j_f:
            d_f = json.load(j_f)
        date = d_f["properties"]["acquired"]
        fn_ym_ims.append(np.int(date[0:7].replace('-','')))
    diff_date_names,diff_date_counts = np.unique(fn_ym_ims,return_counts=True)
    if (len(diff_date_names) == 0):
        return 0,[],[]
    beg = str(diff_date_names[0])
    b_y = int(beg[0:4])
    b_m = int(beg[4:-1])
    end = str(diff_date_names[-1])
    e_y = int(end[0:4])
    e_m = int(end[4:-1])
    max_diff = (e_y-b_y)*12 + (e_m-b_m)
    
    return max_diff, diff_date_counts, diff_date_names

def plot_date_dist(dir, counts, vals):
    save_dir = "output_date_dist"
    x = np.arange(len(counts))
    fig, ax = plt.subplots()
    #print(dir,len(x),len(counts))
    #plt.hist([item for item in l_m if item != 0], 255)
    plt.plot(x,counts,'ro-',linewidth = 1)
    labels = vals
    fig.figsize=(1000,1000)
    fig.subplots_adjust(bottom=0.3)
    ax.set_xticklabels(labels,rotation =90, size=7)
    """else:
        ax.set_xticklabels(labels,rotation = 45)"""
    plt.xticks(x)
    if dir == 'max_diff_dist':
        plt.xlabel('all sites')
        plt.ylabel('max difference')
    else:
        plt.xlabel(dir+'_dates')
        plt.ylabel('frequence')
    fn = join(save_dir,dir + ".png")
    fig.savefig(fn)
    plt.close(fig)

def show_date_dist():
    data_dir = '../org_json'
    dirs = next(walk(data_dir))[1]
    max_diffs = []
    max_dirs = []
    for dir in dirs:
        max_diff,diff_date_counts,diff_date_names = comp_distribution(data_dir,dir)
        if (len(diff_date_counts) != 0):
            plot_date_dist(dir,diff_date_counts,diff_date_names)
            max_diffs.append(max_diff)
            max_dirs.append(dir)
    plot_date_dist("max_diff_dist",max_diffs,max_dirs)
    
if __name__ == '__main__':
    show_date_dist()
