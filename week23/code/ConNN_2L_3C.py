from NN_2L import *
import theano.tensor.signal.downsample
#define convlution network layer
class ConNetLayer(object):
    def __init__(self, input, image_shape, filter_shape, rng, S = (1,1), P = 0, pool_size = (2,2), activation = T.nnet.relu):
        assert image_shape[1] == filter_shape[1]
        e = np.sqrt( 1. / (filter_shape[0] + filter_shape[1]))
        W_value = rng.uniform(low = -e, high = e, size = filter_shape)
        b_value = rng.uniform(low = -e, high = e, size = ((filter_shape[0],)))
        self.W = theano.shared(value = np.asarray(W_value, dtype = theano.config.floatX), name = 'W', borrow = True)
        self.b = theano.shared(value = np.asarray(b_value, dtype = theano.config.floatX), name = 'W', borrow = True)
        self.params = [self.W, self.b]
        self.L2_reg = T.sum(self.W **2) + T.sum(self.b **2)
        #output shape for conv2d is (input_size,filter_size,out_rows,out_cols)
        co = T.nnet.conv2d(input, self.W, subsample = S, border_mode = P) #convolution oper
        #The output has the same dimensions as the input for pooling
        po = T.signal.downsample.max_pool_2d(co, pool_size) #pooling oper

        #
        self.out = activation(po + self.b.dimshuffle('x', 0, 'x', 'x')) #activation

        

#define convolution neural network
class ConNN(object):
    """includes conv layer and mlp layer
    conv layer includes conv, pooling, flatting steps"""
    def __init__(self, input, y, x_mlp, image_shape, filter_shape, S, P, n_in, n_hidden, n_out, rng, pool_size = (2,2)):
        #create connetlayer instance1
        self.cnlayer1 = ConNetLayer(
            input = input,
            image_shape = image_shape[0],
            filter_shape = filter_shape[0],
            rng = rng,
            S = S[0],
            P = P[0],
            pool_size = pool_size,
            activation = T.nnet.relu
        )
        
        #create connetlayer instance2
        self.cnlayer2 = ConNetLayer(
            input = self.cnlayer1.out,
            image_shape = image_shape[1],
            filter_shape = filter_shape[1],
            rng = rng,
            S = S[1],
            P = P[1],
            pool_size = pool_size,
            activation = T.nnet.relu
        )
        
        #create connetlayer instance3
        self.cnlayer3 = ConNetLayer(
            input = self.cnlayer2.out,
            image_shape = image_shape[2],
            filter_shape = filter_shape[2],
            rng = rng,
            S = S[2],
            P = P[2],
            pool_size = pool_size,
            activation = T.nnet.relu
        )
         
        #create MLP layer
        self.mlp = MLP(
            input = T.concatenate([self.cnlayer3.out.flatten(2),x_mlp],axis=1),
            y = y,
            n_in = n_in,
            n_hidden = n_hidden,
            n_out = n_out,
            rng = rng
        )
        self.params = self.cnlayer1.params + self.cnlayer2.params + self.cnlayer3.params +self.mlp.params
        self.L2_reg = self.cnlayer1.L2_reg + self.cnlayer2.L2_reg + self.cnlayer3.L2_reg + self.mlp.L2_reg
        self.y_pred = self.mlp.y_pred
        self.errors = self.mlp.errors
        self.cnnout1 = self.cnlayer1.out
        self.cnnout2 = self.cnlayer2.out
        self.cnnout3 = self.cnlayer3.out
        self.neg_log_likelihood = self.mlp.neg_log_likelihood
        self.rng = rng
        self.input = input
        self.y_p = self.mlp.y_p
