import cv2
import numpy as np
from os import listdir,remove,walk
from os.path import isfile,join
import glob,sys,copy,json
import pickle
from mine_synthetic_abs import get_feas_from_json

#get pixels from each image
mine_diff_pieces = []
mine_org_pieces = []
test_dist_feas = []
N_CHG = 2
W = (64,64)
shift = (64,64)
"""W = (48,48)
shift = (48,48)"""
all_sites_org_ims_names = []            
#load testing original images and files
def load_data():
    #generate file list
    org_ims = []
    median_ims = []
    diff_ims = []
    dist_extra_feas = []
    dpath = "../data"
    root1 = 'test_diff/org_ims'
    root3 = 'test_diff/json_test'
    dirs1 = next(walk(root1))[1]
    dirs3 = next(walk(root3))[1]
    org_dirs = [dir for dir in dirs1]
    jason_dirs = [dir for dir in dirs3]
    org_dirs.sort()
    jason_dirs.sort()
    for dir in org_dirs:
        mypath = join(root1,dir)
        fs = [ f for f in listdir(mypath) if isfile(join(mypath,f)) and (f.endswith(".tif") or f.endswith(".png"))]
        fs.sort()
        #generate original image names for each image of each site
        dirpath = join(dpath,dir)
        fs_org = [f for f in listdir(dirpath) if isfile(join(dirpath,f)) and (f.endswith('.tif') or f.endswith('.png')) and 'analytic' not in f]
        fs_org.sort();
        for f in fs:
            #generate original image names for each image of each site
            fno = f.split("org")[1].split(".")[0];
            i_org_name = (dir,fs_org[int(fno)],f,fno)
            all_sites_org_ims_names.append(i_org_name)
        #################################################
        oims = [cv2.imread(join(mypath,f), cv2.IMREAD_UNCHANGED) for f in fs if 'analytic' not in f]
        for im in oims:
            if im == None or im.shape[2] < 4:
                print("channels less than 4 occurs:",f)
                continue
            org_ims.append(im)
    for _,dir in enumerate(org_dirs):
        fp = join(root1,dir)
        fns = [f for f in listdir(fp) if isfile(join(fp,f)) and (f.endswith('.tif') or f.endswith('.png'))]
        fns.sort()
        root_src = join("../org_diff",dir)
        im_median = cv2.imread(join("figures_mine",dir+"_median.png"),cv2.IMREAD_UNCHANGED)
        for f in fns:
            fn = join(root_src,f)
            im = cv2.imread(fn,cv2.IMREAD_UNCHANGED)
            diff_ims.append(im)
            median_ims.append(im_median)
    for dir in jason_dirs:
        mypath = join(root3,dir)
        fs = [ f for f in listdir(mypath) if isfile(join(mypath,f)) and f.endswith('.json')]
        fs.sort()
        for f in fs:
            fn = join(mypath,f)
            with open(fn) as j_f:
                d = json.load(j_f)
            feas = get_feas_from_json(d)
            dist_extra_feas.append(feas)
    print("test data amount:",len(org_ims),np.array(diff_ims).shape,len(dist_extra_feas))
    return org_ims,diff_ims,dist_extra_feas,median_ims
                                         
                                         
def split_diff(org_ims,i_ims_syn,test_feas,median_ims):
    #split difference images into (64,64)blocks
    """cv2.imwrite("ims35_2.png",i_ims_syn[35])
    cv2.imwrite("ims35_1.png",org_ims[35])
    cv2.imwrite("ims49_2.png",i_ims_syn[49])
    cv2.imwrite("ims49_1.png",org_ims[49])"""

    for _,im in enumerate(i_ims_syn):
        if im == None:
            continue
        m_pieces = []
        m_org_2d_pieces = []
        im_org = np.copy(org_ims[_])
        im_median = np.copy(median_ims[_])
        fea_pieces = []
        for i in range(0,im.shape[0],W[0]):
            m_org_pieces = []
            for j in range(0,im.shape[1],W[1]):
                b = i + W[0]
                r = j + W[1]
                """if b > im.shape[0] or r > im.shape[1]:
                    continue"""
                fea_pieces.append(test_feas[_])
                m_org_piece = im_org[i:b,j:r,:]
                im_win = im[i:b,j:r,0:3]
                im_win1 = im_org[i:b,j:r,0:3]
                im_win2 = im_median[i:b,j:r,0:3]
                im_win_tot = np.vstack((im_win,im_win1,im_win2))
                m_piece = (im_win_tot,N_CHG)
                if b > im.shape[0]:
                    zos = np.zeros((W[0]-m_org_piece.shape[0],m_org_piece.shape[1],4))
                    m_org_piece = np.vstack((m_org_piece,zos))
                    im_win = np.vstack((im_win,zos[:,:,0:3]))
                    im_win1 = np.vstack((im_win1,zos[:,:,0:3]))
                    im_win2 = np.vstack((im_win2,zos[:,:,0:3]))
                if r > im.shape[1]:
                    zos = np.zeros((m_org_piece.shape[0],W[1]-m_org_piece.shape[1],4))
                    m_org_piece = np.hstack((m_org_piece,zos))
                    im_win = np.hstack((im_win,zos[:,:,0:3]))
                    im_win1 = np.hstack((im_win1,zos[:,:,0:3]))
                    im_win2 = np.hstack((im_win2,zos[:,:,0:3]))
                if r > im.shape[1] or b > im.shape[0]:
                    im_win_tot = np.vstack((im_win,im_win1,im_win2))
                    m_piece = (im_win_tot,N_CHG)
                m_pieces.append(m_piece)
                m_org_pieces.append(m_org_piece)
            if len(m_org_pieces) > 0:
                m_org_2d_pieces.append(m_org_pieces)
        mine_diff_pieces.append(m_pieces)
        test_dist_feas.append(fea_pieces)
        if len(m_org_2d_pieces) > 0:
            mine_org_pieces.append(m_org_2d_pieces)
    #print(len(mine_diff_pieces),len(test_dist_feas),len(mine_org_pieces))


def write_diff_pieces():
    n = 0
    fs = glob.glob("mine_diff_pieces/*")
    for f in fs:
        remove(f)
    for ips in mine_diff_pieces[49:50]:
        for i in range((len(ips))):
            fn = "mine_diff_pieces/im50_piece" + str(n) + ".png"
            cv2.imwrite(fn,ips[i][0])
            n += 1
    for ips in mine_diff_pieces[52:53]:
        for i in range((len(ips))):
            fn = "mine_diff_pieces/im53_piece" + str(n) + ".png"
            cv2.imwrite(fn,ips[i][0])
            n += 1
    
def run_mine_diff_out():
    org_ims,diff_ims,dist_extra_feas,median_ims = load_data() 
    split_diff(org_ims,diff_ims,dist_extra_feas,median_ims)
    #write_diff_pieces()
    
if __name__ == '__main__':
    run_mine_diff_out()

