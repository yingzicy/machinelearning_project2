import numpy as np
from os import listdir,walk
from os.path import isfile,join
import sys,cv2,json,pickle

ims_median_info = []
PER_DAY = 0.2
def calc_median(root,site,images):
   #get pixels from each image
    mypath = join(root,site)
    ims = []
    flag = 0
    for f in images:
        pf = join(mypath,f)
        im = cv2.imread(pf, cv2.IMREAD_UNCHANGED)
        if im == None or im.shape[2] < 4 or 'analytic' in f:
            continue
        imf = np.asarray(im, dtype = np.float)
        for i in range(im.shape[0]):
            for j in range(im.shape[1]):
                if imf[i,j,3] == 0:
                    imf[i,j,:] = (np.nan,np.nan,np.nan,np.nan)
        ims.append(imf)
    print(site)
    #compute the median of all the images
    ims_medianf = np.nanmedian(ims, axis = 0)
    ims_median = ims_medianf.astype(np.uint8)
    for i in range(ims_median.shape[0]):
        for j in range(ims_median.shape[1]):
            if ims_median[i,j,3] == np.nan:
                ims_median[i,j,:] = [0,0,0,0]
    return ims_median[:,:,0:3]

def save_ims_median_info(ims_median_info):
    print(len(ims_median_info))
    with open("ims_median_info.pkl","wb") as f:
        pickle.dump(ims_median_info,f)
    f.close()
    
if __name__ == '__main__':
    root = "../data"
    sites = next(walk(root))[1]
    for site in sites:
        fn_ims = []
        fn_ym_ims = []
        fn_y_ims = []
        fn_m_ims = []
        fs = [f for f in listdir(join(root,site)) if (f.endswith(".tif") or f.endswith(".png")) and 'analytic' not in f]
        fs.sort()
        if len(fs) == 0:
            continue
        for f in fs:
            fp = join(root,site,f)
            j_f = f.split("_visual")[0]
            j_fp = join(root,site,j_f) + ".json"
            im = cv2.imread(fp, cv2.IMREAD_UNCHANGED)
            if im == None or im.shape[2] < 4:
                continue
            if site != 'mining':
                with open(j_fp,"r") as j_fn:
                    d = json.load(j_fn)
                j_fn.close()
                if d["properties"]["cloud_cover"] > 0.5:
                    continue
            fn_ims.append(f)
            fn_ym_ims.append(np.int(f[0:6]))
            fn_y_ims.append(np.int(f[0:4]))
            fn_m_ims.append(np.int(f[4:6]))
        #print(site,fn_ims)
        nums,counts = np.unique(fn_ym_ims,return_counts=True)
        y_dist = fn_y_ims[-1]-fn_y_ims[0]
        if y_dist == 0 and len(nums) <= 12:
            continue
        print("nums counts",nums,counts)
        per_days_cnt_ear = np.int(np.sum(counts)*PER_DAY)
        per_days_cnt_lat = np.int(np.sum(counts)*PER_DAY)
        #calc period for ear and lat
        end = 0
        beg = 0
        ear_beg = nums[0]
        lat_end = nums[-1]
        for _,c in enumerate(counts):
            end += c
            if end >= per_days_cnt_ear:
                ear_end = nums[_]
                break
        for _,c in enumerate(counts[::-1]):
            beg += c
            if beg >= per_days_cnt_lat:
                lat_beg = nums[len(nums)-1-_]
                break
        ims_median_info.append({'site':site,'period':(ear_beg,ear_end,lat_beg,lat_end)})
        print(per_days_cnt_ear,per_days_cnt_lat,per_days_cnt_ear, per_days_cnt_lat, ear_beg,ear_end,lat_beg,lat_end)
        im_med_days_ear = calc_median(root,site,fn_ims[:per_days_cnt_ear])
        im_med_days_lat = calc_median(root,site,fn_ims[-per_days_cnt_lat-1:])
        des_root = 'ear_lat_median_ims/'
        cv2.imwrite(des_root+site+'_median_ear.png',im_med_days_ear)
        cv2.imwrite(des_root+site+'_median_lat.png',im_med_days_lat)
    save_ims_median_info(ims_median_info)
    
