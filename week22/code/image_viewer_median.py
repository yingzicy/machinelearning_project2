import numpy as np
import os, sys
import pickle,glob
import cv2
from os import listdir,remove
from os.path import join,isfile
from tkinter import *
from tkinter import messagebox
from PIL import Image, ImageTk

ims_med_ear = []
ims_med_lat = []
ims_median_info = []
current = 0
W_THD = 500
H_THD = 500
S_THD = 4
S_THD1 = 4
    
def get_img_list():
    median_ear_dir = 'ear_lat_median_ims'
    median_lat_dir = 'ear_lat_median_ims'
    #read median images
    f_meds_ear = [f for f in listdir(median_ear_dir) if isfile(join(median_ear_dir,f)) and f.endswith('ear.png')]
    f_meds_ear.sort()
    f_meds_lat = [f for f in listdir(median_lat_dir) if isfile(join(median_lat_dir,f)) and f.endswith('lat.png')]
    f_meds_lat.sort()
    for f in f_meds_ear:
        im = join(median_ear_dir,f)
        ims_med_ear.append(im)
    for f in f_meds_lat:
        im = join(median_lat_dir,f)
        ims_med_lat.append(im)
        
def viewer_func():
    #global label1,label2
    prev_img = ImageTk.PhotoImage(file="prev_arrow.png")
    next_img = ImageTk.PhotoImage(file="next_arrow.png")
    exit_img = ImageTk.PhotoImage(file="exit.png")
    b_prev = Button(frame, command=prev)
    b_prev.config(image=prev_img)
    b_prev.image = prev_img
    b_prev.grid(row=710)
    b_prev.pack()
    b_next = Button(frame,command=next)
    b_next.config(image=next_img)
    b_next.image = next_img
    b_next.grid(row=710, column=1)
    b_next.pack()
    b_exit = Button(frame,command=root.quit)
    b_exit.config(image=exit_img)
    b_exit.image=exit_img
    b_exit.grid(row=710, column=2)
    b_exit.pack()
    
def prev():
    global current
    if not (0 <= current-1 < len(ims_med_ear)):
        messagebox.showinfo('End','No more image.')
        return
    current -= 1
    med_info = ims_median_info[current]
    ear_beg,ear_end,lat_beg,lat_end = med_info["period"]
    #l_top = Label(frame,text="site:"+med_info["site"]+";"+"period:"+"early("+str(ear_beg)+","+str(ear_end)+")"+";late("+str(lat_beg)+","+str(lat_end)+")")
    text="site:"+med_info["site"]+";"+" period:"+" early("+str(ear_beg)+"-"+str(ear_end)+")"+"; late("+str(lat_beg)+"-"+str(lat_end)+")"
    l_top.configure(text = text)
    l_top.text=text
    im_pi1,im_med_pi1 = get_im_and_med()
    #print("prev",im,im_med)
    label1.configure(image=im_pi1)
    label1.image=im_pi1
    label2.configure(image=im_med_pi1)
    label2.image=im_med_pi1
    
def next():
    global current
    if not (0 <= current+1 < len(ims_med_ear)):
        messagebox.showinfo('End','No more image.')
        return
    current += 1
    med_info = ims_median_info[current]
    ear_beg,ear_end,lat_beg,lat_end = med_info["period"]
    #l_top = Label(frame,text="site:"+med_info["site"]+";"+"period:"+"early("+str(ear_beg)+","+str(ear_end)+")"+";late("+str(lat_beg)+","+str(lat_end)+")")
    text = "site:"+med_info["site"]+";"+" period:"+" early("+str(ear_beg)+"-"+str(ear_end)+")"+"; late("+str(lat_beg)+"-"+str(lat_end)+")"
    l_top.configure(text = text)
    l_top.text=text
    im_pi1,im_med_pi1 = get_im_and_med()
    label1.configure(image=im_pi1)
    label1.image=im_pi1
    label2.configure(image=im_med_pi1)
    label2.image=im_med_pi1
    
def read_median_info():
    global ims_median_info
    with open("ims_median_info.pkl","rb") as f:
        ims_median_info = pickle.load(f)
    f.close()
    
def get_im_and_med():
    im,im_med = ims_med_ear[current],ims_med_lat[current]
    print(im,im_med)
    image = Image.open(im)
    w,h = image.size
    if w>W_THD or h>H_THD:
        image = image.resize((int(w/S_THD),int(h/S_THD)), Image.ANTIALIAS)
        w = int(w/S_THD)
        h = int(h/S_THD)
    im_pi1 = ImageTk.PhotoImage(image)
    image_med = Image.open(im_med)
    w1,h1 = image_med.size
    if w1>W_THD or h1>H_THD:
        image_med = image_med.resize((int(w1/S_THD),int(h1/S_THD)), Image.ANTIALIAS)
        w1 = int(w/S_THD)
        h1 = int(h/S_THD)
    im_med_pi1 = ImageTk.PhotoImage(image_med)
    return im_pi1, im_med_pi1

def onFrameConfigure(event):
    canvas.configure(scrollregion=canvas.bbox("all"))
    
read_median_info()
get_img_list()
#define a canvas and put a frame in the canvas
root = Tk()
root.title('image viewer')
canvas = Canvas(root, borderwidth=0,width=600,height=600)
#frame = Frame(canvas, background = "#ffffff")
frame = Frame(canvas)
vsb = Scrollbar(root,orient="vertical",command = canvas.yview)
canvas.configure(yscrollcommand = vsb.set)
vsb.pack(side="right",fill="y")
canvas.pack(side="left",fill="both",expand=True)
canvas.create_window((4,4),window=frame,anchor="nw",tags="frame")
frame.bind("<Configure>",onFrameConfigure)
med_info = ims_median_info[current]
ear_beg,ear_end,lat_beg,lat_end = med_info["period"]
l_top = Label(frame,text="site:"+med_info["site"]+";"+" period:"+" early("+str(ear_beg)+"-"+str(ear_end)+")"+"; late("+str(lat_beg)+"-"+str(lat_end)+")")
l_top.pack()
im_pi,im_med_pi = get_im_and_med()
print(im_pi,im_med_pi)
label1 = Label(frame, image=im_pi)
label1.pack()
label2 = Label(frame, image=im_med_pi)
label2.pack()
viewer_func()
root.mainloop() # wait until user clicks the window
