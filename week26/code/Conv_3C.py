from LR import *
import theano.tensor.signal.downsample
from theano.sandbox.cuda.dnn import dnn_conv
from theano import function,printing

#define convlution network layer
class ConvLayer(object):
    def __init__(self, input, filter_shape, rng, S , P = 'full', pool_size = (2,2), activation = T.nnet.relu):
        #assert input.shape[1] == filter_shape[1]
        e = np.sqrt( 1. / (filter_shape[0] + filter_shape[1]))
        W_value = rng.uniform(low = -e, high = e, size = filter_shape)
        b_value = rng.uniform(low = -e, high = e, size = ((filter_shape[0],)))
        self.W = theano.shared(value = np.asarray(W_value, dtype = theano.config.floatX), name = 'W', borrow = True)
        self.b = theano.shared(value = np.asarray(b_value, dtype = theano.config.floatX), name = 'W', borrow = True)
        self.params = [self.W, self.b]
        self.L2_reg = T.sum(self.W **2) + T.sum(self.b **2)
        
        #conv(X,w,b,activation)
        # co = dnn_conv(X, w, border_mode=int(np.floor(w.get_value().shape[-1]/2.)))
        s = int(np.floor(self.W.get_value().shape[-1]/2.))
        co = T.nnet.conv2d(input,self.W,border_mode=P)[:,:,s:-s,s:-s]
        #co = T.nnet.conv2d(input, self.W, sub_sample = S, border_mode = P) #convolution oper
        if self.b is not None:
            co += self.b.dimshuffle('x',0,'x','x')
        co = activation(co)
        self.out = co

#define convolution neural network
class Conv(object):
    """includes conv layer
    conv layer includes conv, pooling, flatting steps"""
    def __init__(self, input, y, filter_shape, S, P, n_in, n_out, rng, pool_size):
        #create convlayer instance1
        self.cnlayer1 = ConvLayer(
            input = input,
            filter_shape = filter_shape[0],
            rng = rng,
            S = S[0],
            P = P[0],
            pool_size = pool_size[0],
            activation = T.nnet.relu
        )
        #create convlayer instance2
        self.cnlayer2 = ConvLayer(
            input = self.cnlayer1.out,
            filter_shape = filter_shape[1],
            rng = rng,
            S = S[1],
            P = P[1],
            pool_size = pool_size[1],
            activation = T.nnet.relu
        )
        
        #create convlayer instance3
        self.cnlayer3 = ConvLayer(
            input = self.cnlayer2.out,
            filter_shape = filter_shape[2],
            rng = rng,
            S = S[2],
            P = P[2],
            pool_size = pool_size[2],
            activation = T.nnet.relu
        )

        #create convlayer instance4
        self.cnlayer4 = ConvLayer(
            input = self.cnlayer3.out,
            filter_shape = filter_shape[3],
            rng = rng,
            S = S[3],
            P = P[3],
            pool_size = pool_size[3],
            activation = T.nnet.relu
        )
        #softmax
        in_shape = self.cnlayer4.out.shape
        in_shape1 = (in_shape[0],in_shape[2],in_shape[3],in_shape[1])
        self.lrlayer = LogisticRegression(
            input = T.reshape(self.cnlayer4.out,in_shape1),
            n_in = n_in,
            n_out = n_out,
            rng = rng
            )
        
        self.params = self.cnlayer1.params+self.cnlayer2.params+self.cnlayer3.params+self.cnlayer4.params+self.lrlayer.params
        
        self.L2_reg = self.cnlayer1.L2_reg+self.cnlayer2.L2_reg+self.cnlayer3.L2_reg+self.cnlayer4.L2_reg+self.lrlayer.L2_reg
        
        self.y_pred = self.lrlayer.y_pred
        self.errors = self.lrlayer.errors(y)
        self.caeout1 = self.cnlayer1.out
        self.caeout2 = self.cnlayer2.out
        self.caeout3 = self.cnlayer3.out
        self.caeout4 = self.cnlayer4.out
        self.neg_log_likelihood = self.lrlayer.negative_log_likelihood(y)
        self.input = input
        self.y_p = self.lrlayer.y_p
        self.input_shape = self.lrlayer.input_shape
        self.y_p_1d_shape = self.lrlayer.y_p_1d_shape
        self.y_p_shape = self.lrlayer.y_p_shape
        self.y_pred_shape = self.lrlayer.y_pred_shape
