import sys
from os import listdir,remove,makedirs,walk
from os.path import isfile,join,exists
import numpy as np
import numpy.random as rng
import cv2,glob,pickle,copy

noisy_num = 6
noisy_val = 255
shape_num = 3

def read_local_mnist_data(src):
    ims = []
    fs = [f for f in listdir(src) if f.endswith('.png') or f.endswith('.jpg')]
    fs.sort()
    for f in fs:
        im = cv2.imread(join(src,f),cv2.IMREAD_UNCHANGED)
        ims.append(im)
    return ims,fs

def write_chg_data(des,d_arr,fns):
    if not exists(des):
        makedirs(des)
    ds = glob.glob(join(des,"*"))
    for f in ds:
        remove(f)
    """for _,im in enumerate(d_arr):
        if _ < 10:
            cv2.imwrite(join(des,'image' + str(0) + str(_) + '.png'),im)
        else:
            cv2.imwrite(join(des,'image' + str(_) + '.png'),im)"""
    for _,im in enumerate(d_arr):
        cv2.imwrite(join(des,fns[_]),im)
            
def gen_noisy_ims(ims,fs):
    rng = np.random
    for _,im in enumerate(ims):
        #creae noisy
        for i in range(noisy_num):
            x = rng.randint(im.shape[0])
            y = rng.randint(im.shape[1])
            if im[x,y] == 0:
                im[x,y] = 255
            elif im[x,y] > 0:
                im[x,y] = 0
        #create change
        c = rng.randint(shape_num)
        x1,y1,x2,y2 = rng.randint(im.shape[0]),rng.randint(im.shape[1]),rng.randint(im.shape[0]),rng.randint(im.shape[1])
        if c == 0: #Drawing Line
            chg_col = rng.randint(256)
            cv2.line(im,(x1,y1),(x2,y2),(chg_col),3)
        elif c == 1: #Drawing Rectangle
            chg_col = rng.randint(256)
            cv2.rectangle(im,(x1,y1),(x2,y2),(chg_col),2)
        elif c == 2: #Drawing Circle
            chg_col = rng.randint(256)
            cv2.circle(im,(x1,y1), rng.randint(1,8), (chg_col), -1)
    write_chg_data(sys.argv[2],ims,fs)    
    
if __name__ == '__main__':
    ims,fs = read_local_mnist_data(sys.argv[1])
    gen_noisy_ims(ims,fs)
