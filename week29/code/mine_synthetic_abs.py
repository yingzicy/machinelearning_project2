from six.moves import cPickle
import sys
from os import listdir,remove,makedirs,walk
from os.path import isfile,join,exists
import numpy as np
import numpy.random as rng
import cv2,glob,pickle,copy
#import tensorflow.examples.tutorials.mnist.input_data as input_data

ims_blend_diff_pieces = []
ims_syn_test_org_pieces = []
ims_syn_test_pieces = []
valid_org_img_xy = []
mine_diff_pieces = []
mine_org_pieces = []
test_org_img_xy = []
fs_train_X_org = []
fs_valid_X_org = []
fs_test_X_org = []
fs_train_X_chg = []
fs_valid_X_chg = []
fs_test_X_chg = []
W = (32,32)
W_IMG = (28,28)
                                 
def images_diff_sub(ims_org,ims_chg,tp,des):
    ims_diff = []
    for _, im in enumerate(ims_chg):
        im_org = im.reshape(W_IMG[0],W_IMG[1],1)
        im_diff = (abs(im-ims_org[_])*1.0/255).reshape(W_IMG[0],W_IMG[1],1)
        ims_diff.append(im_diff*255)
        org_img_xy = []
        for k1 in range(0,im_diff.shape[0],W[0]):
            m_org_pieces = []
            org_img_x = []
            for k2 in range(0,im_diff.shape[1],W[1]):
                b = k1 + W[0]
                r = k2 + W[1]
                im_win = im_diff[k1:b,k2:r,:]
                im_win1 = im_org[k1:b,k2:r,:]
                im_org_win = im_org[k1:b,k2:r,:]
                if b > im_diff.shape[0]:
                    zos = np.zeros((W[0]-im_org_win.shape[0],im_org_win.shape[1],1))
                    im_org_win = np.vstack((im_org_win,zos))
                    im_win = np.vstack((im_win,zos[:,:,:]))
                    im_win1 = np.vstack((im_win1,zos[:,:,:]))
                if r > im_diff.shape[1]:
                    zos = np.zeros((im_org_win.shape[0],W[1]-im_org_win.shape[1],1))
                    im_org_win = np.hstack((im_org_win,zos))
                    im_win = np.hstack((im_win,zos[:,:,:]))
                    im_win1 = np.hstack((im_win1,zos[:,:,:]))
                labels = []
                for i in range(im_org_win.shape[0]):
                    label = []
                    for j in range(im_org_win.shape[1]):
                        if im_win[i][j] == 0:
                            label.append(1)
                        else:
                            label.append(0)
                    labels.append(label)
                #im_win_tot = np.vstack((im_win,im_win1,im_win2))
                #im_win_tot = np.dstack((im_win,im_win1))
                im_win_tot = im_win
                im_piece = (im_win_tot,labels)
                org_img_x.append(1)
                if tp == 0:
                    ims_blend_diff_pieces.append(im_piece)
                elif tp == 1:
                    ims_syn_test_pieces.append(im_piece)
                    ims_syn_test_org_pieces.append(im_org_win)
                else:
                    mine_diff_pieces.append(im_piece)
                    mine_org_pieces.append(im_org_win)
            org_img_xy.append(np.array(org_img_x))
        if tp == 1:
            valid_org_img_xy.append(np.array(org_img_xy))
        if tp == 2:
            test_org_img_xy.append(np.array(org_img_xy))
    #write_original_data(des,ims_diff)
    
def read_mnist_data():
    m = input_data.read_data_sets("MNIST")
    return m

def read_local_mnist_data(src):
    ims = []
    fs = [f for f in listdir(src) if f.endswith('.png') or f.endswith('.jpg')]
    for f in fs:
        im = cv2.imread(join(src,f),cv2.IMREAD_UNCHANGED)
        ims.append(im)
    return ims,fs
    
def write_original_data(des,d_arr):
    if not exists(des):
        makedirs(des)
    fs = glob.glob(join(des,"*.png"))
    for f in fs:
        remove(f)
    for _,im in enumerate(d_arr):
        im = im.reshape(28,28,1)
        if _ < 10:
            cv2.imwrite(join(des,'image' + str(0) + str(_) + '.png'),im)
        else:
            cv2.imwrite(join(des,'image' + str(_) + '.png'),im)
        
#break the each difference image into pieces with equal size and label them
def images_diff_label():
    #global n1,n2,n3
    rng = np.random
    m_train_X_org,fs_train_X_org[:] = read_local_mnist_data('mnist_data/train')
    m_valid_X_org,fs_valid_X_org[:] = read_local_mnist_data('mnist_data/valid')
    m_test_X_org, fs_test_X_org[:] = read_local_mnist_data('mnist_data/test')
    m_train_X_chg,fs_train_X_chg[:] = read_local_mnist_data('mnist_chg_data/train')
    m_valid_X_chg,fs_valid_X_chg[:] = read_local_mnist_data('mnist_chg_data/valid')
    m_test_X_chg,fs_test_X_chg[:] = read_local_mnist_data('mnist_chg_data/test')
    images_diff_sub(m_train_X_org,m_train_X_chg,0,"mnist_diff/train")
    images_diff_sub(m_valid_X_org,m_valid_X_chg,1,"mnist_diff/valid")
    images_diff_sub(m_test_X_org,m_test_X_chg,2,"mnist_diff/test")
    """write_original_data('mnist_data/train',m_train_X)
    write_original_data('mnist_data/valid',m_valid_X)
    write_original_data('mnist_data/test',m_test_X)"""
    
#write all difference pieces to the disk
def images_diff_pieces_write(ims_pieces = ims_blend_diff_pieces):
    #remove history files from the folder first
    ps = glob.glob("mine_pieces/*")
    for f in ps:
        remove(f)
        
    for _,im in enumerate(ims_pieces):
        #if im[1] ==1:
        fn = 'mine_pieces/image' + str(_) + '.png'
        cv2.imwrite(fn,im[0])
    
def run_mine_syn():
    if sys.argv[1].endswith("syn"):
        read_blend(sys.argv[1])
    elif sys.argv[1].endswith("diff"):
        read_difference_abs(sys.argv[1])
    else:
        images_diff_label()
        #images_diff_pieces_write(ims_blend_diff_pieces)
    
if __name__ == '__main__':
    images_diff_label()




