from six.moves import cPickle
import sys
from os import listdir,remove
from os.path import isfile,join
import numpy as np
import numpy.random as rng
import cv2
import glob
import pickle
import copy
from mine_images_median_diff_abs import comp_diff_val_distribution,comp_ims_median
#from mine_images_median_diff_abs import *
dist_features = []
ims_blend_hum = []
ims_blend_nat = []
ims_blend_nc = []
ims_blend_nat_diff = []
ims_blend_hum_diff = []
ims_blend_nc_diff = []
ims_blend_diff_pieces = []
dist_ims_feas = []
hum_chg_locs = []
dist_feas = []
ims_syn_test_org = []
ims_syn_test_pieces = []
dist_ims_test_feas = []
ims_blend_hum_all = []
ims_blend_nat_all = []
ims_blend_nc_all = []
ims_blend_add_org = []
W = (64,64)
shift = (20,20)
NC_THD = 5
SYN_TEST_IMG = 0
IMG_SHAPE = (64,64)
ROW_DIFF = 150

#load images from cifar-10
def load_data_syn(fpath):
    """load orginal image like mine_mean, hum and nat images"""
    if fpath == "mine_syn" or fpath == "mine_syn_diff":
        mypath = "figures_mine/ims_median.tif"
    elif fpath == "bh_syn" or fpath == "bh_syn_diff":
        mypath = "figures_mine/bh_median.tif"
    else:
        mypath = "figures_mine/bg_median.tif"
    im_med= cv2.imread(mypath, cv2.IMREAD_UNCHANGED)
    fp = "hum_nat/add"
    add_ims = []
    fs = [f for f in listdir(fp) if isfile(join(fp,f)) and (f.endswith(".tif") or f.endswith(".png"))]
    fs.sort()
    for f in fs:
        fn = join(fp,f)
        add_ims.append(cv2.imread(fn, cv2.IMREAD_UNCHANGED))
    return im_med, add_ims

def write_loc_features(fpath, hum_chg_locs):
    #locs = {"nat":nat_chg_locs, "hum":hum_chg_locs}
    if fpath == "mine_syn" or fpath == "mine_syn_diff":
        f = open("features_mn.pkl","wb")
    elif fpath == "bh_syn" or fpath == "bh_syn_diff":
        f = open("features_bh.pkl","wb")
    else:
        f = open("features_bg.pkl","wb")
    #pickle.dump(locs,fn)
    pickle.dump(hum_chg_locs,f)
    f.close()

def read_features(fpath):
    hum_chg_loc = []
    if fpath == "mine_syn" or fpath == "mine_syn_diff":
        f = open("features_mn.pkl","rb")
        hum_chg_loc[:] = pickle.load(f)
    elif fpath == "bh_syn" or fpath == "bh_syn_diff":
        f = open("features_bh.pkl","rb")
        hum_chg_loc[:] = pickle.load(f)
    else:
        f = open("features_bg.pkl","rb")
        hum_chg_loc[:] = pickle.load(f)
    hum_chg_locs.append(hum_chg_loc)
    f.close()
    
#generate natural change image for the first half images
def images_hum(fpath, ims, add_ims):
    rng = np.random
    mid = int(len(ims)/2)
    c = 0
    cols = []
    for im in ims:
        hum_chg_loc = []
        rn = int(im.shape[0]/ROW_DIFF)
        cn = int(im.shape[1]/ROW_DIFF)
        cols[:] = []
        for i_ in range(rn):
            col = rng.permutation(cn)
            cols.append(col)
        j_num = int(cn/2)
        for i_ in range(rn):
            for j_ in range(j_num):
                inx = (i_*j_num + j_)%len(add_ims)
                h = add_ims[inx].shape[0]-1
                w = add_ims[inx].shape[1]-1
                t = i_ * ROW_DIFF
                l = cols[i_][j_] * ROW_DIFF
                b = t + h
                r = l + w
                if b > im.shape[0] or  r > im.shape[1]:
                    continue
                if 0 not in im[t:b,l:r,3]:
                    for ii in range(b-t+1):
                        for jj in range(r-l+1):
                            if add_ims[inx][ii,jj,3] != 0:
                                im[t+ii,l+jj,0:3] = add_ims[inx][ii,jj,0:3]
                    loc = (t,l,h,w)
                    hum_chg_loc.append((loc,inx))
        ims_blend_hum.append(im)
        hum_chg_locs.append(hum_chg_loc)
    write_loc_features(fpath, hum_chg_locs)    

#generate human change images for the other half images
def images_nat(ims,ncs):
    for im in ims:
        ims_blend_nat.append(im)
    for im in ncs:
        ims_blend_nc.append(im)

        
#blend the original image with cloud image
def images_blend(fpath, add_ims):
    if fpath == "mine_syn" or fpath == "mine_syn_diff":
        fp_nat = "hum_nat/nat_mine"
        fp_hum = "hum_nat/hum_mine"
        fp_nc = "hum_nat/nc_mine"
    elif fpath == "bh_syn" or fpath == "bh_syn_diff":
        fp_nat = "hum_nat/nat_bh"
        fp_hum = "hum_nat/hum_bh"
        fp_nc = "hum_nat/nc_bh"
    else:
        fp_nat = "hum_nat/nat_bg"
        fp_hum = "hum_nat/hum_bg"
        fp_nc = "hum_nat/nc_bg"
    fn_nat = [f for f in listdir(fp_nat) if isfile(join(fp_nat,f)) and (f.endswith('.tif') or f.endswith('.png'))]
    fn_hum = [f for f in listdir(fp_hum) if isfile(join(fp_hum,f)) and (f.endswith('.tif') or f.endswith('.png'))]
    fn_nc = [f for f in listdir(fp_nc) if isfile(join(fp_hum,f)) and (f.endswith('.tif') or f.endswith('.png'))]
    hums = []
    nats = []
    ncs = []
    for f in fn_nat:
        fp = join(fp_nat,f)
        imn= cv2.imread(fp, cv2.IMREAD_UNCHANGED)
        nats.append(imn)
    for f in fn_hum:
        fp = join(fp_hum,f)
        imh = cv2.imread(fp, cv2.IMREAD_UNCHANGED)
        hums.append(imh)
    for f in fn_nc:
        fp = join(fp_nc,f)
        imh = cv2.imread(fp, cv2.IMREAD_UNCHANGED)
        ncs.append(imh)
    images_nat(nats,ncs)
    images_hum(fpath, hums,add_ims)

def write_blend(fpath):
    fp = fpath
    fpp = fp + "/*"
    fs = glob.glob(fpp)
    for f in fs:
        remove(f)
    for _ in range(len(ims_blend_nat)):
        if _ < 10:
            fn = fp + "/ims_blend_nat0" + str(_) + ".tif"
        else:
            fn = fp + "/ims_blend_nat" + str(_) + ".tif"
        cv2.imwrite(fn,ims_blend_nat[_])
        
    for _ in range(len(ims_blend_nc)):
        if _ < 10:
            fn = fp + "/ims_blend_nc0" + str(_) + ".tif"
        else:
            fn = fp + "/ims_blend_nc" + str(_) + ".tif"
        cv2.imwrite(fn,ims_blend_nc[_])
        
    for _ in range(len(ims_blend_hum)):
        if _ < 10:
            fn = fp + "/ims_blend_hum0" + str(_) + ".tif"
        else:
            fn = fp + "/ims_blend_hum" + str(_) + ".tif"
        cv2.imwrite(fn,ims_blend_hum[_])
        
def read_blend(fpath = 'mine_syn'):
    fs = [f for f in listdir(fpath) if isfile(join(fpath,f)) and (f.endswith('.tif') or f.endswith('.png'))]
    if len(fs) == 0:
        im_mid,add_ims = load_data_syn(fpath)
        images_blend(fpath, add_ims)
        write_blend(fpath)
    else:
        ims_blend_hum[:] = []
        ims_blend_nat[:] = []
        ims_blend_nc[:] = []
        for f in fs:
            fn = join(fpath,f)
            im = cv2.imread(fn, cv2.IMREAD_UNCHANGED).astype(np.float)
            #im = im + NC_THD   #add some value to no change pixels
            if f.startswith('ims_blend_nat'):
                ims_blend_nat.append(im)
            elif f.startswith('ims_blend_hum'):
                ims_blend_hum.append(im)
            else:
                ims_blend_nc.append(im)
            
def write_blend_diff_abs(fpath, ims_blend_diff_tmp):
    fp = fpath
    fpp =fpath + "/*"
    fs = glob.glob(fpp)
    for f in fs:
        remove(f)
    for i in range(len(ims_blend_diff_tmp)):
        if i < len(ims_blend_hum_diff):
            print("hum",i)
            if i < 10:
                fn = fp + "/ims_blend_diff_hum0" + str(i) + ".tif"
            else:
                fn = fp + "/ims_blend_diff_hum" + str(i) + ".tif"
            cv2.imwrite(fn,ims_blend_diff_tmp[i])
        elif i < (len(ims_blend_hum_diff)+len(ims_blend_nat_diff)):
            _ = i-len(ims_blend_hum_diff)
            print("nat",i)
            if _ < 10:
                fn = fp + "/ims_blend_diff_nat0" + str(_) + ".tif"
            else:
                fn = fp + "/ims_blend_diff_nat" + str(_) + ".tif"
            cv2.imwrite(fn,ims_blend_diff_tmp[i])
        else:
            _ = i - (len(ims_blend_hum_diff)+len(ims_blend_nat_diff))
            if _ < 10:
                fn = fp + "/ims_blend_diff_nc0" + str(_) + ".tif"
            else:
                fn = fp + "/ims_blend_diff_nc" + str(_) + ".tif"
            cv2.imwrite(fn,ims_blend_diff_tmp[i])
            
def comp_abs(x,y,z):
    if z != 0:
        return abs(x-y)
    else:
        return 0
    
def comp_norm(x,m,M):
    if z != 0:
        return (x-m)/(M-m)
    else:
        return 0

#normalize each difference image
def norm_img_diff(ims_blend_diff_hn):
    m = 300
    M = -300
    for im in ims_blend_diff_hn:
        alpha = np.tile(im[:,:,3].ravel().astype(np.object),3)
        rgb = im[:,:,0:3].ravel().astype(np.object)
        rgb[alpha==0] = None
        rgb = list(filter(None.__ne__,rgb))
        m_tmp= np.amin(rgb)
        M_tmp = np.amax(rgb)
        if m_tmp < m:
            m = m_tmp
        if M_tmp > M:
            M = M_tmp
    print(m,M)
    for im in ims_blend_diff_hn:
        """imc = copy.deepcopy(im)
        rbg = imc[:,:,0:3].ravel()
        alpha = np.tile(imc[:,:,3].ravel(),3)
        im[:,:,0:3] = np.array([comp_norm(rgb[i],m,M,alpha[i]) for i in range(alpha.shape[0])]).reshape(im.shape[0],im.shape[1],3)"""
        for i in range(im.shape[0]):
            for j in range(im.shape[1]):
                #for k in range(im.shape[2]):
                if im[i,j,3] != 0:
                    im[i,j,0:3] = (im[i,j,0:3] - m)/(M - m) * 255
                    
#compute the difference bewteen median and individual image for each image row
def images_difference_abs(fpath,ims_med):
    for im in ims_blend_hum:
        imc = copy.deepcopy(im)
        for i in range(imc.shape[0]):
            for j in range(imc.shape[1]):
                if imc[i,j,3] != 0:
                    imc[i,j,0:3] = np.abs(imc[i,j,0:3] - ims_med[i,j,0:3])
        ims_blend_hum_diff.append(imc)
    if fpath == 'mine_syn' or fpath == 'mine_syn_diff':
        ims_med_org = cv2.imread('figures_mine/ims_median.tif', cv2.IMREAD_UNCHANGED)
    elif fpath == 'bh_syn' or fpath == 'bh_syn_diff':
        ims_med_org = cv2.imread('figures_mine/bh_median.tif', cv2.IMREAD_UNCHANGED)
    else:
        ims_med_org = cv2.imread('figures_mine/bg_median.tif', cv2.IMREAD_UNCHANGED)
    for im in ims_blend_nat:
        imc = copy.deepcopy(im)
        for i in range(imc.shape[0]):
            for j in range(imc.shape[1]):
                if imc[i,j,3] != 0:
                    imc[i,j,0:3] = np.abs(imc[i,j,0:3] - ims_med_org[i,j,0:3])
        ims_blend_nat_diff.append(imc)
    for im in ims_blend_nc:
        imc = copy.deepcopy(im)
        for i in range(imc.shape[0]):
            for j in range(imc.shape[1]):
                if imc[i,j,3] != 0:
                    imc[i,j,0:3] = np.abs(imc[i,j,0:3] - ims_med_org[i,j,0:3])
        ims_blend_nc_diff.append(imc)
    ims_blend_diff_tmp = list(copy.deepcopy(ims_blend_hum_diff))
    ims_blend_diff_tmp.extend(copy.deepcopy(ims_blend_nat_diff))
    ims_blend_diff_tmp.extend(copy.deepcopy(ims_blend_nc_diff))
    norm_img_diff(ims_blend_diff_tmp)
    write_blend_diff_abs(fpath, ims_blend_diff_tmp)

def read_blend_med():
    ims_blend_tmp = ims_blend_hum
    ims_blend_tmp.extend(ims_blend_nat)
    ims_blend_tmp.extend(ims_blend_nc)
    ims_med = comp_ims_median(ims_blend_tmp)
    return ims_med

def read_difference_all(fpath,fp):
    fs = [f for f in listdir(fpath) if isfile(join(fpath,f)) and (f.endswith('.tif') or f.endswith('.png'))]
    ims_blend_hum_diff_ = []
    ims_blend_nat_diff_ = []
    ims_blend_nc_diff_ = []
    for f in fs:
            fn = join(fpath,f)
            im = cv2.imread(fn, cv2.IMREAD_UNCHANGED)
            if f.startswith('ims_blend_diff_hum'):
                ims_blend_hum_diff_.append(im)
            elif f.startswith('ims_blend_diff_nat'):
                ims_blend_nat_diff_.append(im)
            else:
                ims_blend_nc_diff_.append(im)
    ims_blend_hum_diff.append(ims_blend_hum_diff_)
    ims_blend_nat_diff.append(ims_blend_nat_diff_)
    ims_blend_nc_diff.append(ims_blend_nc_diff_)
    read_features(fpath)
    read_blend(fp)  #get original blend images for testing syn data
    ims_blend_hum_all.append(copy.deepcopy(ims_blend_hum))
    ims_blend_nat_all.append(copy.deepcopy(ims_blend_nat))
    ims_blend_nc_all.append(copy.deepcopy(ims_blend_nc))
    read_blend(fpath) #get differece blend images to calculate distribution values
    ims_blend = list(ims_blend_hum)
    ims_blend.extend(ims_blend_nat)
    ims_blend.extend(ims_blend_nc)
    dist_feas.append(comp_diff_val_distribution(ims_blend))
                
def read_difference_abs(fpath):
    if fpath == "mine_syn_diff" or fpath == "bh_syn_diff" or fpath == "bg_syn_diff":
        if fpath == "mine_syn_diff":
            fp = "mine_syn"
            read_blend(fp)
            ims_med = cv2.imread("figures_mine/ims_syn_median.tif",cv2.IMREAD_UNCHANGED)
            if ims_med == None:
                ims_med = read_blend_med()
                cv2.imwrite("figures_mine/ims_syn_median.tif",ims_med)
        elif fpath == "bh_syn_diff":
            fp = "bh_syn"
            read_blend(fp)
            ims_med = cv2.imread("figures_mine/bh_syn_median.tif",cv2.IMREAD_UNCHANGED)
            if ims_med == None:
                ims_med = read_blend_med()
                cv2.imwrite("figures_mine/bh_syn_median.tif",ims_med)
        else:
            fp = "bg_syn"
            read_blend(fp)
            ims_med = cv2.imread("figures_mine/bg_syn_median.tif",cv2.IMREAD_UNCHANGED)
            if ims_med == None:
                ims_med = read_blend_med()
                cv2.imwrite("figures_mine/bg_syn_median.tif",ims_med)
        images_difference_abs(fpath, ims_med)
    else:
        read_difference_all("mine_syn_diff","mine_syn")
        read_difference_all("bh_syn_diff","bh_syn")
        read_difference_all("bg_syn_diff","bg_syn")
        #print(np.array(dist_feas).shape)

#check if human change occurred in add_ims
def ishum(add_ims,row,item):
    for r in row:
        inx = r[1]
        (t,l,h,w) = r[0]
        b = t+h
        r = l+w
        t1 = item[0]
        l1 = item[1]
        b1 = item[2]
        r1 = item[3]
        if (t<=t1<b1<=b and l<=l1<r1<=r):
            if 255 in add_ims[inx][t1-t:b1-t,l1-l:r1-l,3]:
                return True
        elif (l1<l<r1 and t1<t<b1):
            if 255 in add_ims[inx][0:b1-t,0:r1-l,3]:
                return True
        elif (l1<r<r1 and t1<t<b1):
            if 255 in add_ims[inx][0:b1-t,l1-l:r-l,3]:
                return True
        elif (t1<b<b1 and l1<l<r1):
            if 255 in add_ims[inx][t1-t:b-t,0:r1-l,3]:
                return True
        elif (l1<r<r1 and t1<b<b1):
            if 255 in add_ims[inx][t1-t:b-t,l1-l:r-l,3]:
                return True
        elif (t1<t<b1 and l<=l1<r1<=r):
            if 255 in add_ims[inx][0:b1-t,l1-l:r1-l,3]:
                return True
        elif (t1<b<b1 and l<=l1<r1<=r):
            if 255 in add_ims[inx][t1-t:b-t,l1-l:r1-l,3]:
                return True
        elif (t<=t1<b1<=b and l1<l<r1):
            if 255 in add_ims[inx][t1-t:b1-t,0:r1-l,3]:
                return True
        elif (t<=t1<b1<=b and l1<r<r1):
            if 255 in add_ims[inx][t1-t:b1-t,l1-l:r-l,3]:
                return True
    return False
    
def images_diff_divide_sub(im_diff, im_org, indx, i,add_ims):
    global n1,n2,n3
    
    diff_pieces = []
    org_pieces = []
    #ei_size = int(700/len(ims_blend_nat_diff))
    #print("ei_size:",ei_size)
    if i < len(ims_blend_hum_diff[indx]):
        m_org_2d_pieces = []
        k = 0
        row = []
        row[:] = hum_chg_locs[indx][i]
        row[:] = sorted(row, key=lambda x: x[0][1])
        row0 = [(x[0][0],x[0][1]) for x in row]
        for k1 in range(0,im_diff.shape[0],W[0]):
            m_org_pieces = []
            for k2 in range(0,im_diff.shape[1],W[1]):
                b = k1 + W[0]
                r = k2 + W[1]
                im_win = im_diff[k1:b,k2:r,0:3]
                if b > im_diff.shape[0] or r > im_diff.shape[1]:
                    continue
                if 0 in im_diff[k1:b,k2:r,3] and ((i != SYN_TEST_IMG and indx == 0) or indx != 0): #ignore border blocks
                    continue
                ih = ishum(add_ims,row,(k1,k2,b,r))
                if  ih == True:
                    im_piece = (im_win,2)
                else:
                    im_piece = (im_win,0)
                    #print("n0",k1,k2,b,r,n1)
                    #input("aaa")
                if i == SYN_TEST_IMG and indx == 0: #take image 28 of mine to be a testing image
                    im_org_win = im_org[k1:b,k2:r,:]
                    ims_syn_test_pieces.append(im_piece)
                    #dist_ims_test_feas.append(feas)
                    if im_piece[1] == 0:
                        dist_ims_test_feas.append([x for x in dist_feas[indx][i]])
                    else:
                        dist_ims_test_feas.append([x for x in dist_feas[indx][i]])
                    m_org_pieces.append(im_org_win)
                else:
                    if im_piece[1] == 0:
                        n1 += 1
                        ims_blend_diff_pieces.append(im_piece)
                        #dist_ims_feas.append(feas)
                        dist_ims_feas.append([x for x in dist_feas[indx][i]])
                    else:
                        ims_blend_diff_pieces.append(im_piece)
                        dist_ims_feas.append([x for x in dist_feas[indx][i]])
                        #dist_ims_feas.extend(feas)
                        n3 += 1     
            if (len(m_org_pieces)) > 0:
                m_org_2d_pieces.append(m_org_pieces)
            if i == SYN_TEST_IMG and indx == 0:
                ims_syn_test_org.append(m_org_2d_pieces)
        #print(np.array(ims_syn_test_org).shape)
            #cv2.imwrite("aaa.tif",im_org)
            #print("ims_syn_test:",np.array(ims_syn_test_org).shape,np.array(ims_syn_test_pieces).shape,np.array(dist_ims_test_feas).shape)
                    
    elif i < (len(ims_blend_hum_diff[indx])+len(ims_blend_nat_diff[indx])):
        for k1 in range(0,im_diff.shape[0],W[0]):
            for k2 in range(0,im_diff.shape[1],W[1]):
                b = k1 + W[0]
                r = k2 + W[1]
                if b > im_diff.shape[0] or r > im_diff.shape[1]:
                    continue
                im_win = im_diff[k1:b,k2:r,0:3]
                if 0 in im_diff[k1:b,k2:r,3]: #ignore border blocks
                    continue
                #if np.sum(im_win) < NC_THD*W[0]*W[1]*3:
                #    continue
                n2 += 1
                im_piece = (im_win,1)
                ims_blend_diff_pieces.append(im_piece)
                #feas = [0,0,0,0]
                dist_ims_feas.append([x for x in dist_feas[indx][i]])
                #dist_ims_feas.append(feas)
 
n1=0
n2=0
n3=0
#break the each difference image into pieces with equal size and label them
def images_diff_divide_label(add_ims, ims_blend_hum_diff = ims_blend_hum_diff):
    global n1,n2,n3
    rng = np.random
    #print(len(ims_blend_hum_diff),len(ims_blend_nat_diff),len(ims_blend_nc_diff))
    for _ in range(len(ims_blend_hum_diff)):     
        ims_blend_diff = list(ims_blend_hum_diff[_])
        ims_blend_diff.extend(ims_blend_nat_diff[_])
        ims_blend_diff.extend(ims_blend_nc_diff[_])
        ims_blend_org = list(ims_blend_hum_all[_])
        ims_blend_org.extend(ims_blend_nat_all[_])
        ims_blend_org.extend(ims_blend_nc_all[_])
        #print(len(ims_blend_diff),len(ims_blend_org))
        for i in range(len(ims_blend_diff)):
            im_diff = copy.deepcopy(ims_blend_diff[i])
            im_org = copy.deepcopy(ims_blend_org[i])
            images_diff_divide_sub(im_diff,im_org,_,i,add_ims)
    #images_diff_pieces_write(ims_syn_test_pieces)
    print("class0/1/2(no change/nat change/hum change):",n1,n2,n3)
    
#write all difference pieces to the disk
def images_diff_pieces_write(ims_pieces = ims_blend_diff_pieces):
    #remove history files from the folder first
    ps = glob.glob("mine_pieces/*.tif")
    for f in ps:
        remove(f)
        
    for i in range(len(ims_pieces)):
        if ims_pieces[i][1] == 0:
            fn = 'mine_pieces/image' + str(i) + '.tif'
            cv2.imwrite(fn,ims_pieces[i][0])

def release_list():
    del ims_blend_hum[:]
    del ims_blend_nat[:]
    del ims_blend_nat_diff[:]
    del ims_blend_hum_diff[:]
    
def run_mine_syn():
    if sys.argv[1] == "mine_syn" or sys.argv[1] == "bh_syn" or sys.argv[1] == "bg_syn":
        img_med,add_ims = load_data_syn(sys.argv[1])
        read_blend(sys.argv[1])
    #write_loc_features()
    elif sys.argv[1] == "mine_syn_diff" or sys.argv[1] == "bh_syn_diff" or sys.argv[1] == "bg_syn_diff":
        read_difference_abs(sys.argv[1])
    else:
        img_med,add_ims = load_data_syn(sys.argv[1])
        read_difference_abs(sys.argv[1])
        images_diff_divide_label(add_ims)
        images_diff_pieces_write()
    release_list()
    
if __name__ == '__main__':
    run_mine_syn()         




