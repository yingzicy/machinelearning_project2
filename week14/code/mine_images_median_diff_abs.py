import cv2
import numpy as np
from os import listdir,remove
from os.path import isfile,join
import glob
import sys
import copy
import matplotlib.pyplot as plt
import pickle

#get pixels from each image
i_ims_syn = []
org_ims = []
mine_diff_pieces = []
mine_org_pieces = []
test_feas = []
test_dist_feas = []
N_CHG = 1
W = (64,64)
shift = (32,32)
perc_hum = 0.20
perc_nat = 0.9
perc_thd = 0.08
#perc = 0.2
DIFF_IMS = "imdd"
NC = 2
NC1 = 5
def load_data():
    #generate file list
    mypath = sys.argv[5]
    images = [ f for f in listdir(mypath) if isfile(join(mypath,f)) and (f.endswith(".tif") or f.endswith(".png"))]
    images.sort()

    """p = images[0]
    images = [p,p,p,p]"""
    for f in images:
        pf = join(mypath,f)
        im = cv2.imread(pf, cv2.IMREAD_UNCHANGED)
        if im.shape[2] < 4 or 'analytic' in f:
            print("channels less than 4 occurs")
            continue
        org_ims.append(im)
        im = im.astype(np.int)
        i_ims_syn.append(im)

def plot_dist(n,vals,counts):
    fig = plt.figure(n)
    plt.hist([item for item in l_im if item != 0], 255)
    plt.plot(vals[1:],counts[1:],'r--',linewidth = 1)
    plt.xlabel('difference values')
    plt.ylabel('frequence')
    fn = "mine_diff_distribution/img" + str(n) + ".png"
    fig.savefig(fn)
    plt.close(fig)

def comp_perc_values(fra, vals, counts):
    pos = int(len(vals)*fra)
    ct = np.sum(counts[pos:])
    per = ct/np.sum(counts[1:])
    return per

def comp_key_values(vals,counts):
    dist_mean = 0.0
    vals_mean = []
    dist_var = 0.0
    if counts[0] == 0:
        dist_mean = np.dot(vals[1:],counts[1:])/np.sum(counts[1:])
        vals_mean = np.pow(np.subtract(vals[1:],dist_mean),2)
        dist_var = np.sum(vals_mean)/np.sum(counts[1:])
    else:
        dist_mean = np.dot(vals,counts)/np.sum(counts)
        vals_mean = np.subtract(vals,dist_mean)
        dist_var = np.sum(vals_mean)/np.sum(counts)
    mid = 0.5
    quart = 0.75
    perc_values = []
    perc_values.append(dist_mean)
    perc_values.append(dist_var)
    perc_values.append(comp_perc_values(mid,vals,counts))
    perc_values.append(comp_perc_values(quart,vals,counts))
    #print(perc_values)
    return perc_values

#save distribution features for training
def save_dist_features(dist_features):
    f = open("dist_features.pkl","wb")
    pickle.dump(dist_features,f)
    f.close()
    
#compute the difference distribution for each difference image 
def comp_diff_val_distribution(ims = i_ims_syn):
    ims_diff_dist = []
    n = 0
    dist_features = []
    for im in ims:
        l_im = im[:,:,0:3].ravel()
        vals,counts = np.unique(l_im, return_counts = True)
        dist_feature = comp_key_values(vals,counts)
        dist_features.append(dist_feature)
        #plot the value distribution for each difference image
        """plot_dist(n,vals,counts)
        n += 1"""
    #save_dist_features(dist_features)
    return dist_features

#remove noise information from the difference images
def com_diff_rm_noise(ims = i_ims_syn):
    ims_diff_dist = []
    n = 0
    miu = []
    ims_tshd = []
    k = 0
    for im in ims:
        l_im = im[:,:,0:3].ravel()
        vals,counts = np.unique(l_im, return_counts = True)
        tc = np.sum(counts[1:])
        c_tmp = 0
        mid = int(len(vals)/2)
        ct = np.sum(counts[mid:])
        per_mid = ct/np.sum(counts[1:])
        print(mid,ct,per_mid,np.sum(counts[1:]))
        #tp = int(tc * perc)
        tp = 0
        if per_mid > perc_thd:    #filter noise for nat change or hum change(nat change with a big area normally)
            tp = int(tc * perc_nat)
            print("img",k,"over 0.1")
        else:
            tp = int(tc * perc_hum)
        for i in range(counts.shape[0]-1,-1,-1):
            c_tmp += counts[i]
            if c_tmp >= tp:
                break
        ims_tshd.append(i)
        #remove all pixel values less than the threshold value
        t_value = vals[i]
        k += 1
        for i in range(im.shape[0]):
            for j in range(im.shape[1]):
                if im[i,j,3]!=0:
                    if im[i,j,0] < t_value:
                        im[i,j,0] = NC
                    if im[i,j,1] < t_value:
                        im[i,j,1] = NC
                    if im[i,j,2] < t_value:
                        im[i,j,2] = NC

#read original difference data
def read_org_diff_data(mypath = "mine_diff"):
    #fs = [f for f in listdir(mypath) if isfile(join(mypath,f)) and (f.endswith(".tif") or f.endswith(".png")) and (f.startswith(sys.argv[1]) or f.startswith("ims_blend"))]
    fs = [f for f in listdir(mypath) if isfile(join(mypath,f)) and (f.endswith(".tif") or f.endswith(".png")) and (f.startswith("ims_median.tif") or f.startswith("ims_blend") or f.startswith("bh_median.tif") or f.startswith("bg_median.tif"))]
    fs.sort()
    if len(fs) == 0:
        compute_diff_abs(sys.argv[1])
        #compute_diff_abs("ims_median.tif")
        write_diff_images([],mypath)
    else:
        i_ims_syn[:] = []
        for f in fs:
            fn = join(mypath,f)
            im = cv2.imread(fn, cv2.IMREAD_UNCHANGED)
            i_ims_syn.append(im.astype(np.float))
    return fs

#read mean difference images
def read_diff_mean_data(mypath = "mine_diff_norm_mean",des = "mine_diff_norm_v2_mean_perc"):
    #mypath = "mine_diff_norm_mean"
    fs = [f for f in listdir(mypath) if isfile(join(mypath,f)) and (f.endswith(".tif") or f.endswith(".png")) and (f.startswith("ims_median.tif") or f.startswith("ims_blend"))]
    fs.sort()
    if len(fs) == 0:
        fs = read_org_diff_data(des)
        norm_diff_imgs_mu()
        norm_img_diff()
        write_diff_images(fs, mypath)
    else:
        read_diff_v2_data(mypath, fs, des)
            
#read theta difference images
def read_diff_theta_data(mypath = "mine_diff_norm_theta",des = "mine_diff_norm_v2_theta_perc"):
    #mypath = "mine_diff_norm_theta"
    fs = [f for f in listdir(mypath) if isfile(join(mypath,f)) and (f.endswith(".tif") or f.endswith(".png")) and (f.startswith("ims_median.tif") or f.startswith("ims_blend"))]
    fs.sort()
    if len(fs) == 0:
        fs = read_org_diff_data(des)
        norm_diff_imgs_mu()
        norm_diff_imgs_theta()
        norm_img_diff()
        write_diff_images(fs, mypath)
    else:
        read_diff_v2_data(mypath, fs, des)

#read theta difference images
def read_diff_v2_data(mypath, fs, fp = "mine_diff_norm_v2"):
    i_ims_syn[:] = []
    for f in fs:
        fn = join(mypath,f)
        im = cv2.imread(fn, cv2.IMREAD_UNCHANGED)
        im = im.astype(np.float)
        i_ims_syn.append(im)
    ims_median = comp_ims_median()
    compute_diff_abs_sub(ims_median)
    com_diff_rm_noise()
    write_diff_images(fs, fp)
    
def read_data(mypath = "mine_diff"):
    fs = [f for f in listdir(mypath) if isfile(join(mypath,f)) and (f.endswith(".tif") or f.endswith(".png")) and (f.startswith("ims_median.tif") or f.startswith("ims_blend"))]
    fs.sort()
    if len(fs) == 0:
        print("no such file exists")
    else:
        i_ims_syn[:] = []
        for f in fs:
            fn = join(mypath,f)
            im = cv2.imread(fn, cv2.IMREAD_UNCHANGED)
            i_ims_syn.append(im)
    return fs
            
def read_rm_noise(fp,des):
    fs = read_data(fp)
    com_diff_rm_noise()
    write_diff_images(fs, des)
    
#read original difference images from disk
def read_diff_data(opt):
    fp = sys.argv[3]
    des = sys.argv[4]
    if opt == '0':
        read_org_diff_data(fp)
    elif opt == '1':
        read_diff_mean_data(fp,des)
    elif opt == '2':
        read_diff_theta_data(fp,des)
    elif opt =='3':
        read_rm_noise(fp,des)
    else:
        read_data(fp)
        #compute test distribution features for original mine difference images
        mypath = "mine_diff"
        fs = [f for f in listdir(mypath) if isfile(join(mypath,f)) and (f.endswith(".tif") or f.endswith(".png")) and (f.startswith("ims_median.tif") or f.startswith("ims_blend"))]
        fs.sort()
        org_diff_ims = []
        if len(fs) == 0:
            print("no such file exists")
        else:
            for f in fs:
                fn = join(mypath,f)
                im = cv2.imread(fn, cv2.IMREAD_UNCHANGED)
                org_diff_ims.append(im)
        test_feas[:] = comp_diff_val_distribution(org_diff_ims)
        
        
#normalize each difference image
def norm_img_diff(ims = i_ims_syn):
    m = 300.0
    M = -300.0
    for im in ims:
        alpha = np.tile(im[:,:,3].ravel().astype(np.object),3)
        rgb = im[:,:,0:3].ravel().astype(np.object)
        rgb[alpha==0] = None
        rgb = list(filter(None.__ne__,rgb))
        if len(rgb) == 0:
            continue
        m_tmp= min(rgb)
        M_tmp = max(rgb)
        if m_tmp < m:
            m = m_tmp
        if M_tmp > M:
            M = M_tmp
        print(m,M)
    
    print(m,M)
    for im in ims:
        for i in range(im.shape[0]):
            for j in range(im.shape[1]):
                #for k in range(im.shape[2]):
                if im[i,j,3] != 0:
                    im[i,j,0:3] = (im[i,j,0:3] - m)/(M - m) * 255

def comp_abs(x,y,z):
    if z != 0:
        return abs(x-y)
    else:
        return 0
    
def compute_diff_abs_sub(ims_ref_img, ims = i_ims_syn):
    for im in ims:
        for i in range(im.shape[0]):
            for j in range(im.shape[1]):
                #for k in range(im.shape[2]):
                if im[i,j,3] != 0:
                    im[i,j,0:3] = np.abs(im[i,j,0:3] - ims_ref_img[i,j,0:3])
    norm_img_diff()

#compute the absolute value for the difference bewteen each image and mean/median/percentile image and normalize them        
def compute_diff_abs(fname):
    #read reference image, median mean or pert10
    fn = "figures_mine/" + fname
    ims_ref_img = cv2.imread(fn, cv2.IMREAD_UNCHANGED).astype(np.int)
    #compute difference between median image and original images
    compute_diff_abs_sub(ims_ref_img)
    
def split_diff():
    #split difference images into (64,64)blocks
    """cv2.imwrite("ims35_2.tif",i_ims_syn[35])
    cv2.imwrite("ims35_1.tif",org_ims[35])
    cv2.imwrite("ims49_2.tif",i_ims_syn[49])
    cv2.imwrite("ims49_1.tif",org_ims[49])"""
    k = 0
    for im in i_ims_syn:
        i_ims_syn[k] = im.astype(np.uint8)
        k += 1
    for k in range(len(i_ims_syn)):
        m_pieces = []
        m_org_2d_pieces = []
        im = np.copy(i_ims_syn[k])
        im_org = np.copy(org_ims[k])
        fea_pieces = []
        #print("image shape:", im.shape)
        for i in range(0,im.shape[0],W[0]):
            m_org_pieces = []
            for j in range(0,im.shape[1],W[1]):
                b = i + W[0]
                r = j + W[1]
                if b >= im.shape[0] or r >= im.shape[1]:
                    continue
                """if [0,0,0] in im[i:b,j:r]:  #ignore incomplete block
                    m_piece = (np.zeros((b-i,r-j,3)),N_CHG)
                else:"""
                m_piece = (im[i:b,j:r,0:3],N_CHG)
                """s = np.sum(m_piece[0])
                if s < NC1 * W[0] * W[1] * 3:
                    #fea_pieces.append([0,0,0,0])
                    fea_pieces.append([x for x in test_feas[k]])
                else:"""
                fea_pieces.append(test_feas[k])
                m_org_piece = im_org[i:b,j:r,:]
                m_pieces.append(m_piece)
                m_org_pieces.append(m_org_piece)
            if len(m_org_pieces) > 0:
                m_org_2d_pieces.append(m_org_pieces)
        mine_diff_pieces.append(m_pieces)
        test_dist_feas.append(fea_pieces)
        if len(m_org_2d_pieces) > 0:
            mine_org_pieces.append(m_org_2d_pieces)

#compute the mean
def comp_ims_mean(ims):
    ims_mean = []
    for im in ims:
        mu = 0
        count = 0
        for i in range(im.shape[0]):
            for j in range(im.shape[1]):
                if im[i,j,3] != 0:
                    mu += np.sum(im[i,j,0:3])
                    count += 3
        if count != 0:
            mu = int(mu/count)
        ims_mean.append(mu)
    print("max mu and min mu:",max(ims_mean),min(ims_mean))
    return ims_mean


#normalize original difference images, use X-mean/theta or X-mean
def norm_diff_imgs_mu(ims = i_ims_syn):
    #compute mean
    ims_mean = comp_ims_mean(ims)
    print(ims_mean)
    k = 0
    for im in ims:
        for i in range(im.shape[0]):
            for j in range(im.shape[1]):
                if im[i,j,3] != 0:
                    im[i,j,0:3] -= ims_mean[k]
        k += 1
    
def norm_diff_imgs_theta(ims = i_ims_syn):
    #compute mean
    ims_diff_mu_sqr = copy.deepcopy(ims)
    for im in ims_diff_mu_sqr:
        im[:,:,0:3] = np.square(im[:,:,0:3])
    ims_theta = np.sqrt(comp_ims_mean(ims_diff_mu_sqr))
    print("ims_std",ims_theta)
    k = 0
    for im in ims:
        im[:,:,0:3] = im[:,:,0:3]/ims_theta[k]
        k += 1
def comp_nan(x,z):
    if z == 0:
        return np.nan
    else:
        return x
def comp_notnan(x,z):
    if z == 0:
        return 0
    else:
        return x
def comp_ims_median(ims = i_ims_syn):
    ims_copy = []
    for im in ims:
        imf = np.copy(im).astype(np.float)
        for i in range(im.shape[0]):
            for j in range(im.shape[1]):
                if imf[i,j,3] == 0:
                    imf[i,j,:] = (np.nan,np.nan,np.nan,np.nan)
        ims_copy.append(imf)
    
    #compute the median of all the images
    ims_medianf = np.nanmedian(ims_copy, axis = 0)
    ims_median = ims_medianf.astype(np.uint8)
    for i in range(ims_median.shape[0]):
        for j in range(ims_median.shape[1]):
            if ims_median[i,j,3] == np.nan:
                ims_median[i,j,:] = [0,0,0,0]
    return ims_median
                
def write_diff_pieces():
    n = 0
    fs = glob.glob("mine_pieces/*")
    for f in fs:
        remove(f)
    for ips in mine_diff_pieces:
        for i in range(int(len(ips)/2)):
            fn = "mine_diff_pieces/image" + str(n) + "_ims_median.tif"
            cv2.imwrite(fn,ips[i])
            n += 1
#write difference images to the disk
def write_diff_images(fs, fpath):
    fp = fpath + "/*"
    fs1 = glob.glob(fp)
    for f in fs1:
        if f.startswith("ims_median.tif") or f.startswith("ims_blend") or f.startswith("bh_median.tif") or f.startswith("bg_median.tif"):
            remove(f)
    for _,im in enumerate(i_ims_syn):
        im = im.astype(np.uint8)
        fn = []
        if _ < 10:
            seq = '0' + str(_)
            #fn="{0}/{1}{2}.tif".format(fpath,sys.argv[1],seq)
            if len(fs) == 0:
                fn = fpath + "/" + sys.argv[1] + seq + ".tif"
            else:
                fn = fpath + "/" + fs[_] + seq + ".tif"
        else:
            if len(fs) == 0:
                fn = fpath + "/" + sys.argv[1] + str(_) + ".tif"
            else:
                fn = fpath + "/" + fs[_] + str(_) + ".tif"
        cv2.imwrite(fn,im)
        
def release_list():
    del i_ims_syn[:]
    del org_ims[:]
    
def run_mine_diff_out():
    opt = sys.argv[2]
    load_data()
    read_diff_data(opt)
    #ims_tshd = comp_diff_val_distribution()   
    split_diff()
    release_list()
    
if __name__ == '__main__':
    run_mine_diff_out()

