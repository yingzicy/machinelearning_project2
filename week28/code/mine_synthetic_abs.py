from six.moves import cPickle
import sys
from os import listdir,remove,makedirs,walk
from os.path import isfile,join,exists
import numpy as np
import numpy.random as rng
import cv2,glob,pickle,copy,json
import tensorflow.examples.tutorials.mnist.input_data as input_data
from datetime import datetime
from shutil import copyfile
from site_date_distribution import comp_distribution

ims_blend_diff_pieces = []
ims_syn_test_org_pieces = []
ims_syn_test_pieces = []
valid_org_img_xy = []
mine_diff_pieces = []
mine_org_pieces = []
test_org_img_xy = []
W = (32,32)
W_IMG = (28,28)
                    
                  
def images_diff_sub(ims_blend_diff,tp):    
    for _, im in enumerate(ims_blend_diff):
        im_diff = im.reshape(W_IMG[0],W_IMG[1],1)
        im_org = im_diff
        org_img_xy = []
        for k1 in range(0,im_diff.shape[0],W[0]):
            m_org_pieces = []
            org_img_x = []
            for k2 in range(0,im_diff.shape[1],W[1]):
                b = k1 + W[0]
                r = k2 + W[1]
                im_win = im_diff[k1:b,k2:r,:]
                im_win1 = im_org[k1:b,k2:r,:]
                im_org_win = im_org[k1:b,k2:r,:]
                if b > im_diff.shape[0]:
                    zos = np.zeros((W[0]-im_org_win.shape[0],im_org_win.shape[1],1))
                    im_org_win = np.vstack((im_org_win,zos))
                    im_win = np.vstack((im_win,zos[:,:,:]))
                    im_win1 = np.vstack((im_win1,zos[:,:,:]))
                if r > im_diff.shape[1]:
                    zos = np.zeros((im_org_win.shape[0],W[1]-im_org_win.shape[1],1))
                    im_org_win = np.hstack((im_org_win,zos))
                    im_win = np.hstack((im_win,zos[:,:,:]))
                    im_win1 = np.hstack((im_win1,zos[:,:,:]))
                labels = []
                for i in range(im_org_win.shape[0]):
                    label = []
                    for j in range(im_org_win.shape[1]):
                        if im_org_win[i][j] <= 0:
                            label.append(1)
                        else:
                            label.append(0)
                    labels.append(label)
                #im_win_tot = np.vstack((im_win,im_win1,im_win2))
                #im_win_tot = np.dstack((im_win,im_win1))
                im_win_tot = im_win
                im_piece = (im_win_tot,labels)
                org_img_x.append(1)
                if tp == 0:
                    ims_blend_diff_pieces.append(im_piece)
                elif tp == 1:
                    ims_syn_test_pieces.append(im_piece)
                    ims_syn_test_org_pieces.append(im_org_win)
                else:
                    mine_diff_pieces.append(im_piece)
                    mine_org_pieces.append(im_org_win)
            org_img_xy.append(np.array(org_img_x))
        if tp == 1:
            valid_org_img_xy.append(np.array(org_img_xy))
        if tp == 2:
            test_org_img_xy.append(np.array(org_img_xy))
                    
def read_mnist_data():
    m = input_data.read_data_sets("MNIST")
    return m

def read_local_mnist_data(src):
    ims = []
    fs = [f for f in listdir(src) if f.endswith('.png') or f.endswith('.jpg')]
    fs.sort()
    for f in fs:
        im = cv2.imread(join(src,f),cv2.IMREAD_UNCHANGED)
        ims.append(im)
    return ims
    
def write_original_data(des,d_arr):
    for _,im in enumerate(d_arr):
        im = im.reshape(28,28,1)
        if _ < 10:
            cv2.imwrite(join(des,'image' + str(0) + str(_) + '.png'),im)
        else:
            cv2.imwrite(join(des,'image' + str(_) + '.png'),im)
        
#break the each difference image into pieces with equal size and label them
def images_diff_label():
    #global n1,n2,n3
    rng = np.random
    """m_data = read_mnist_data()
    m_train_X = rng.permutation(m_data.train.images)[:5000]*255
    m_train_y = m_data.train.labels
    m_valid_X = m_data.test.images[0:len(m_data.test.images)-100]
    m_valid_y = m_data.test.labels[0:len(m_data.test.images)-100]
    m_test_X = m_data.test.images[len(m_data.test.images)-100:]
    m_test_y = m_data.test.labels[len(m_data.test.images)-100:]
    m_valid_X = rng.permutation(m_data.test.images)[:200]*255
    m_test_X = rng.permutation(m_data.test.images)[:100]*255"""
    m_train_X = read_local_mnist_data('mnist_data/train')
    m_valid_X = read_local_mnist_data('mnist_data/valid')
    m_test_X = read_local_mnist_data('mnist_data/test')
    images_diff_sub(m_train_X,0)
    images_diff_sub(m_valid_X,1)
    images_diff_sub(m_test_X,2)
    """write_original_data('mnist_data/train',m_train_X)
    write_original_data('mnist_data/valid',m_valid_X)
    write_original_data('mnist_data/test',m_test_X)"""
    
#write all difference pieces to the disk
def images_diff_pieces_write(ims_pieces = ims_blend_diff_pieces):
    #remove history files from the folder first
    ps = glob.glob("mine_pieces/*")
    for f in ps:
        remove(f)
        
    for _,im in enumerate(ims_pieces):
        #if im[1] ==1:
        fn = 'mine_pieces/image' + str(_) + '.png'
        cv2.imwrite(fn,im[0])
    
def run_mine_syn():
    if sys.argv[1].endswith("syn"):
        read_blend(sys.argv[1])
    elif sys.argv[1].endswith("diff"):
        read_difference_abs(sys.argv[1])
    else:
        images_diff_label()
        #images_diff_pieces_write(ims_blend_diff_pieces)
    
if __name__ == '__main__':
    run_mine_syn()         




