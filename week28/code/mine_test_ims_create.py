import cv2
import numpy as np
from os import listdir,remove,walk
from os.path import isfile,join
import glob,sys,copy,json
import pickle
from mine_synthetic_abs import get_feas_from_json

#get pixels from each image
mine_diff_pieces = []
mine_org_pieces = []
test_dist_feas = []
test_org_img_xy = []
N_CHG = 2
W = (64,64)
shift = (64,64)
all_sites_org_ims_names = []            
#load testing original images and files
def load_data():
    #generate file list
    org_ims = []
    median_ims = []
    diff_ims = []
    dist_extra_feas = []
    dpath = "../data"
    test_dir = 'test_diff/'
    org_json_dir = '../org_json'
    org_diff_dir = '../org_diff'
    ims_med_dir = 'figures_mine'
    fs = [f for f in listdir(test_dir) if f.endswith('.tif') or f.endswith('.png')]
    for f in fs:
        site = f.split('_org')[0]
        #read original image
        im_org = cv2.imread(join(test_dir,f),cv2.IMREAD_UNCHANGED)
        if im_org == None or im_org.shape[2] < 4:
                print("channels less than 4 occurs:",f)
                continue
        org_ims.append(im_org)
        #read diff image
        im_diff = cv2.imread(join(org_diff_dir,site,f),cv2.IMREAD_UNCHANGED)
        diff_ims.append(im_diff)
        #read median iamge
        im_med = cv2.imread(join('figures_mine',site+'_median.png'),cv2.IMREAD_UNCHANGED)
        median_ims.append(im_med)
        #read json file
        if '.tif' in f:
            j_fn = f.replace('.tif','.json')
        elif '.png' in f:
            j_fn = f.replace('.png','.json')
        with open(join(org_json_dir,site,j_fn)) as j_f:
            d = json.load(j_f)
        feas = get_feas_from_json(d)
        dist_extra_feas.append(feas)
        #generate original image names for each image of each site
        dirpath = join(dpath,site)
        fs_org = [ff for ff in listdir(dirpath) if isfile(join(dirpath,ff)) and (ff.endswith('.tif') or ff.endswith('.png')) and 'analytic' not in ff]
        fs_org.sort()
        fno = f.split("org")[1].split(".")[0]
        i_org_name = (site,fs_org[int(fno)],f,fno)
        all_sites_org_ims_names.append(i_org_name)
        
    print("test data amount:",len(org_ims),np.array(diff_ims).shape,len(dist_extra_feas))
    return org_ims,diff_ims,dist_extra_feas,median_ims
                                         
                                         
def split_diff(org_ims,i_ims_syn,test_feas,median_ims):
    
    for _,im in enumerate(i_ims_syn):
        if im == None:
            continue
        org_img_xy = []
        m_pieces = []
        m_org_2d_pieces = []
        im_org = np.copy(org_ims[_])
        im_median = np.copy(median_ims[_])
        for k1 in range(0,im.shape[0],W[0]):
            m_org_pieces = []
            org_img_x = []
            for k2 in range(0,im.shape[1],W[1]):
                b = k1 + W[0]
                r = k2 + W[1]
                m_org_piece = im_org[k1:b,k2:r,:]
                im_win = im[k1:b,k2:r,0:3]
                im_win1 = im_org[k1:b,k2:r,0:3]
                im_win2 = im_median[k1:b,k2:r,0:3]
                if b > im.shape[0]:
                    zos = np.zeros((W[0]-m_org_piece.shape[0],m_org_piece.shape[1],4))
                    m_org_piece = np.vstack((m_org_piece,zos))
                    im_win = np.vstack((im_win,zos[:,:,0:3]))
                    im_win1 = np.vstack((im_win1,zos[:,:,0:3]))
                    im_win2 = np.vstack((im_win2,zos[:,:,0:3]))
                if r > im.shape[1]:
                    zos = np.zeros((m_org_piece.shape[0],W[1]-m_org_piece.shape[1],4))
                    m_org_piece = np.hstack((m_org_piece,zos))
                    im_win = np.hstack((im_win,zos[:,:,0:3]))
                    im_win1 = np.hstack((im_win1,zos[:,:,0:3]))
                    im_win2 = np.hstack((im_win2,zos[:,:,0:3]))
                #im_win_tot = np.vstack((im_win,im_win1,im_win2))
                for i in range(m_org_piece.shape[0]):
                    for j in range(m_org_piece.shape[1]):
                        if m_org_piece[i][j][3] == 0:
                            m_org_piece[i][j] = [0,0,0,0]
                            im_win[i][j] = [0,0,0]
                            im_win1[i][j] = [0,0,0]
                            im_win2[i][j] = [0,0,0]
                #im_win_tot = np.dstack((im_win,im_win1))
                im_win_tot = im_win1
                m_piece = (im_win_tot,N_CHG)
                mine_diff_pieces.append(m_piece)
                mine_org_pieces.append(m_org_piece)
                org_img_x.append(1)
            org_img_xy.append(np.array(org_img_x))
        test_org_img_xy.append(np.array(org_img_xy))
    print(len(mine_diff_pieces),len(mine_org_pieces))


def write_diff_pieces():
    n = 0
    fs = glob.glob("mine_diff_pieces/*")
    for f in fs:
        remove(f)
    for ips in mine_diff_pieces[49:50]:
        for i in range((len(ips))):
            fn = "mine_diff_pieces/im50_piece" + str(n) + ".png"
            cv2.imwrite(fn,ips[i][0])
            n += 1
    for ips in mine_diff_pieces[52:53]:
        for i in range((len(ips))):
            fn = "mine_diff_pieces/im53_piece" + str(n) + ".png"
            cv2.imwrite(fn,ips[i][0])
            n += 1
    
def run_mine_diff_out():
    org_ims,diff_ims,dist_extra_feas,median_ims = load_data() 
    split_diff(org_ims,diff_ims,dist_extra_feas,median_ims)
    #write_diff_pieces()
    
if __name__ == '__main__':
    run_mine_diff_out()

