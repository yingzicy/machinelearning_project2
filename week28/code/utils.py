from mine_synthetic_abs import *
#from mine_test_ims_create import *
import csv

ISIZE_BEGIN = 0
ISIZE_END = 50
B_SIZE = 1 #border size

#load training and testing datasets from local
def load_mine_data():
    datasets = []
    run_mine_syn()
    #run_mine_diff_out()
    #train dataset
    X = np.array([np.array(i[0]) for i in ims_blend_diff_pieces])
    Y = np.array([np.array(i[1]).astype(np.int) for i in ims_blend_diff_pieces])
    datasets.append((X,Y))
    valid_X = np.array([np.array(i[0]) for i  in ims_syn_test_pieces])
    valid_Y = np.array([np.array(i[1]).astype(np.int) for i  in ims_syn_test_pieces])
    datasets.append((valid_X,valid_Y))
    test_X = np.array([np.array(i[0]) for i in mine_diff_pieces])
    datasets.append((test_X))
    print("train_X shape and train_Y shape:",X.shape,Y.shape)
    print("valid_X shape and valid_Y shape:",valid_X.shape, valid_Y.shape);
    print("test_X shape:",test_X.shape)
    return datasets

#output the testing results, that is ,the whole image
def write_results(ims, org_ims_xy, des, start):
    fp = des
    fpp = des + "/*"
    fs = glob.glob(fpp)
    for f in fs:
        remove(f)
    #print(h,w)
    n = start
    inx = 0
    """tc = 0
    for _, imxy in enumerate(org_ims_xy):
        tc += imxy.shape[0]*imxy.shape[1]
    print("total count for imxys:",len(ims),tc)"""
    for _,imxy in enumerate(org_ims_xy):
        h = imxy.shape[0]*W[0]
        w = imxy.shape[1]*W[1]
        reg_ims = np.zeros((h,w))
        for k1 in range(imxy.shape[0]):
            for k2 in range(imxy.shape[1]):
                if inx < start:
                    inx += 1
                    continue
                t = k1*W[0]
                b = (k1+1)*W[0]
                l = k2*W[1]
                r = (k2+1)*W[1]
                im = ims[inx]
                im = im.reshape(im.shape[0],im.shape[1]*im.shape[2])
                reg_ims[t:b,l:r] = im[:,:]
                inx += 1
        if n < 10:
            fn = fp + "/ims_result" + str(0) + str(n) + ".png"
        else:
            fn = fp + "/ims_result" + str(n) + ".png"
        cv2.imwrite(fn,reg_ims)
        n += 1           
        
def create_target_ims_names(test_labels):
    target_ims_names = []
    labels = []
    for _,test_label in enumerate(test_labels):
        test_label1 = test_label.flatten()
        label = [0,0,0]
        t = all_sites_org_ims_names[_]
        cls,counts = np.unique(test_label1,return_counts = True)
        for i,j in zip(cls,counts):
            label[i] = j
        n_t = t[0]+"$"
        if label[0] != 0:
            n_t += "Hum"
        if label[1] != 0:
            n_t += "Nat"
        if label[2] != 0:
            n_t += "Non"
        n_t += "$"+str(t[-1]) + ".png"
        target_ims_names.append(n_t)
        #labels.append(label)
    #return target_ims_names,labels
    return target_ims_names
        
#visualize the testing results
def show_visualized_results(test_labels, org_pieces, org_ims_xy, start, end, des = 'results'):
    BLUE = [255,0,0,255]
    RED = [255]
    inx = 0
    inx1 = 0
    for _,imxy in enumerate(org_ims_xy):
        for k1 in range(imxy.shape[0]):
            for k2 in range(imxy.shape[1]):
                yn = imxy[k1,k2]
                im = org_pieces[inx1]
                inx1 += 1
                if yn == 0:
                    continue
                test = test_labels[inx]
                inx += 1
                if len(im) == 0: #blank images occur
                    continue
                for ii in range(W[0]):
                    for jj in range(W[1]):
                        if test[ii,jj] == 0:
                            im[ii,jj] = RED 
    write_results(org_pieces,org_ims_xy,des,start)

def load_params():
    #f = open("params.pkl","rb")
    f = open(sys.argv[3],"rb")
    #params = pickle.load(f,encoding = 'latin1')
    params = pickle.load(f)
    f.close()
    return params

def save_params(params):
    f = open("params.pkl","wb")
    pickle.dump(params,f)
    f.close()

def save_ims_info(test_labels,org_pieces,target_ims_names,label_counts):
    #write ims detailed info as a pkl file
    ims_list_info = []
    for _,test_label in enumerate(test_labels):
        t = all_sites_org_ims_names[_]
        t_t = {'site':t[0],'org_name':t[1],'target_name':target_ims_names[_],'img_no':t[-1],'img_shape':np.array(org_pieces[_]).shape[0:2],'img_labels':test_label}
        ims_list_info.append(t_t)
    with open("ims_list_info.pkl","wb") as f:
        pickle.dump(ims_list_info,f)
    f.close()

    #write ims info as a cvs file
    with open('ims_list_info.csv', 'w') as csvfile:
        fieldnames = ['site', 'org_name', 'target_name', 'hum_count', 'nat_count', 'non_count']
        imswriter = csv.writer(csvfile, delimiter=',', quotechar='|', quoting=csv.QUOTE_MINIMAL)
        imswriter.writerow(fieldnames)
        for _,test_label in enumerate(test_labels):
            t = all_sites_org_ims_names[_]
            t_t = [t[0],t[1],target_ims_names[_],label_counts[_][0], label_counts[_][1], label_counts[_][2]]
            imswriter.writerow(t_t)
    csvfile.close()
