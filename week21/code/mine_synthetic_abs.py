from six.moves import cPickle
import sys
from os import listdir,remove,makedirs,walk
from os.path import isfile,join,exists
import numpy as np
import numpy.random as rng
import cv2,glob,pickle,copy,json
from datetime import datetime
from shutil import copyfile
#from mine_images_median_diff_abs import comp_diff_val_distribution,comp_ims_median
#from mine_images_median_diff_abs import *

ims_blend_diff_pieces = []
dist_ims_feas = []
ims_syn_test_org_pieces = []
ims_syn_test_pieces = []
dist_ims_test_feas = []
"""W = (32,32)
shift = (32,32)
shift1 = (32,32)
NC_THD = 10
IMG_SHAPE = (32,32)"""
W = (64,64)
shift = (64,64)
shift1 = (64,64)
NC_THD = 10
IMG_SHAPE = (64,64)
"""W = (48,48)
shift = (48,48)
shift1 = (48,48)
NC_THD = 10
IMG_SHAPE = (48,48)"""
HUM_RATIO = 0.05
SS = 0
E = 0

#load patchs for blending
def load_add_ims(fpath):
    add_ims = []
    add_fp = "hum_nat/add_targets"
    if fpath.startswith('all'):
        add_dirs = [f for f in next(walk(add_fp))[1]]
        add_dirs.sort()
        #print("add dirs:",add_dirs)
        for dir in add_dirs:
            add_im = []
            add_fpp = add_fp+'/'+dir
            add_fs = [f for f in listdir(add_fpp) if isfile(join(add_fpp,f)) and (f.endswith('.tif') or f.endswith('.png'))]
            add_fs.sort()
            for add_f in add_fs:
                add_fn = join(add_fpp,add_f)
                add_im.append(cv2.imread(add_fn, cv2.IMREAD_UNCHANGED))
            add_ims.append(add_im)
    else:
        add_dirs = [f for f in next(walk(add_fp))[1] if f.startswith('add') and f.endswith(fpath.split('_',1)[0])]
        add_dirs.sort()
        add_fpp = add_fp+'/'+ add_dirs[0]       
        add_fs = [f for f in listdir(add_fpp) if isfile(join(add_fpp,f)) and (f.endswith(".tif") or f.endswith(".png"))]
        add_fs.sort()
        add_im = []
        for f in add_fs:
            fn = join(add_fpp,f)
            add_im.append(cv2.imread(fn, cv2.IMREAD_UNCHANGED))
        add_ims.append(add_im)
    return add_ims

#write location features for all human change images of each site
def write_loc_features(fpath, dir, hum_chg_locs):
    fn = dir
    print("write loc features:",fn)
    f = open(fpath+'/features_'+fn+".pkl","wb")
    pickle.dump(hum_chg_locs,f)
    f.close()
    
#generate natural change image for the first half images
def images_hum(dir, ims, add_ims):
    rng = np.random
    #mid = int(len(ims)/2)
    c = 0
    hum_chg_locs = []
    r_ival = max([max(im.shape[0],im.shape[1]) for im in add_ims])
    for im in ims:
        hum_chg_loc = []
        rn = int(im.shape[0]/r_ival)
        cn = int(im.shape[1]/r_ival)
        cols = [rng.permutation(cn) for i_ in range(rn)]
        cn1 = int(1*cn/2)
        for i_ in range(rn):
            for j_ in range(cn1):
                inx = (i_*cn1 + j_)%len(add_ims)
                h = add_ims[inx].shape[0]-1
                w = add_ims[inx].shape[1]-1
                t = i_ * r_ival
                l = cols[i_][j_] * r_ival
                b = t + h
                r = l + w
                if b > im.shape[0] or  r > im.shape[1]:
                    continue
                if 0 not in im[t:b,l:r,3]:
                    for ii in range(b-t+1):
                        for jj in range(r-l+1):
                            if add_ims[inx][ii,jj,3] != 0:
                                im[t+ii,l+jj,0:3] = add_ims[inx][ii,jj,0:3]
                    loc = (t,l,h,w)
                    hum_chg_loc.append((loc,inx))
        hum_chg_locs.append(hum_chg_loc)
    write_loc_features('features', dir , hum_chg_locs)    

#generate human change images for the other half images
def images_nat(ims):
    for im in ims:
        ims_blend_nat.append(im)
    for im in ncs:
        ims_blend_nc.append(im)
        
def write_blend_json_ims(root_src, root_des, js, tp):
    if not exists(root_des):
        makedirs(root_des)
    fns = root_des + "/*"
    fs = glob.glob(fns)
    for f in fs:
        if tp == 1 and f.startswith('ims_blend_diff_hum'):
            remove(f)
        elif tp == 2 and f.startswith('ims_blend_diff_nat'):
            remove(f)
        elif tp == 3 and f.startswith('ims_blend_diff_nc'):
            remove(f)
        elif tp == 0:
            remove(f)
    for _,f in enumerate(js):
        rs = join(root_src,f)+'.json'
        if any([join(root_src,t) for t in listdir(root_src) if join(root_src,t) == rs]) == False:
            print("no such json file",rs)
            return
        if tp == 1:
            if _ < 10:
                fn = root_des + "/ims_blend_diff_hum0" + str(_) + ".json"
            else:
                fn = root_des + "/ims_blend_diff_hum" + str(_) + ".json"
            copyfile(rs,fn)
        if tp == 2:
            if _ < 10:
                fn = root_des + "/ims_blend_diff_nat0" + str(_) + ".json"
            else:
                fn = root_des + "/ims_blend_diff_nat" + str(_) + ".json"
            copyfile(rs,fn)
        if tp == 3:
            if _ < 10:
                fn = root_des + "/ims_blend_diff_nc0" + str(_) + ".json"
            else:
                fn = root_des + "/ims_blend_diff_nc" + str(_) + ".json"
            copyfile(rs,fn)
        if tp == 0:
            fn = join(root_des,f) + ".json"
            copyfile(rs,fn)
            
def write_blend_ims(fpath,ims, tp):
    if not exists(fpath):
        makedirs(fpath)
    fns = fpath + "/*"
    fs = glob.glob(fns)
    for f in fs:
        if tp == 1 and f.startswith('ims_blend_hum'):
            remove(f)
        elif tp == 2 and f.startswith('ims_blend_nat'):
            remove(f)
        elif tp == 3 and f.startswith('ims_blend_nc'):
            remove(f)
    for _,im in enumerate(ims):
        if tp == 1:
            if _ < 10:
                fn = fpath + "/ims_blend_hum0" + str(_) + ".png"
            else:
                fn = fpath + "/ims_blend_hum" + str(_) + ".png"
            cv2.imwrite(fn,im)
        if tp == 2:
            if _ < 10:
                fn = fpath + "/ims_blend_nat0" + str(_) + ".png"
            else:
                fn = fpath + "/ims_blend_nat" + str(_) + ".png"
            cv2.imwrite(fn,im)
        if tp == 3:
            if _ < 10:
                fn = fpath + "/ims_blend_nc0" + str(_) + ".png"
            else:
                fn = fpath + "/ims_blend_nc" + str(_) + ".png"
            cv2.imwrite(fn,im)
        
#blend the original image with cloud image
def images_blend(fpath, add_ims):
    #read directory names under 'hum_nat'
    add_fp = 'hum_nat'
    add_fp1 = join(add_fp,'org_ims/hum')
    add_fp2 = join(add_fp,'org_ims/nat')
    add_fp3 = join(add_fp,'org_ims/nc')
    add_fp4 = '../org_json'
    if fpath.startswith('all'):
        hum_dirs = [f for f in next(walk(add_fp1))[1]]
        hum_dirs.sort()
        nat_dirs = [f for f in next(walk(add_fp2))[1]]
        nat_dirs.sort()
        nc_dirs = [f for f in next(walk(add_fp3))[1]]
        nc_dirs.sort()
    else:
        hum_dirs = [f for f in next(walk(add_fp1))[1] if f.endswith(fpath.split('_',1)[0])]
        hum_dirs.sort()
        nat_dirs = [f for f in next(walk(add_fp2))[1] if f.endswith(fpath.split('_',1)[0])]
        nat_dirs.sort()
        nc_dirs = [f for f in next(walk(add_fp3))[1] if f.endswith(fpath.split('_',1)[0])]
        nc_dirs.sort()
        
    #blend each image of each directory and save to directories under 'syn'
    syn_fp = 'syn'
    print("blend hum dirs:",hum_dirs)
    for _,dir in enumerate(hum_dirs):
        fp = join(add_fp1,dir)
        hum_fns = [f for f in listdir(fp) if isfile(join(fp,f)) and (f.endswith('.tif') or f.endswith('.png'))]
        hum_fns.sort()
        print("hum files:",hum_fns)
        hum_ims = [cv2.imread(join(fp,f),cv2.IMREAD_UNCHANGED) for f in hum_fns if f.endswith('.tif') or f.endswith('.png')]
        images_hum(dir, hum_ims,add_ims[_])
        write_blend_ims(syn_fp + '/' + dir,hum_ims,1)
    for _,dir in enumerate(nat_dirs):
        fp = join(add_fp2,dir)
        nat_fns = [f for f in listdir(fp) if isfile(join(fp,f)) and (f.endswith('.tif') or f.endswith('.png'))]
        nat_fns.sort()
        nat_ims = [cv2.imread(join(fp,f),cv2.IMREAD_UNCHANGED) for f in nat_fns if f.endswith('.tif') or f.endswith('.png')]
        #images_nat_nc(nat_ims)
        write_blend_ims(syn_fp + '/' + dir,nat_ims,2)
    for _,dir in enumerate(nc_dirs):
        fp = join(add_fp3,dir)
        nc_fns = [f for f in listdir(fp) if isfile(join(fp,f)) and (f.endswith('.tif') or f.endswith('.png'))]
        nc_fns.sort()
        nc_ims = [cv2.imread(join(fp,f),cv2.IMREAD_UNCHANGED) for f in nc_fns if f.endswith('.tif') or f.endswith('.png')]
        #images_nat_nc(nc_ims)
        write_blend_ims(syn_fp + '/' + dir,nc_ims,3)
    #copy json files to directory syn_diff/json_train
    for _,dir in enumerate(hum_dirs):
        fp = join(add_fp1,dir)
        hum_fns = [f.split('.',1)[0] for f in listdir(fp) if isfile(join(fp,f)) and (f.endswith('.tif') or f.endswith('.png'))]
        hum_fns.sort()
        write_blend_json_ims(join(add_fp4,dir),"syn_diff/json_train/"+dir,hum_fns,1)
    for _,dir in enumerate(nat_dirs):
        fp = join(add_fp2,dir)
        nat_fns = [f.split('.',1)[0] for f in listdir(fp) if isfile(join(fp,f)) and (f.endswith('.tif') or f.endswith('.png'))]
        nat_fns.sort()
        write_blend_json_ims(join(add_fp4,dir),"syn_diff/json_train/"+dir,nat_fns,2)
    for _,dir in enumerate(nc_dirs):
        fp = join(add_fp3,dir)
        nc_fns = [f.split('.',1)[0] for f in listdir(fp) if isfile(join(fp,f)) and (f.endswith('.tif') or f.endswith('.png'))]
        nc_fns.sort()
        write_blend_json_ims(join(add_fp4,dir),"syn_diff/json_train/"+dir,nc_fns,3)

    #copy json files to directory test_diff/json_train
    add_fp5 = 'test_diff/org_ims'
    dirs = next(walk(add_fp5))[1]
    for _,dir in enumerate(dirs):
        fp = join(add_fp5,dir)
        fns = [f.split('.',1)[0] for f in listdir(fp) if isfile(join(fp,f)) and (f.endswith('.tif') or f.endswith('.png'))]
        fns.sort()
        write_blend_json_ims(join(add_fp4,dir),"test_diff/json_test/"+dir,fns,0)
    
    
#read blended images 
def read_blend(fpath):
    #syn single site or all sites
    ims_blend_hums = []
    ims_blend_nats = []
    ims_blend_ncs = []
    if fpath.endswith('syn'):
        print("generating synthetic images for all sites or single site......")
        add_ims = load_add_ims(fpath)
        images_blend(fpath, add_ims)
    else:
        if fpath.startswith('all'):
            dirs = next(walk('syn'))[1]
            dirs.sort()
        else:
            dirs = [fpath.split('_',1)[0]]
        if len(dirs) == 0:
            print("please create synthetic images first!")
            return
        for dir in dirs:
            ims_blend_hum = []
            ims_blend_nat = []
            ims_blend_nc = []
            fp = 'syn/'+dir
            fs = [f for f in listdir(fp) if isfile(join(fp,f)) and (f.endswith('.tif') or f.endswith('.png'))]
            fs.sort()
            if len(fs) == 0:
                print("no synthetic images for site",dir)
            for f in fs:
                fn = join(fp,f)
                im = cv2.imread(fn, cv2.IMREAD_UNCHANGED).astype(np.float)
                if f.startswith('ims_blend_nat'):
                    ims_blend_nat.append(im)
                elif f.startswith('ims_blend_hum'):
                    ims_blend_hum.append(im)
                else:
                    ims_blend_nc.append(im)
            ims_blend_hums.append(ims_blend_hum)
            ims_blend_nats.append(ims_blend_nat)
            ims_blend_ncs.append(ims_blend_nc)
    return (ims_blend_hums,ims_blend_nats,ims_blend_ncs)
           
            
def write_blend_diff(fpath, ims_blend_diffs):
    fp = fpath
    fpp =fpath + "/*"
    if not exists(fpath):
        makedirs(fpath)
    fs = glob.glob(fpp)
    for f in fs:
        remove(f)
    ims_blend_diff_hum,ims_blend_diff_nat,ims_blend_diff_nc = ims_blend_diffs
    for _,im in enumerate(ims_blend_diff_hum):
        if _ < 10:
            fn = fp + "/ims_blend_diff_hum0" + str(_) + ".png"
        else:
            fn = fp + "/ims_blend_diff_hum" + str(_) + ".png"
        cv2.imwrite(fn,im)
    for _,im in enumerate(ims_blend_diff_nat):
        if _ < 10:
            fn = fp + "/ims_blend_diff_nat0" + str(_) + ".png"
        else:
            fn = fp + "/ims_blend_diff_nat" + str(_) + ".png"
        cv2.imwrite(fn,im)
    for _,im in enumerate(ims_blend_diff_nc):
        if _ < 10:
            fn = fp + "/ims_blend_diff_nc0" + str(_) + ".png"
        else:
            fn = fp + "/ims_blend_diff_nc" + str(_) + ".png"
        cv2.imwrite(fn,im)

def write_test_diff(fp, ims_tests):
    fpp =fp + "/*"
    if not exists(fp):
        makedirs(fp)
    fs = glob.glob(fpp)
    for f in fs:
        remove(f)
    for _,im in enumerate(ims_tests):
        if _ < 10:
            fn = join(fp,'ims_blend_diff0') + str(_) + ".png"
        else:
            fn = join(fp,'ims_blend_diff') + str(_) + ".png"
        cv2.imwrite(fn,im)

#normalize each difference image
def norm_img_diff(ims_blend_diff_hn):
    m = 300
    M = -300
    for im in ims_blend_diff_hn:
        alpha = np.tile(im[:,:,3].ravel().astype(np.object),3)
        rgb = im[:,:,0:3].ravel().astype(np.object)
        rgb[alpha==0] = None
        rgb = list(filter(None.__ne__,rgb))
        m_tmp= np.amin(rgb)
        M_tmp = np.amax(rgb)
        if m_tmp < m:
            m = m_tmp
        if M_tmp > M:
            M = M_tmp
    print(m,M)
    for im in ims_blend_diff_hn:
        for i in range(im.shape[0]):
            for j in range(im.shape[1]):
                #for k in range(im.shape[2]):
                if im[i,j,3] != 0:
                    im[i,j,0:3] = (im[i,j,0:3] - m)/(M - m) * 255
                    
#compute the difference bewteen median and individual image for each image row
def images_difference_abs(fpath, ims_med, ims_blends):
    ims_blend_hum,ims_blend_nat,ims_blend_nc = ims_blends
    ims_blend_diff_hum = []
    ims_blend_diff_nat = []
    ims_blend_diff_nc = []
    for im in ims_blend_hum:
        imc = copy.deepcopy(im)
        for i in range(imc.shape[0]):
            for j in range(imc.shape[1]):
                if imc[i,j,3] != 0:
                    imc[i,j,0:3] = np.abs(imc[i,j,0:3] - ims_med[i,j,0:3])
        ims_blend_diff_hum.append(imc)
    for im in ims_blend_nat:
        imc = copy.deepcopy(im)
        for i in range(imc.shape[0]):
            for j in range(imc.shape[1]):
                if imc[i,j,3] != 0:
                    imc[i,j,0:3] = np.abs(imc[i,j,0:3] - ims_med[i,j,0:3])
        ims_blend_diff_nat.append(imc)
    for im in ims_blend_nc:
        imc = copy.deepcopy(im)
        for i in range(imc.shape[0]):
            for j in range(imc.shape[1]):
                if imc[i,j,3] != 0:
                    imc[i,j,0:3] = np.abs(imc[i,j,0:3] - ims_med[i,j,0:3])
        ims_blend_diff_nc.append(imc)
    ims_blend_diff_tmp = list(ims_blend_diff_hum)
    ims_blend_diff_tmp.extend(ims_blend_diff_nat)
    ims_blend_diff_tmp.extend(ims_blend_diff_nc)
    norm_img_diff(ims_blend_diff_tmp)
    write_blend_diff(fpath, (ims_blend_diff_hum,ims_blend_diff_nat,ims_blend_diff_nc))

#read location of human changes for all sites
def read_loc_features(fpath):
    fs = [f for f in listdir(fpath) if isfile(join(fpath,f)) and f.endswith('.pkl')]
    fs.sort()
    hum_chg_locs = []
    for f in fs:
        fn = open(join(fpath,f),"rb")
        hum_chg_loc = pickle.load(fn)
        hum_chg_locs.append(hum_chg_loc)
        fn.close()
    return hum_chg_locs
              
#load difference images for single or all sites
def read_difference_all(fpath):
    dirs = next(walk(fpath))[1]
    dirs.sort()
    ims_blend_diff_hums = []
    ims_blend_diff_nats = []
    ims_blend_diff_ncs = []
    hum_chg_locs = []
    for dir in dirs:
        fp = join(fpath,dir)
        fs = [f for f in listdir(fp) if isfile(join(fp,f)) and (f.endswith('.tif') or f.endswith('.png'))];
        fs.sort()
        ims_blend_diff_hum_ = []
        ims_blend_diff_nat_ = []
        ims_blend_diff_nc_ = []
        for f in fs:
            fn = join(fp,f)
            im = cv2.imread(fn, cv2.IMREAD_UNCHANGED)
            if f.startswith('ims_blend_diff_hum'):
                ims_blend_diff_hum_.append(im)
            elif f.startswith('ims_blend_diff_nat'):
                ims_blend_diff_nat_.append(im)
            else:
                ims_blend_diff_nc_.append(im)
        ims_blend_diff_hums.append(ims_blend_diff_hum_)
        ims_blend_diff_nats.append(ims_blend_diff_nat_)
        ims_blend_diff_ncs.append(ims_blend_diff_nc_)
    hum_chg_locs = read_loc_features('features')
    return (ims_blend_diff_hums,ims_blend_diff_nats,ims_blend_diff_ncs),hum_chg_locs

#extract feas from each json file
def get_feas_from_json(d):
    #f1(1/2/3/4,spring/sum/autum/winter),f2(1/2/3,morning/afternoon/night),f3(1/2/3/4,N-E,E-S,S-W,W-N),f4(1/2/3,clear/betw/cloudy)
    date = datetime.strptime(d["properties"]["acquired"],"%Y-%m-%dT%H:%M:%S.%fZ")
    #date = datetime.strptime(d["properties"]["acquired"][0:10],"%Y-%m-%d")
    if 3<=date.month<=5:
        f1 = 1
    elif 6<=date.month <=8:
        f1 = 2
    elif 9<=date.month <=11:
        f1 = 3
    else:
        f1 = 4
    time = date
    #time = datetime.strptime(d["properties"]["acquired"][11:19],"%H:%M:%S")
    if time.hour < 11:
        f2 = 1
    elif time.hour < 17:
        f2 = 2
    else:
        f2 = 3
    sun = d["properties"]["sun_azimuth"]
    if sun < 90:
        f3 = 1
    elif sun < 180:
        f3 = 2
    elif sun < 270:
        f3 = 3
    else:
        f3 =4
    cloud = d["properties"]["cloud_cover"]
    if cloud < 0.1:
        f4 = 1
    elif cloud < 0.5:
        f4 = 2
    else:
        f4 = 3
    feas = [f1,f2,f3,f4]
    return feas

#get extra features for revelent directories    
def get_feas(fpath,dirs,tp):
    dist_extra_feas = []
    for _,dir in enumerate(dirs):
        fp = join(fpath,dir)
        if tp == 0:
            fns = [f for f in listdir(fp) if isfile(join(fp,f)) and f.endswith('.json') and f.startswith('ims_blend_diff_hum')]
        elif tp == 1:
            fns = [f for f in listdir(fp) if isfile(join(fp,f)) and f.endswith('.json') and f.startswith('ims_blend_diff_nat')]
        else:
            fns = [f for f in listdir(fp) if isfile(join(fp,f)) and f.endswith('.json') and f.startswith('ims_blend_diff_nc')]
        fns.sort()
        #print("json file names",fns)
        dist_fea = []
        for f in fns:
            fn = join(fp,f)
            with open(fn) as j_f:    
                d = json.load(j_f)
            feas = get_feas_from_json(d)
            dist_fea.append(feas)
        dist_extra_feas.append(dist_fea)
    return dist_extra_feas

#read extra features from json files
def read_extra_feas(fpath):
    dirs = [f for f in next(walk(fpath))[1]]
    dirs.sort()
    dist_feas_hums = get_feas(fpath,dirs,0)
    dist_feas_nats = get_feas(fpath,dirs,1)
    dist_feas_ncs = get_feas(fpath,dirs,2)
    return (dist_feas_hums,dist_feas_nats,dist_feas_ncs)
        
#read difference images for all or single sites 
def read_difference_abs(fpath):
    hum_chg_locs = []
    ims_blend_diffs = ()
    dist_extra_feas = []
    if fpath.endswith('diff'):
        ims_blends = read_blend(fpath)
        ims_blend_hums,ims_blend_nats,ims_blend_ncs = ims_blends
        print("len of hums,nats,ncs for all sites",len(ims_blend_hums),len(ims_blend_nats),len(ims_blend_ncs))
        if fpath.startswith('all'):
            dirs = next(walk('syn'))[1] #for training
            dirs.sort()
            print("syn dirs",dirs)
        else:
            dirs = [fpath.split('_',1)[0]]
        #generate difference images for training
        for _,dir in enumerate(dirs):
            print(dir,len(ims_blend_hums[_]),len(ims_blend_nats[_]),len(ims_blend_ncs[_]))
            ims_blends = (ims_blend_hums[_],ims_blend_nats[_],ims_blend_ncs[_])
            ims_med = cv2.imread("figures_mine/"+dir+"_median.png",cv2.IMREAD_UNCHANGED)
            if ims_med == None:
                print("please create median image for",dir)
                return 
                #ims_med = read_blend_med(ims_blend_hum[_])
                #cv2.imwrite("figures_mine/"+dir+"_median.png",ims_med)
            images_difference_abs('syn_diff/diff_ims/'+dir, ims_med, ims_blends)
    else:
        ims_blend_diffs,hum_chg_locs = read_difference_all("syn_diff/diff_ims")
        dist_extra_feas = read_extra_feas('syn_diff/json_train')
        
    return ims_blend_diffs,hum_chg_locs,dist_extra_feas

#check if human change occurred in add_ims
def ishum(add_ims,row,item):
    for r in row:
        inx = r[1]
        (t,l,h,w) = r[0]
        b = t+h
        r = l+w
        t1 = item[0]
        l1 = item[1]
        b1 = item[2]
        r1 = item[3]
        if (t<=t1<b1<=b and l<=l1<r1<=r):
            temp = add_ims[inx][t1-t:b1-t,l1-l:r1-l,3]
            ratio = np.count_nonzero(temp)/(W[0]*W[1])
            if 255 in add_ims[inx][t1-t:b1-t,l1-l:r1-l,3] and ratio > HUM_RATIO:
            #if ratio > HUM_RATIO:
                return True
        elif (l1<l<r1 and t1<t<b1):
            temp = add_ims[inx][0:b1-t,0:r1-l,3]
            ratio = np.count_nonzero(temp)/(W[0]*W[1])
            if 255 in add_ims[inx][0:b1-t,0:r1-l,3] and ratio > HUM_RATIO:
            #if ratio > HUM_RATIO:
                return True
        elif (l1<r<r1 and t1<t<b1):
            temp = add_ims[inx][0:b1-t,l1-l:r-l,3]
            ratio = np.count_nonzero(temp)/(W[0]*W[1])
            if 255 in add_ims[inx][0:b1-t,l1-l:r-l,3] and ratio >  HUM_RATIO:
            #if ratio >  HUM_RATIO:
                return True
        elif (t1<b<b1 and l1<l<r1):
            temp = add_ims[inx][t1-t:b-t,0:r1-l,3]
            ratio = np.count_nonzero(temp)/(W[0]*W[1])
            if 255 in add_ims[inx][t1-t:b-t,0:r1-l,3] and ratio > HUM_RATIO:
            #if ratio > HUM_RATIO:
                return True
        elif (l1<r<r1 and t1<b<b1):
            temp = add_ims[inx][t1-t:b-t,l1-l:r-l,3]
            ratio = np.count_nonzero(temp)/(W[0]*W[1])
            if 255 in add_ims[inx][t1-t:b-t,l1-l:r-l,3] and ratio > HUM_RATIO:
            #if ratio > HUM_RATIO:
                return True
        elif (t1<t<b1 and l<=l1<r1<=r):
            temp = add_ims[inx][0:b1-t,l1-l:r1-l,3]
            ratio = np.count_nonzero(temp)/(W[0]*W[1])
            if 255 in add_ims[inx][0:b1-t,l1-l:r1-l,3] and ratio > HUM_RATIO:
            #if ratio > HUM_RATIO:
                return True
        elif (t1<b<b1 and l<=l1<r1<=r):
            temp = add_ims[inx][t1-t:b-t,l1-l:r1-l,3]
            ratio = np.count_nonzero(temp)/(W[0]*W[1])
            if 255 in add_ims[inx][t1-t:b-t,l1-l:r1-l,3] and ratio > HUM_RATIO:
            #if ratio > HUM_RATIO:
                return True
        elif (t<=t1<b1<=b and l1<l<r1):
            temp = add_ims[inx][t1-t:b-t,0:r1-l,3]
            ratio = np.count_nonzero(temp)/(W[0]*W[1])
            if 255 in add_ims[inx][t1-t:b-t,l1-l:r1-l,3] and ratio > HUM_RATIO:
            #if ratio > HUM_RATIO:
                return True
        elif (t<=t1<b1<=b and l1<r<r1):
            temp = add_ims[inx][t1-t:b-t,l1-l:r-l,3]
            ratio = np.count_nonzero(temp)/(W[0]*W[1])
            if 255 in add_ims[inx][t1-t:b-t,l1-l:r1-l,3] and ratio > HUM_RATIO:
            #if ratio > HUM_RATIO:
                return True
    return False

#divide each image here    
def images_diff_divide_hum_sub(ims_blend_diff_hum, add_ims, dist_extra_feas_hum, hum_chg_locs,ims_blend_orgs):
    #global n1,n2,n3
    n1 = 0
    n3 = 0
    diff_pieces = []
    org_pieces = []
    test_id = [0]
    for _,im_diff in enumerate(ims_blend_diff_hum):
        ims_syn_test_piece = []
        dist_ims_test_fea = []
        m_org_2d_pieces = []
        k = 0
        row = hum_chg_locs[_]
        row = sorted(row, key=lambda x: x[0][1])
        row0 = [(x[0][0],x[0][1]) for x in row]
        if _ in test_id:
            s_r = W[0]
            s_c = W[1]
        else:
            s_r = shift[0]
            s_c = shift[1]
        im_org = ims_blend_orgs[_]
        for k1 in range(0,im_diff.shape[0],s_r):
            m_org_pieces = []
            for k2 in range(0,im_diff.shape[1],s_c):
                b = k1 + W[0]
                r = k2 + W[1]
                im_win = im_diff[k1:b,k2:r,0:3]
                if (b > im_diff.shape[0] or r > im_diff.shape[1] or 0 in im_diff[k1:b,k2:r,3]) and _ not in test_id:
                    continue
                ih = ishum(add_ims,row,(k1,k2,b,r))
                if  ih == True:
                    im_piece = (im_win,0)
                else:
                    im_piece = (im_win,2)
                #if _ == len(ims_blend_diff_hum)-1: #take image 28 of mine to be a testing image
                if _ in test_id: #take image 28 of mine to be a testing image
                    #print(k1,k2,ih,im_piece[1])
                    im_org_win = im_org[k1:b,k2:r,:]
                    if b > im_diff.shape[0]:
                        zos = np.zeros((W[0]-im_org_win.shape[0],im_org_win.shape[1],4))
                        im_org_win = np.vstack((im_org_win,zos))
                        im_piece = (np.vstack((im_piece[0],zos[:,:,0:3])),2)
                    if r > im_diff.shape[1]:
                        zos = np.zeros((im_org_win.shape[0],W[1]-im_org_win.shape[1],4))
                        im_org_win = np.hstack((im_org_win,zos))
                        im_piece = (np.hstack((im_piece[0],zos[:,:,0:3])),2)
                    #dist_ims_test_fea.append([x for x in feas])
                    ims_syn_test_piece.append(im_piece)
                    dist_ims_test_fea.append([x for x in dist_extra_feas_hum[_]])
                    m_org_pieces.append(im_org_win)
                else:
                    if im_piece[1] == 2:
                        """n3 += 1
                        ims_blend_diff_pieces.append(im_piece)
                        #dist_ims_feas.append([x for x in feas])
                        dist_ims_feas.append([x for x in dist_extra_feas_hum[_]])"""
                    else:
                        """ims_blend_diff_pieces.append(im_piece)
                        dist_ims_feas.append([x for x in dist_extra_feas_hum[_]])"""
                        ims_blend_diff_pieces.append(im_piece)
                        dist_ims_feas.append([x for x in dist_extra_feas_hum[_]])
                        #dist_ims_feas.append([x for x in feas])
                        n1 += 1     
            if (len(m_org_pieces)) > 0:
                m_org_2d_pieces.append(m_org_pieces)
            #if i == SYN_TEST_IMG_HUM and indx == 0:
            #    ims_syn_test_org_piece.append(m_org_2d_pieces)
        if _ in test_id:
            dist_ims_test_feas.append(dist_ims_test_fea)
            ims_syn_test_pieces.append(ims_syn_test_piece)
            ims_syn_test_org_pieces.append(m_org_2d_pieces)
    return n1,n3
                  
def images_diff_divide_nn_sub(ims_blend_diff_nn,dist_extra_feas_nn,ims_blend_orgs,tp,aa):
    global SS,E
    n = 0
    test_id = []
    if tp == 0:
        cs = 1 #nat change
        #if (len(ims_blend_diff_nn) >= 10):
        test_id = [0,len(ims_blend_diff_nn)-1]
    else:
        cs = 2 #no change
        #if (len(ims_blend_diff_nn) >= 10):
        test_id = [len(ims_blend_diff_nn)-1,len(ims_blend_diff_nn)-2]
            
    for _, im_diff in enumerate(ims_blend_diff_nn):
        ims_syn_test_piece = []
        dist_ims_test_fea = []
        m_org_2d_pieces = []
        if _ in test_id:
            s_r = W[0]
            s_c = W[1]
        else:
            s_r = shift1[0]
            s_c = shift1[1]
        im_org = ims_blend_orgs[_]
        #choose the top half for 0 or the bottom half for 1 for no change
        half = np.random.randint(2)
        for k1 in range(0,im_diff.shape[0],s_r):
            m_org_pieces = []
            for k2 in range(0,im_diff.shape[1],s_c):
                b = k1 + W[0]
                r = k2 + W[1]
                if (b > im_diff.shape[0] or r > im_diff.shape[1] or 0 in im_diff[k1:b,k2:r,3]) and _ not in test_id:
                    continue
                im_win = im_diff[k1:b,k2:r,0:3]
                if _ in test_id:
                    im_piece = (im_win,cs)
                    im_org_win = im_org[k1:b,k2:r,:]
                    """if b < im_diff.shape[0] and r < im_diff.shape[1] and tp == 0 and  0 not in im_diff[k1:b,k2:r,3]:
                        if aa == 2:
                            if SS == 0:
                                SS = len(ims_blend_diff_pieces)
                            E = len(ims_blend_diff_pieces)
                        n += 1
                        ims_blend_diff_pieces.append(im_piece)
                        dist_ims_feas.append([x for x in dist_extra_feas_nn[_]])"""
                    if 0 in im_diff[k1:b,k2:r,3]:
                        im_piece = (im_win,2)
                    if b > im_diff.shape[0]:
                        zos = np.zeros((W[0]-im_org_win.shape[0],im_org_win.shape[1],4))
                        im_org_win = np.vstack((im_org_win,zos))
                        im_piece = (np.vstack((im_piece[0],zos[:,:,0:3])),2)
                    if r > im_diff.shape[1]:
                        zos = np.zeros((im_org_win.shape[0],W[1]-im_org_win.shape[1],4))
                        im_org_win = np.hstack((im_org_win,zos))
                        im_piece = (np.hstack((im_piece[0],zos[:,:,0:3])),2)
                    ims_syn_test_piece.append(im_piece)
                    dist_ims_test_fea.append([x for x in dist_extra_feas_nn[_]])
                    #dist_ims_test_fea.append(feas)
                    m_org_pieces.append(im_org_win)
                else:
                    if np.sum(im_win.ravel()) < NC_THD*W[0]*W[1]*3 and tp == 0:
                        continue
                    if im_diff.shape[0] > 700 or im_diff.shape[1] > 500:
                        if half == 0 and tp == 1 and  b > np.int(im_diff.shape[0]/2):
                            continue
                        if half == 1 and tp == 1 and b < np.int(im_diff.shape[0]/2):
                            continue
                    im_piece = (im_win,cs)
                    n += 1
                    ims_blend_diff_pieces.append(im_piece)
                    dist_ims_feas.append([x for x in dist_extra_feas_nn[_]])
                    #dist_ims_feas.append([x for x in feas])
            if (len(m_org_pieces)) > 0:
                m_org_2d_pieces.append(m_org_pieces)
        if _ in test_id:
            dist_ims_test_feas.append(dist_ims_test_fea)
            ims_syn_test_pieces.append(ims_syn_test_piece)
            ims_syn_test_org_pieces.append(m_org_2d_pieces)
    return n

def read_ims_info():
    ims_list_info = []
    with open("ims_list_info_chg.pkl","rb") as f:
        ims_list_info = pickle.load(f)
    f.close()
    return ims_list_info

def images_diff_divide_extra_all():
    #read org and diff images from train_chg
    counts = [0,0,0]
    train_new_diff = []
    train_new_json = []
    train_new_ims_info = []
    dir_diff = "../org_diff"
    dir_json = "../org_json"
    train_new_ims_info = read_ims_info()
    for _,im_info in enumerate(train_new_ims_info):
        site = im_info["site"]
        target_name = im_info["target_name"]
        t_names = target_name.split("$")
        im_seq = t_names[2].split(".")[0]
        i_diff_name = t_names[0]+"_org"+im_seq+".tif"
        #print("org_img_name correct?",i_diff_name)
        im = cv2.imread(join(dir_diff,site,i_diff_name),cv2.IMREAD_UNCHANGED)
        train_new_diff.append(im)
        i_json_name = t_names[0]+"_org"+im_seq+".json"
        with open(join(dir_json,site,i_json_name)) as j_f:
            d = json.load(j_f)
            train_new_json.append(d)
    for _,im in enumerate(train_new_diff):
        im_info_labels = train_new_ims_info[_]["img_labels"]
        #if (len(im_info_labels) < 20):
        #    print(im_info_labels)
        im_json = train_new_json[_]
        n = 0
        feas = get_feas_from_json(im_json)
        for i in range(0,im.shape[0],W[0]):
            for j in range(0,im.shape[1],W[1]):
                label = im_info_labels[n]
                t = i
                b = i+W[0]
                l = j
                r = j+W[1]
                if b >= im.shape[0] or r >= im.shape[1] or 0 in im[t:b,l:r,3]:
                    continue;
                if im_info_labels[n] == 2:
                    n += 1
                    continue
                im_piece = (im[t:b,l:r,0:3],im_info_labels[n])
                ims_blend_diff_pieces.append(im_piece)
                dist_ims_feas.append(feas)
                #if im_info_labels[n] == 2 and counts[2] > 3000:
                counts[im_info_labels[n]] += 1
                n += 1
    print("extra counts:",counts)
    return counts
        
def read_hum_extra_pieces(path = "mine_diff_pieces"):
    global n1
    fs = [f for f in listdir(path) if isfile(join(path,f)) and (f.endswith(".tif") or f.endswith(".png"))]
    fs.sort()
    for f in fs:
        fn = join(path,f)
        im = cv2.imread(fn,cv2.IMREAD_UNCHANGED)
        im_piece = (im,0)
        ims_blend_diff_pieces.append(im_piece)
        dist_ims_feas.append([2,2,0,1])
        n1 += 1

#break the each difference image into pieces with equal size and label them
def images_diff_divide_label(add_ims,ims_blend_diffs, dist_extra_feas, hum_chg_locs):
    #global n1,n2,n3
    n1 = 0
    n2 = 0
    n3 = 0
    rng = np.random
    ims_blend_diff_hums,ims_blend_diff_nats,ims_blend_diff_ncs = ims_blend_diffs
    dist_extra_feas_hums, dist_extra_feas_nats, dist_extra_feas_ncs = dist_extra_feas
    ims_blend_hums,ims_blend_nats,ims_blend_ncs = read_blend('all')
    for _,ims_blend_diff_hum in enumerate(ims_blend_diff_hums):
        c1,c3= images_diff_divide_hum_sub(ims_blend_diff_hum,add_ims[_],dist_extra_feas_hums[_],hum_chg_locs[_],ims_blend_hums[_])
        n1 += c1
        n3 += c3
    for _,ims_blend_diff_nat in enumerate(ims_blend_diff_nats):
        c2 = images_diff_divide_nn_sub(ims_blend_diff_nat,dist_extra_feas_nats[_],ims_blend_nats[_],0,_)
        n2 += c2
    for _,ims_blend_diff_nc in enumerate(ims_blend_diff_ncs):
        c3 = images_diff_divide_nn_sub(ims_blend_diff_nc,dist_extra_feas_ncs[_],ims_blend_ncs[_],1,_)
        n3 += c3
    print("class0/1/2(hum change/nat change/no change):",n1,n2,n3)
    c1,c2,c3 = images_diff_divide_extra_all();
    n1 += c1
    n2 += c2
    n3 += c3
    #print(np.array(ims_syn_test_org_pieces).shape, np.array(dist_ims_test_feas).shape, np.array(ims_syn_test_pieces).shape)
    #read_hum_extra_pieces()
    print("class0/1/2(hum change/nat change/no change):",n1,n2,n3)
    
#write all difference pieces to the disk
def images_diff_pieces_write(ims_pieces = ims_blend_diff_pieces):
    #remove history files from the folder first
    ps = glob.glob("mine_pieces/*.png")
    for f in ps:
        remove(f)
        
    for _,im in enumerate(ims_pieces):
        if im[1] == 1:
            fn = 'mine_pieces/image' + str(_) + '.png'
            cv2.imwrite(fn,im[0])
    
def run_mine_syn():
    if sys.argv[1].endswith("syn"):
        read_blend(sys.argv[1])
    elif sys.argv[1].endswith("diff"):
        read_difference_abs(sys.argv[1])
    else:
        add_ims = load_add_ims(sys.argv[1])
        ims_blend_diffs,hum_chg_locs,dist_extra_feas = read_difference_abs(sys.argv[1])
        images_diff_divide_label(add_ims, ims_blend_diffs, dist_extra_feas, hum_chg_locs)
        #images_diff_pieces_write()
    
if __name__ == '__main__':
    run_mine_syn()         




