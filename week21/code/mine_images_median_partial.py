import numpy as np
from os import listdir,walk
from os.path import isfile,join
import sys

root = "../data"
sites = next(walk(root))[1]
for site in sites:
    fn_ims = []
    fn_ym_ims = []
    fs = [f for f in listdir(site) if (f.endswith(".tif") or f.endswith(".png")) and 'analytic' not in f]
    fs.sort()
    for f in fs:
        fp = join(root,site,f)
        im = cv.imread(fp, cv.IMREAD_UNCHANGED)
        if im == None or im.shape[2] < 4:
            continue
        fn_ims.append(f)
        fn_ym_ims.append(np.int(f[0:6]))
        nums,counts = np.unique(fn_ym_ims,return_counts=True)
        
