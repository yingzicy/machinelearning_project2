import numpy as np
import os, sys
import pickle,glob
import cv2
from os import listdir,remove
from os.path import join,isfile
from tkinter import *
from tkinter import messagebox
from PIL import Image, ImageTk

W = (64,64)
img_list = []
ims_org = []
current = 0
img_list_info = []
img_list_info_chg = []
B_SIZE = 1
W_THD = 500
H_THD = 500
S_THD = 8
S_THD1 = 4

def show_position(x, y):
    mousePosition = StringVar()
    mousePosition.set( "("+str(x)+","+str(y)+")")

def comp_positions(x, y):
    im_shape = img_list_info_chg[current]["img_shape"]
    print("im_shape",im_shape)
    if im_shape[0]*W[0] > W_THD or im_shape[1]*W[1] > H_THD:
        slice_pos = (int(x/(W[0]/S_THD1)),int(y/(W[1]/S_THD1)))
    else:
        slice_pos = (int(x/W[0]),int(y/W[1]))
    print("slice_pos",slice_pos)
    label_pos = slice_pos[0]*im_shape[1] + slice_pos[1]
    return slice_pos,label_pos

def chg_slice_content(x, y, type):
    BLUE = [255,0,0,255]
    RED = [0,0,255,255]
    im = cv2.imread(img_list[current][0],cv2.IMREAD_UNCHANGED)
    t_s = x - x%W[0]
    l_s = y - y%W[1]
    b_s = t_s + W[0]
    r_s = l_s + W[1]
    t = t_s + B_SIZE
    b = b_s - B_SIZE
    l = l_s +  B_SIZE
    r = r_s - B_SIZE
    #add border for each piece
    if type == 0:
        im_core = im[t:b,l:r,:]
        im_piece = cv2.copyMakeBorder(im_core,B_SIZE,B_SIZE,B_SIZE,B_SIZE,cv2.BORDER_CONSTANT,value=RED)
        im[t_s:b_s,l_s:r_s] = im_piece
        #print("2",inx)
    elif type == 1:
        im_core = im[t:b,l:r,:]
        im_piece = cv2.copyMakeBorder(im_core,B_SIZE,B_SIZE,B_SIZE,B_SIZE,cv2.BORDER_CONSTANT,value=BLUE)
        im[t_s:b_s,l_s:r_s,:] = im_piece
        #print("1",inx)
    else:
        """site = img_list_info_chg[current]["site"]
        im_tar_name = img_list[current][0].split("/")[1]
        print(im_tar_name)
        im_org_name = [i["org_name"] for i in img_list_info_chg if i["target_name"] == im_tar_name]
        print(join("../data",site,im_org_name[0]))
        im_org = cv2.imread(join("../data",site,im_org_name[0]), cv2.IMREAD_UNCHANGED)"""
        im_org = ims_org[current]
        im_piece = im_org[t_s:b_s,l_s:r_s,:]
        if b_s > im_org.shape[0]:
            zos = np.zeros((W[0]-im_piece.shape[0],im_piece.shape[1],4))
            im_piece = np.vstack((im_piece,zos))
        if r_s > im_org.shape[1]:
            zos = np.zeros((im_piece.shape[0],W[1]-im_piece.shape[1],4))
            im_piece = np.hstack((im_piece,zos))        
        im[t_s:b_s,l_s:r_s,:] = im_piece[:,:,0:3]
    cv2.imwrite(img_list[current][0],im)
    
def left_click_hum_chg(event):
    y = event.x
    x = event.y
    print("x,y",x,y)
    slice_pos, label_pos = comp_positions(x, y)
    _=[ i for i in range(len(img_list_info)) if img_list_info[i]["target_name"] == img_list_info_chg[current]["target_name"]][0]
    print("left click img_list_info:",label_pos,img_list_info[_]["target_name"])
    print("left click img_list_info_chg:",label_pos,img_list_info_chg[current]["target_name"])
    img_list_info[_]["img_labels"][label_pos] = 0
    img_list_info_chg[current]["img_labels"][label_pos] = 0
    im_shape = img_list_info_chg[current]["img_shape"]
    if im_shape[0]*W[0] > W_THD or im_shape[1]*W[1] > H_THD:
        chg_slice_content(x*S_THD1, y*S_THD1, 0)
    else:
        chg_slice_content(x, y, 0)
    im = img_list[current][0]
    image = Image.open(im)
    w,h = image.size
    if w>W_THD or h>H_THD:
        image = image.resize((int(w/S_THD1), int(h/S_THD1)), Image.ANTIALIAS)
    im_pi = ImageTk.PhotoImage(image)
    #im_pi = ImageTk.PhotoImage(file=im)
    label1.configure(image=im_pi)
    label1.image=im_pi
    
def right_click_nat_chg(event):
    y = event.x
    x = event.y
    print("x,y",x,y)
    slice_pos, label_pos = comp_positions(x, y)
    _ = [ i for i in range(len(img_list_info)) if img_list_info[i]["target_name"] == img_list_info_chg[current]["target_name"]][0]
    print("_:",_)
    print("len of img_list_info[_]:",len(img_list_info[_]["img_labels"]))
    print("right click img_list_info:",label_pos,img_list_info[_]["target_name"])
    print("right click img_list_info_chg:",label_pos,img_list_info_chg[current]["target_name"])
    img_list_info[_]["img_labels"][label_pos] = 1
    img_list_info_chg[current]["img_labels"][label_pos] = 1
    im_shape = img_list_info_chg[current]["img_shape"]
    if im_shape[0]*W[0] > W_THD or im_shape[1]*W[1] > H_THD:
        chg_slice_content(x*S_THD1, y*S_THD1, 1)
    else:
        chg_slice_content(x, y, 1)
    im = img_list[current][0]
    image = Image.open(im)
    w,h = image.size
    if w>W_THD or h>H_THD:
        image = image.resize((int(w/S_THD1), int(h/S_THD1)), Image.ANTIALIAS)
    im_pi = ImageTk.PhotoImage(image)
    #im_pi = ImageTk.PhotoImage(file=im)
    label1.configure(image=im_pi)
    label1.image=im_pi
    
def double_lclick_no_chg(event):
    y = event.x
    x = event.y
    print("x,y",x,y)
    slice_pos, label_pos = comp_positions(x, y)
    _=[ i for i in range(len(img_list_info)) if img_list_info[i]["target_name"] == img_list_info_chg[current]["target_name"]][0]
    print("double click img_list_info:",label_pos,img_list_info[_]["target_name"])
    print("double click img_list_info_chg:",label_pos,img_list_info_chg[current]["target_name"])
    img_list_info[_]["img_labels"][label_pos] = 2
    img_list_info_chg[current]["img_labels"][label_pos] = 2
    im_shape = img_list_info_chg[current]["img_shape"]
    if im_shape[0]*W[0] > W_THD or im_shape[1]*W[1] > H_THD:
        chg_slice_content(x*S_THD1, y*S_THD1, 2)
    else:
        chg_slice_content(x, y, 2)
    im = img_list[current][0]
    image = Image.open(im)
    w,h = image.size
    if w>W_THD or h>H_THD:
        image = image.resize((int(w/S_THD1), int(h/S_THD1)), Image.ANTIALIAS)
    im_pi = ImageTk.PhotoImage(image)
    #im_pi = ImageTk.PhotoImage(file=im)
    label1.configure(image=im_pi)
    label1.image=im_pi
    
def get_img_list():
    result_dir = 'results_chg'
    median_dir = 'figures_mine'
    #read median images
    f_meds = [f for f in listdir(median_dir) if isfile(join(median_dir,f)) and f.endswith('.png')]
    f_meds.sort()
    ims_meds = {}
    for f in f_meds:
        nim = f.split("_median",1)[0]
        #im = Image.open(join(median_dir,f))
        im = join(median_dir,f)
        ims_meds[nim] = im
    f_res = [f for f in listdir("results")];
    f_res.sort();
    #clear results_chg folder
    fs_chg = glob.glob("results_chg/*")
    for f in fs_chg:
        remove(f);
    for _,f in enumerate(f_res):
        try:
            #image = Image.open(join(result_dir,f))
            img_org = join("results",f)
            image = join(result_dir,f)
            image_med = ims_meds[f.split("$",1)[0]]
            #filter images with all no change
            r = [r for r in img_list_info if r["target_name"] == f][0]
            nums,counts = np.unique(r["img_labels"],return_counts=True)
            if (nums.shape[0] == 1 and nums[0] == 2):
                continue
            print(f,nums,counts)
            cv2.imwrite(image,cv2.imread(img_org,cv2.IMREAD_UNCHANGED))
            img_list_info_chg.append(r)
            img_list.append((image,image_med))
            site = r["site"]
            im_tar_name = r["target_name"]
            im_org_name = r["org_name"]
            fp = join("../data",site,im_org_name)
            ims_org.append(cv2.imread(fp, cv2.IMREAD_UNCHANGED))
        except Exception as ex:
            pass
        
def read_img_list_info():
    #fields:site/org_name/target_name/img_no/img_shape/img_labels
    with open("ims_list_info.pkl","rb") as f:
        img_list_info[:] = pickle.load(f)
    f.close()

def save_img_list_info(event):
    print("saving ims list info...")
    #fields:site/org_name/target_name/img_no/img_shape/img_labels
    with open("ims_list_info.pkl","wb") as f:
        pickle.dump(img_list_info,f)
    f.close()
    with open("ims_list_info_chg.pkl","wb") as f:
        pickle.dump(img_list_info_chg,f)
    f.close()

    #save changed images
    for _,f_im in enumerate([i[0] for i in img_list]):
        i_name = f_im.split("/")[1]
        im = cv2.imread(f_im, cv2.IMREAD_UNCHANGED)
        f_des = join("results",i_name)
        cv2.imwrite(f_des,im)
        
def viewer_func():
    #global label1,label2
    prev_img = ImageTk.PhotoImage(file="prev_arrow.png")
    next_img = ImageTk.PhotoImage(file="next_arrow.png")
    exit_img = ImageTk.PhotoImage(file="exit.png")
    b_prev = Button(frame, command=prev)
    b_prev.config(image=prev_img)
    b_prev.image = prev_img
    b_prev.grid(row=710)
    b_prev.pack()
    b_next = Button(frame,command=next)
    b_next.config(image=next_img)
    b_next.image = next_img
    b_next.grid(row=710, column=1)
    b_next.pack()
    b_exit = Button(frame,command=root.quit)
    b_exit.config(image=exit_img)
    b_exit.image=exit_img
    b_exit.grid(row=710, column=2)
    b_exit.pack()
    b_exit.bind("<Button-1>",save_img_list_info)
    
def prev():
    global current
    if not (0 <= current-1 < len(img_list)):
        messagebox.showinfo('End','No more image.')
        return
    current -= 1
    im_pi,im_med_pi = get_im_and_med()
    #print("prev",im,im_med)
    label1.configure(image=im_pi)
    label1.image=im_pi
    label2.configure(image=im_med_pi)
    label2.image=im_med_pi

def rescale_image(w,h):
    scal_w = NEW_SIZE[0]/w
    scal_h = NEW_SIZE[1]/h
    return scal_w,scal_h

def next():
    global current
    if not (0 <= current+1 < len(img_list)):
        messagebox.showinfo('End','No more image.')
        return
    current += 1
    im_pi,im_med_pi = get_im_and_med()
    label1.configure(image=im_pi)
    label1.image=im_pi
    label2.configure(image=im_med_pi)
    label2.image=im_med_pi

def get_im_and_med():
    im,im_med = img_list[current]
    image = Image.open(im)
    w,h = image.size
    if w>W_THD or h>H_THD:
        image = image.resize((int(w/S_THD1),int(h/S_THD1)), Image.ANTIALIAS)
        w = int(w/S_THD1)
        h = int(h/S_THD1)
    im_pi = ImageTk.PhotoImage(image)
    image_med = Image.open(im_med)
    w1,h1 = image_med.size
    if w1>W_THD or h1>H_THD:
        image_med = image_med.resize((int(w1/S_THD),int(h1/S_THD)), Image.ANTIALIAS)
        w1 = int(w/S_THD)
        h1 = int(h/S_THD)
    im_med_pi = ImageTk.PhotoImage(image_med)
    return im_pi, im_med_pi

def onFrameConfigure(event):
    canvas.configure(scrollregion=canvas.bbox("all"))
    
read_img_list_info()    
get_img_list()
#define a canvas and put a frame in the canvas
root = Tk()
root.title('image viewer')
canvas = Canvas(root, borderwidth=0,background = "#ffffff",width=600,height=600)
frame = Frame(canvas, background = "#ffffff")
vsb = Scrollbar(root,orient="vertical",command = canvas.yview)
canvas.configure(yscrollcommand = vsb.set)
vsb.pack(side="right",fill="y")
canvas.pack(side="left",fill="both",expand=True)
canvas.create_window((4,4),window=frame,anchor="nw",tags="frame")
frame.bind("<Configure>",onFrameConfigure)

l_top = Label(frame,text="left_click:hum change; right_click:nat change; double_left_click:no change")
l_top.pack()
im_pi,im_med_pi = get_im_and_med()
label1 = Label(frame, image=im_pi)
label1.pack()
label1.bind("<Button-1>", left_click_hum_chg)
label1.bind("<Button-2>", right_click_nat_chg)
label1.bind("<Double-1>", double_lclick_no_chg)
label2 = Label(frame, image=im_med_pi)
label2.pack()
viewer_func()
root.mainloop() # wait until user clicks the window
