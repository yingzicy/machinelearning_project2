import numpy as np
import matplotlib.pyplot as plt
import pickle

def show_performance(scores, accs):
    print(len(scores),len(accs))
    fig = plt.figure()
    plt.plot(scores)
    plt.xlabel('training step')
    plt.ylabel('loss value')
    fn = "figures_mine/scores.png"
    fig.savefig(fn)
    plt.close(fig)
    fig = plt.figure()
    plt.plot(accs)
    plt.xlabel('training step')
    plt.ylabel('accuracy')
    fn = "figures_mine/accuracy.png"
    fig.savefig(fn)
    plt.close(fig)

def read_features():
    scores = []
    accs = []
    f = open("scores.pkl","rb")
    scores = pickle.load(f)
    f.close()
    f = open("accuracy.pkl","rb")
    accs = pickle.load(f)
    f.close()
    return scores, accs

if __name__ == '__main__':
    scores, accs = read_features()
    show_performance(scores, accs)
