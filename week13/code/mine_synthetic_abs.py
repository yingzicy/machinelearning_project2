from six.moves import cPickle
import sys
from os import listdir,remove
from os.path import isfile,join
import numpy as np
import numpy.random as rng
import cv2
import glob
import pickle
import copy
from mine_images_median_diff_abs import comp_diff_val_distribution,comp_ims_median
#from mine_images_median_diff_abs import *
dist_features = []
ims_blend_hum = []
ims_blend_nat = []
ims_blend_nc = []
ims_blend_nat_diff = []
ims_blend_hum_diff = []
ims_blend_nc_diff = []
ims_blend_diff_pieces = []
dist_ims_feas = []
hum_chg_locs = []
dist_feas = []

W = (64,64)
shift = (10,10)
NC_THD = 5

#load images from cifar-10
def load_data_syn(fpath):
    """load orginal image like mine_mean, hum and nat images"""
    if fpath == "mine_syn" or fpath == "mine_syn_diff":
        mypath = "figures_mine/ims_median.tif"
    elif fpath == "bh_syn" or fpath == "bh_syn_diff":
        mypath = "figures_mine/bh_median.tif"
    else:
        mypath = "figures_mine/bg_median.tif"
    im_med= cv2.imread(mypath, cv2.IMREAD_UNCHANGED)
    fp = "hum_nat/add"
    add_ims = []
    fs = [f for f in listdir(fp) if isfile(join(fp,f)) and (f.endswith(".tif") or f.endswith(".png"))]
    for f in fs:
        fn = join(fp,f)
        add_ims.append(cv2.imread(fn, cv2.IMREAD_UNCHANGED))
    return im_med, add_ims

def write_loc_features(fpath, hum_chg_locs):
    #locs = {"nat":nat_chg_locs, "hum":hum_chg_locs}
    if fpath == "mine_syn" or fpath == "mine_syn_diff":
        f = open("features_mn.pkl","wb")
    elif fpath == "bh_syn" or fpath == "bh_syn_diff":
        f = open("features_bh.pkl","wb")
    else:
        f = open("features_bg.pkl","wb")
    #pickle.dump(locs,fn)
    pickle.dump(hum_chg_locs,f)
    f.close()

def read_features(fpath):
    hum_chg_loc = []
    if fpath == "mine_syn" or fpath == "mine_syn_diff":
        f = open("features_mn.pkl","rb")
        hum_chg_loc[:] = pickle.load(f)
    elif fpath == "bh_syn" or fpath == "bh_syn_diff":
        f = open("features_bh.pkl","rb")
        hum_chg_loc[:] = pickle.load(f)
    else:
        f = open("features_bg.pkl","rb")
        hum_chg_loc[:] = pickle.load(f)
    hum_chg_locs.append(hum_chg_loc)
    f.close()

def comp_median(ims_blend_nh):
    ims_median = np.nanmedian(ims_blend_nh, axis = 0)
    return ims_median
    
#generate natural change image for the first half images
def images_hum(fpath, ims,add_ims):
    rng = np.random
    mid = int(len(ims)/2)
    c = 0
    for im in ims:
        hum_loc_rng = []
        hum_chg_loc = []
        if fpath == "mine_syn" or fpath == "mine_syn_diff":
            for _ in range(30):
                i = rng.randint(im.shape[0])
                j = rng.randint(im.shape[1])
                if len(hum_loc_rng) == 0:
                    hum_loc_rng.append((i,j))
                flag = 0
                for t in hum_loc_rng:
                    if abs(i - t[0]) < 100 or abs(j - t[1]) < 100:
                        flag = 1
                        break
                if flag == 0:
                    hum_loc_rng.append((i,j))
        else:
            hum_loc_rng.append((0,0))
            hum_loc_rng.append((int(im.shape[0]/2),0))
            hum_loc_rng.append((0,int(im.shape[1]/2)))
            hum_loc_rng.append((int(im.shape[0]/2),int(im.shape[1]/2)))
        for _ in range(len(hum_loc_rng)):
            if fpath != "mine_syn" and fpath != "mine_syn_diff":
                r = rng.randint(3)
                if r <= 1:
                    continue
            t = hum_loc_rng[_][0]
            l = hum_loc_rng[_][1]
            inx = rng.randint(11)
            h = add_ims[inx].shape[0]
            w = add_ims[inx].shape[1]
            b = t + h
            r = l + w
            if b > im.shape[0] or r > im.shape[1]:
                continue
            #if 0 not in im[t:b,l:r,3]:
            for ii in range(add_ims[inx].shape[0]):
                for jj in range(add_ims[inx].shape[1]):
                    if add_ims[inx][ii,jj,3] != 0:
                        im[t+ii,l+jj,0:3] = add_ims[inx][ii,jj,0:3]
            loc = list(hum_loc_rng[_])
            loc.extend([h,w])
            loc = tuple(loc)
            hum_chg_loc.append(loc)
        c += 1
        ims_blend_hum.append(im)
        hum_chg_locs.append(hum_chg_loc)
    write_loc_features(fpath, hum_chg_locs)

#generate human change images for the other half images
def images_nat(ims,ncs):
    for im in ims:
        ims_blend_nat.append(im)
    for im in ncs:
        ims_blend_nc.append(im)

        
#blend the original image with cloud image
def images_blend(fpath, add_ims):
    if fpath == "mine_syn" or fpath == "mine_syn_diff":
        fp_nat = "hum_nat/nat_mine"
        fp_hum = "hum_nat/hum_mine"
        fp_nc = "hum_nat/nc_mine"
    elif fpath == "bh_syn" or fpath == "bh_syn_diff":
        fp_nat = "hum_nat/nat_bh"
        fp_hum = "hum_nat/hum_bh"
        fp_nc = "hum_nat/nc_bh"
    else:
        fp_nat = "hum_nat/nat_bg"
        fp_hum = "hum_nat/hum_bg"
        fp_nc = "hum_nat/nc_bg"
    fn_nat = [f for f in listdir(fp_nat) if isfile(join(fp_nat,f)) and (f.endswith('.tif') or f.endswith('.png'))]
    fn_hum = [f for f in listdir(fp_hum) if isfile(join(fp_hum,f)) and (f.endswith('.tif') or f.endswith('.png'))]
    fn_nc = [f for f in listdir(fp_nc) if isfile(join(fp_hum,f)) and (f.endswith('.tif') or f.endswith('.png'))]
    hums = []
    nats = []
    ncs = []
    for f in fn_nat:
        fp = join(fp_nat,f)
        imn= cv2.imread(fp, cv2.IMREAD_UNCHANGED)
        nats.append(imn)
    for f in fn_hum:
        fp = join(fp_hum,f)
        imh = cv2.imread(fp, cv2.IMREAD_UNCHANGED)
        hums.append(imh)
    for f in fn_nc:
        fp = join(fp_nc,f)
        imh = cv2.imread(fp, cv2.IMREAD_UNCHANGED)
        ncs.append(imh)
    images_nat(nats,ncs)
    images_hum(fpath, hums,add_ims)

def write_blend(fpath):
    fp = fpath
    fpp = fp + "/*"
    fs = glob.glob(fpp)
    for f in fs:
        remove(f)
    for _ in range(len(ims_blend_nat)):
        if _ < 10:
            fn = fp + "/ims_blend_nat0" + str(_) + ".tif"
        else:
            fn = fp + "/ims_blend_nat" + str(_) + ".tif"
        cv2.imwrite(fn,ims_blend_nat[_])
        
    for _ in range(len(ims_blend_nc)):
        if _ < 10:
            fn = fp + "/ims_blend_nc0" + str(_) + ".tif"
        else:
            fn = fp + "/ims_blend_nc" + str(_) + ".tif"
        cv2.imwrite(fn,ims_blend_nc[_])
        
    for _ in range(len(ims_blend_hum)):
        if _ < 10:
            fn = fp + "/ims_blend_hum0" + str(_) + ".tif"
        else:
            fn = fp + "/ims_blend_hum" + str(_) + ".tif"
        cv2.imwrite(fn,ims_blend_hum[_])
        
def read_blend(fpath = 'mine_syn'):
    fs = [f for f in listdir(fpath) if isfile(join(fpath,f)) and (f.endswith('.tif') or f.endswith('.png'))]
    if len(fs) == 0:
        im_mid,add_ims = load_data_syn(fpath)
        images_blend(fpath, add_ims)
        write_blend(fpath)
    else:
        for f in fs:
            fn = join(fpath,f)
            im = cv2.imread(fn, cv2.IMREAD_UNCHANGED).astype(np.float)
            #im = im + NC_THD   #add some value to no change pixels
            if f.startswith('ims_blend_nat'):
                ims_blend_nat.append(im)
            elif f.startswith('ims_blend_hum'):
                ims_blend_hum.append(im)
            else:
                ims_blend_nc.append(im)
            
def write_blend_diff_abs(fpath, ims_blend_diff_tmp):
    fp = fpath
    fpp =fpath + "/*"
    fs = glob.glob(fpp)
    for f in fs:
        remove(f)
    for i in range(len(ims_blend_diff_tmp)):
        if i < len(ims_blend_hum_diff):
            if i < 10:
                fn = fp + "/ims_blend_diff_hum0" + str(i) + ".tif"
            else:
                fn = fp + "/ims_blend_diff_hum" + str(i) + ".tif"
            cv2.imwrite(fn,ims_blend_diff_tmp[i])
        elif i < (len(ims_blend_hum_diff)+len(ims_blend_nat_diff)):
            _ = i-len(ims_blend_hum_diff)
            if _ < 10:
                fn = fp + "/ims_blend_diff_nat0" + str(_) + ".tif"
            else:
                fn = fp + "/ims_blend_diff_nat" + str(_) + ".tif"
            cv2.imwrite(fn,ims_blend_diff_tmp[i])
        else:
            _ = i - (len(ims_blend_hum_diff)+len(ims_blend_nat_diff))
            if _ < 10:
                fn = fp + "/ims_blend_diff_nc0" + str(_) + ".tif"
            else:
                fn = fp + "/ims_blend_diff_nc" + str(_) + ".tif"
            cv2.imwrite(fn,ims_blend_diff_tmp[i])
            
def comp_abs(x,y,z):
    if z != 0:
        return abs(x-y)
    else:
        return 0
    
def comp_norm(x,m,M):
    if z != 0:
        return (x-m)/(M-m)
    else:
        return 0

#normalize each difference image
def norm_img_diff(ims_blend_diff_hn):
    m = 300
    M = -300
    for im in ims_blend_diff_hn:
        alpha = np.tile(im[:,:,3].ravel().astype(np.object),3)
        rgb = im[:,:,0:3].ravel().astype(np.object)
        rgb[alpha==0] = None
        rgb = list(filter(None.__ne__,rgb))
        m_tmp= np.amin(rgb)
        M_tmp = np.amax(rgb)
        if m_tmp < m:
            m = m_tmp
        if M_tmp > M:
            M = M_tmp
    print(m,M)
    for im in ims_blend_diff_hn:
        """imc = copy.deepcopy(im)
        rbg = imc[:,:,0:3].ravel()
        alpha = np.tile(imc[:,:,3].ravel(),3)
        im[:,:,0:3] = np.array([comp_norm(rgb[i],m,M,alpha[i]) for i in range(alpha.shape[0])]).reshape(im.shape[0],im.shape[1],3)"""
        for i in range(im.shape[0]):
            for j in range(im.shape[1]):
                #for k in range(im.shape[2]):
                if im[i,j,3] != 0:
                    im[i,j,0:3] = (im[i,j,0:3] - m)/(M - m) * 255
                    
#compute the difference bewteen median and individual image for each image row
def images_difference_abs(fpath,ims_med):
    for im in ims_blend_nat:
        imc = copy.deepcopy(im)
        for i in range(imc.shape[0]):
            for j in range(imc.shape[1]):
                if imc[i,j,3] != 0:
                    imc[i,j,0:3] = np.abs(imc[i,j,0:3] - ims_med[i,j,0:3])
        ims_blend_nat_diff.append(imc)
    for im in ims_blend_hum:
        imc = copy.deepcopy(im)
        for i in range(imc.shape[0]):
            for j in range(imc.shape[1]):
                if imc[i,j,3] != 0:
                    imc[i,j,0:3] = np.abs(imc[i,j,0:3] - ims_med[i,j,0:3])
        ims_blend_hum_diff.append(imc)
    for im in ims_blend_nc:
        imc = copy.deepcopy(im)
        for i in range(imc.shape[0]):
            for j in range(imc.shape[1]):
                if imc[i,j,3] != 0:
                    imc[i,j,0:3] = np.abs(imc[i,j,0:3] - ims_med[i,j,0:3])
    ims_blend_nc_diff.append(imc)
    ims_blend_diff_tmp = list(ims_blend_hum_diff)
    ims_blend_diff_tmp.extend(ims_blend_nat_diff)
    ims_blend_diff_tmp.extend(ims_blend_nc_diff)
    norm_img_diff(ims_blend_diff_tmp)
    write_blend_diff_abs(fpath, ims_blend_diff_tmp)

def read_blend_med():
    ims_blend_tmp = ims_blend_hum
    ims_blend_tmp.extend(ims_blend_nat)
    ims_blend_tmp.extend(ims_blend_nc)
    ims_med = comp_ims_median(ims_blend_tmp)
    return ims_med

def read_difference_all(fpath):
    fs = [f for f in listdir(fpath) if isfile(join(fpath,f)) and (f.endswith('.tif') or f.endswith('.png'))]
    ims_blend_hum_diff_ = []
    ims_blend_nat_diff_ = []
    ims_blend_nc_diff_ = []
    for f in fs:
            fn = join(fpath,f)
            im = cv2.imread(fn, cv2.IMREAD_UNCHANGED)
            if f.startswith('ims_blend_diff_hum'):
                ims_blend_hum_diff_.append(im)
            elif f.startswith('ims_blend_diff_nat'):
                ims_blend_nat_diff_.append(im)
            else:
                ims_blend_nc_diff_.append(im)
    ims_blend_hum_diff.append(ims_blend_hum_diff_)
    ims_blend_nat_diff.append(ims_blend_nat_diff_)
    ims_blend_nc_diff.append(ims_blend_nc_diff_)
    read_features(fpath)
    ims_blend_hum[:] = []
    ims_blend_nat[:] = []
    ims_blend_nc[:] = []
    read_blend(fpath)
    ims_blend = list(ims_blend_hum)
    ims_blend.extend(ims_blend_nat)
    ims_blend.extend(ims_blend_nc)
    dist_feas.append(comp_diff_val_distribution(ims_blend))
                
def read_difference_abs(fpath):
    if fpath == "mine_syn_diff" or fpath == "bh_syn_diff" or fpath == "bg_syn_diff":
        if fpath == "mine_syn_diff":
            fp = "mine_syn"
            ims_med = cv2.imread("figures_mine/ims_median.tif",cv2.IMREAD_UNCHANGED)
            if ims_med.size == 0:
                ims_med = read_blend_med()
                cv2.imwrite("figures_mine/ims_median.tif",ims_med)
        elif fpath == "bh_syn_diff":
            fp = "bh_syn"
            ims_med = cv2.imread("figures_mine/bh_median.tif",cv2.IMREAD_UNCHANGED)
            if ims_med.size == 0:
                ims_med = read_blend_med()
                cv2.imwrite("figures_mine/bh_median.tif",ims_med)
        else:
            fp = "bg_syn"
            ims_med = cv2.imread("figures_mine/bg_median.tif",cv2.IMREAD_UNCHANGED)
            if ims_med.size == 0:
                ims_med = read_blend_med()
                cv2.imwrite("figures_mine/bg_median.tif",ims_med)
        read_blend(fp)
        images_difference_abs(fpath, ims_med)
        return
    else:
        read_difference_all("mine_syn_diff")
        read_difference_all("bh_syn_diff")
        read_difference_all("bg_syn_diff")
                
def images_diff_divide_sub(im_diff, indx, i, n1,n2,n3):
    diff_pieces = []
    org_pieces = []
    #ei_size = int(700/len(ims_blend_nat_diff))
    #print("ei_size:",ei_size)
    if i < len(ims_blend_hum_diff[indx]):
        for _ in range(len(hum_chg_locs[indx][i])):
            hc_h = hum_chg_locs[indx][i][_][2]+W[0]
            hc_w = hum_chg_locs[indx][i][_][3]+W[1]
            t = hum_chg_locs[indx][i][_][0]
            l = hum_chg_locs[indx][i][_][1]
            b = t + hum_chg_locs[indx][i][_][2]
            r = l + hum_chg_locs[indx][i][_][3]
            for i_ in range(t,b,shift[0]):
                for j_ in range(l,r,shift[1]):
                    w = W[0]
                    h = W[1]
                    b1 = i_ + h
                    r1 = j_ + w
                    if b1 > b or r1 >r:
                        continue
                    im_win = im_diff[i_:b1,j_:r1,0:3]
                    #if np.sum(im_win) > NC_THD * W[0] * W[1]*3:
                    n3 += 1
                    im_piece = (im_win,1)
                    ims_blend_diff_pieces.append(im_piece)
                    dist_ims_feas.append(dist_feas[indx][i])
    elif i < (len(ims_blend_hum_diff[indx])+len(ims_blend_nat_diff[indx])):
        for k1 in range(0,im_diff.shape[0],W[0]):
            for k2 in range(0,im_diff.shape[1],W[1]):
                b = k1 + W[0]
                r = k2 + W[1]
                if b > im_diff.shape[0] or r > im_diff.shape[1]:
                    continue
                im_win = im_diff[k1:b,k2:r,0:3]
                s = np.sum(im_win)
                """if s <= NC_THD * W[0] * W[1]*3 and s > 0:
                    n1 += 1
                    im_piece = (im_win,0)
                    ims_blend_diff_pieces.append(im_piece)
                    #dist_ims_feas.append([0,0,0,0])
                    dist_ims_feas.append([x for x in dist_feas[i]])"""
                if s >  NC_THD * W[0] * W[1]*3:
                    """if n2_ > ei_size:
                        continue"""
                    #if n2 > 400:
                     #   continue
                    n2 += 1
                    im_piece = (im_win,0)
                    ims_blend_diff_pieces.append(im_piece)
                    dist_ims_feas.append(dist_feas[indx][i])
    else:   
        for k1 in range(0,im_diff.shape[0],W[0]):
            for k2 in range(0,im_diff.shape[1],W[1]):
                b = k1 + W[0]
                r = k2 + W[1]
                if b > im_diff.shape[0] or r > im_diff.shape[1]:
                    continue
                im_win = im_diff[k1:b,k2:r,0:3]
                s = np.sum(im_win)
                """if s <= NC_THD * W[0] * W[1]*3:
                    if n1 > 400:
                        continue"""
                #if n1 > 300:
                #    continue
                n1 += 1
                im_piece = (im_win,0)
                ims_blend_diff_pieces.append(im_piece)
                #dist_ims_feas.append([0,0,0,0])
                dist_ims_feas.append([x for x in dist_feas[indx][i]])
    return n1, n2, n3

#break the each difference image into pieces with equal size and label them
def images_diff_divide_label():
    n1 = 0
    n2 = 0
    n3 = 0
    rng = np.random
    #print(len(ims_blend_hum_diff),len(ims_blend_nat_diff),len(ims_blend_nc_diff))
    for _ in range(len(ims_blend_hum_diff)):     
        ims_blend_diff = list(ims_blend_hum_diff[_])
        #print(len(ims_blend_hum_diff[_]))
        ims_blend_diff.extend(ims_blend_nat_diff[_])
        #print(len(ims_blend_nat_diff[_]))
        ims_blend_diff.extend(ims_blend_nc_diff[_])
        #print(len(ims_blend_nc_diff[_]))
        for i in range(len(ims_blend_diff)):
            im_diff = copy.deepcopy(ims_blend_diff[i])
            n1, n2, n3 = images_diff_divide_sub(im_diff,_,i,n1,n2,n3)
    print("class0/1/2(no change/nat change/hum change):",n1,n2,n3)
    
#write all difference pieces to the disk
def images_diff_pieces_write():
    #remove history files from the folder first
    ps = glob.glob("mine_pieces/*.tif")
    for f in ps:
        remove(f)
        
    for i in range(len(ims_blend_diff_pieces)):
        fn = 'mine_pieces/image' + str(i) + '.tif'
        cv2.imwrite(fn,ims_blend_diff_pieces[i][0])

def release_list():
    del ims_blend_hum[:]
    del ims_blend_nat[:]
    del ims_blend_nat_diff[:]
    del ims_blend_hum_diff[:]
    
def run_mine_syn():
    if sys.argv[1] == "mine_syn" or sys.argv[1] == "bh_syn" or sys.argv[1] == "bg_syn":
        read_blend(sys.argv[1])
    #write_loc_features()
    elif sys.argv[1] == "mine_syn_diff" or sys.argv[1] == "bh_syn_diff" or sys.argv[1] == "bg_syn_diff":
        img_med,add_ims = load_data_syn(sys.argv[1])
        read_difference_abs(sys.argv[1])
    else:
        img_med,add_ims = load_data_syn(sys.argv[1])
        read_difference_abs(sys.argv[1])
        images_diff_divide_label()
        #images_diff_pieces_write()
    release_list()
    
if __name__ == '__main__':
    run_mine_syn()         




