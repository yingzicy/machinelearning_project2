from ConNN_2L import *
from timeit import default_timer as timer
from sklearn import preprocessing,datasets
from sklearn.metrics import precision_recall_fscore_support, accuracy_score,confusion_matrix

#set the parameters
"""train_steps = 4500 
alpha = 0.65 #learning rate
r_rate = 0.00001 #regularization strength
filter_size = 1"""

#set the parameters filter_size = 10
train_steps = 700
ALP_INI = 0.0002 #learning rate
r_rate = 0.00000000001 #regularization strength
S = [(1,1),(2,2)] #stride for filtering
P = ['valid','valid'] #padding size
filter_size = [4, 4]
n_hidden = 600
image_size = 64
n_class = 3
pool_size = (2,2)
filter = [(5,5),(4,4)]
mlp_extra_feas = 4

def ometrics(y_true, y_pred):     
    cm = confusion_matrix(y_true, y_pred)
    cm1 = [[x for x in reversed(r)] for r in cm]
    for c in reversed(cm1):
        print(c)
    precision, recall, f_measure, support= precision_recall_fscore_support(y_true, y_pred)
    accuracy  = accuracy_score(y_true, y_pred)
    print("pecision:", ' '.join(['%.3f ' %(k) for k in precision]))
    print("recall:", ' '.join(['%.3f ' %(k) for k in recall]))
    print("f_measure:", ' '.join(['%.3f ' %(k) for k in f_measure]))
    print("accuracy: %.3f" %(accuracy,))
    
def comp_cnn_shape():
    image_shape1 = []
    f_shape = []
    ww = []
    for _ in range(len(filter_size)+1):
        if _ == 0:
            f_shape.append((filter_size[0],3,filter[_][0],filter[_][1]))
            image_shape1.append((3,image_size,image_size))
        else:
            w = image_shape1[_-1][1]
            f = f_shape[_-1][2]
            if P[_-1] == 'valid':
                p = 0
            s = S[_-1][0]
            if (w-f+2*p)%s != 0:
                print("the size of filter input is not divisable by stride")
            w1 = (w-f+2*p)/s + 1
            if w1%pool_size[0] != 0:
                print("the size of pooling input is not divisable by pool size")
            w1 = np.int(w1/pool_size[0])
            ww.append(w1)
            image_shape1.append((filter_size[_-1],w1,w1))
            if _ < len(filter_size):
                f_shape.append((filter_size[_],filter_size[_-1],filter[_][0],filter[_][1]))
    n_in = image_shape1[-1][0]*image_shape1[-1][1]*image_shape1[-1][2]
    return ww,n_in

def remove_alpha_blocks(org_x, x,dist_x,y):
    tx = x
    dist_tx = dist_x
    tyy = []
    nz_indes = []
    nz_tx = []
    nz_dist_tx = []
    for k in range(tx.shape[0]):
        if (0 not in org_x[k][:,:,3]): #no need to train, they are border blocks or alpha = 0
            nz_indes.append(k)
            nz_tx.append(tx[k])
            nz_dist_tx.append(dist_tx[k])
            if len(y) != 0:
                tyy.append(y[k])
    if len(nz_tx) == 0:
        return
    txx = np.array(nz_tx).transpose(0,-1,1,2).astype(theano.config.floatX)
    dxx = np.array(nz_dist_tx).astype(theano.config.floatX)
    #print(txx.shape,dxx.shape)
    return txx, dxx, tyy, nz_indes

def run_cnn(train_x, train_dist_x, train_y, valid_x, valid_dist_x, valid_y, test_x, test_dist_x, m_params):
    rng = np.random
    #compute the number of the units for each layer
    ww, n_in = comp_cnn_shape()
    #define theano variables
    x = T.tensor4(name = 'x', dtype = theano.config.floatX)
    y = T.ivector(name = 'y')
    x_mlp = T.matrix(name = 'x_mlp', dtype = theano.config.floatX)
    alp = T.fscalar(name = 'alp')
    #define cnn
    #print("image_shape,filter_shape,n_in:")
    image_shape = [(x.shape[0],3,image_size,image_size),(x.shape[0],filter_size[0],ww[0])]
    filter_shape = [(filter_size[0],3,filter[0][0],filter[0][1]), (filter_size[1],filter_size[0],filter[1][0],filter[1][1])]
    #print(image_shape,filter_shape,n_in)
    n_in = n_in
    
    cnn = ConNN(input = x,
                y = y,
                image_shape = image_shape,
                filter_shape = filter_shape,
                S = S,
                P = P,
                n_in = n_in,
                n_hidden = n_hidden,
                n_out = n_class,
                rng = rng
                )
    #loss function
    cost = cnn.neg_log_likelihood + r_rate* cnn.L2_reg
    params = cnn.params
    #gradient decent
    gparams = T.grad(cost,params)
    updates = [(param, param - alp * gparam) for param, gparam in zip(params, gparams)]

    train_func = theano.function(inputs = [x, y, alp],
                                 outputs = [cost,cnn.errors,cnn.y_pred],
                                 updates = updates
                                 )
    validation_func = theano.function(inputs = [x,y],
                                  outputs = [cnn.y_pred, cnn.errors]
                                  )
    predict_func = theano.function(inputs = [x],
                                  outputs = [cnn.y_pred,cnn.y_p]
                                  )
    #train and predict
    batch_size = 20
    m = train_x.shape[0]
    nb = int(m/batch_size)
    h_iter = int(train_steps/2)
    t_h = h_iter
    pow = 2
    n = 0
    alpha = np.float32(ALP_INI)
    y_pred_train = []
    scores = []
    errors = []
    y_preds = []
    y_v_errs = []
    y_err_min = 10.0
    y_preds_best = []
    if sys.argv[4] != '1':
        for i in range(train_steps):
            y_pred_train = []
            if i == t_h and n < 2:
                #alpha = alpha - np.float32(0.002)
                alpha = alpha/2
                h_iter = h_iter/pow
                t_h = t_h + h_iter
                n += 1
            #alpha = alpha - np.float32(0.0001)
            sum = 0
            sum_err = 0
            for j in range(nb):
                c,err,y_pred_b= train_func(train_x[j*batch_size:(j+1)*batch_size],train_y[j*batch_size:(j+1)*batch_size], alpha)
                sum += c
                sum_err += err
                y_pred_train.extend(y_pred_b)
            sum /= nb
            sum_err /= nb       
            print("%.5f,%.5f"%(sum,sum_err))
            scores.append(sum)
            errors.append(sum_err)
            #valid change
            y_preds[:] = []
            y_v_errs[:] = []
            #print(np.array(ims_syn_test_org_pieces).shape)
            _ = 0
            for img in ims_syn_test_org_pieces:
                y_pred = []
                x_shape = np.array(img).shape
                #print(x_shape)
                org_x = np.array(img).reshape(x_shape[0]*x_shape[1],x_shape[2],x_shape[3],x_shape[4])
                txx, dxx, tyy, nz_indes = remove_alpha_blocks(org_x, valid_x[_], valid_dist_x[_], valid_y[_])
                print(txx.shape,dxx.shape)
                y_pred_v,y_err_v = validation_func(txx, tyy)
                y_pred[:] = [2 for i in range(valid_x[_].shape[0])]
                for inx, k in enumerate(nz_indes):
                    y_pred[k] = y_pred_v[inx]
                y_v_errs.append(y_err_v)
                    #print("%.5f"%(y_err_v))
                y_preds.append(y_pred)
                _ += 1
            print("valid errors:",' '.join(['%.5f ' %(k) for k in y_v_errs]))
            #for e in y_v_errs:
            #    print("%.5f"%(e))
            y_err_avg = np.mean(np.array(y_v_errs))
            #print("average errors:%.5f"%(y_err_avg))
            #if y_v_errs[0] < 0.1 and y_err_avg < 0.1 and sum_err < 0.05:
            #    break
            #if y_err_avg < y_err_min and sum_err < 0.1:
            if y_err_avg < y_err_min:
                y_err_min = y_err_avg
                y_preds_best[:] = y_preds
                print("y_err_min",y_err_min)
                r_params = ([cnn.cnlayer1.W.get_value(), cnn.cnlayer2.W.get_value(), 
                             cnn.mlp.hidlayer.W.get_value(), cnn.mlp.lrlayer.W.get_value()],
                            [cnn.cnlayer1.b.get_value(), cnn.cnlayer2.b.get_value(),
                             cnn.mlp.hidlayer.b.get_value(), cnn.mlp.lrlayer.b.get_value()])
            
            #print the confusion matrix for training procudure
        print("the confusion matrix of traning results for sythetic images is: \n")
        last = len(y_pred_train)
        ometrics(train_y[0:last], y_pred_train)
        print("the confusion matrix of testing data for sythetic images is: \n")
        """if len(y_preds_best) == 0:
            y_preds_best = y_preds"""
        for _ in range(len(y_preds_best)):
            ometrics(valid_y[_], y_preds_best[_])
        #print(len(valid_y),np.array(ims_syn_test_org_pieces).shape)
        show_visualized_results(y_preds_best, ims_syn_test_org_pieces, 0, len(y_preds_best), 'results_syn')
    else:
        (W1,b1) = m_params
        W11, W12, W21, W22 = W1
        b11, b12, b21, b22 = b1
        cnn.cnlayer1.W.set_value(W11)
        cnn.cnlayer1.b.set_value(b11)
        cnn.cnlayer2.W.set_value(W12)
        cnn.cnlayer2.b.set_value(b12)
        cnn.mlp.hidlayer.W.set_value(W21)
        cnn.mlp.hidlayer.b.set_value(b21)
        cnn.mlp.lrlayer.W.set_value(W22)
        cnn.mlp.lrlayer.b.set_value(b22)

    #testing on multiple difference images
    y_preds = []
    y_probs = []
    if sys.argv[4] != '1':
        (W1,b1) = r_params
        W11, W12, W21, W22 = W1
        b11, b12, b21, b22 = b1
        cnn.cnlayer1.W.set_value(W11)
        cnn.cnlayer1.b.set_value(b11)
        cnn.cnlayer2.W.set_value(W12)
        cnn.cnlayer2.b.set_value(b12)
        cnn.mlp.hidlayer.W.set_value(W21)
        cnn.mlp.hidlayer.b.set_value(b21)
        cnn.mlp.lrlayer.W.set_value(W22)
        cnn.mlp.lrlayer.b.set_value(b22)
    print(np.array(mine_org_pieces[ISIZE_BEGIN:ISIZE_END]).shape)
    _ = 0
    for img in  mine_org_pieces[ISIZE_BEGIN:ISIZE_END]:
        y_pred = []
        x_shape = np.array(img).shape
        #print(x_shape)
        org_x = np.array(img).reshape(x_shape[0]*x_shape[1],x_shape[2],x_shape[3],x_shape[4])
        txx, dxx, tyy, nz_indes = remove_alpha_blocks(org_x, test_x[_], test_dist_x[_], [])
        print(txx.shape,dxx.shape)
        y_pred_1, y_p_1 = predict_func(txx)
        y_pred[:] = [2 for i in range(test_x[_].shape[0])]
        for inx, k in enumerate(nz_indes):
            y_pred[k] = y_pred_1[inx]
            inx += 1
        y_preds.append(y_pred)
        y_probs.append(y_p_1)
        _ += 1
    f = open("prob.txt","w")
    for probs in y_probs[-1]:
        f.write(' '.join(["%.5f"%(x) for x in probs])+'\n')
    if sys.argv[4] != '1':
        return y_preds,r_params,scores,errors
    else:
        return y_preds,[],[],[]

def show_performance(scores, errors):
    print(len(scores),len(errors))
    accs = [1-x for x in errors]
    fig = plt.figure()
    plt.plot(scores)
    plt.xlabel('training step')
    plt.ylabel('loss value')
    fn = "figures_mine/scores.png"
    fig.savefig(fn)
    plt.close(fig)
    fig = plt.figure()
    plt.plot(accs)
    plt.xlabel('training step')
    plt.ylabel('accuracy')
    fn = "figures_mine/accuracy.png"
    fig.savefig(fn)
    plt.close(fig)
    
def save_results(scores, errors):
    accs = [1-x for x in errors]
    f = open("scores.pkl", "wb")
    pickle.dump(scores,f)
    f.close()
    f = open("accuracy.pkl", "wb")
    pickle.dump(accs,f)
    f.close()
    
if __name__ == '__main__':
    #start time
    start = timer()

    #load data
    datasets, X_dist, valid_dist_X, test_dist_X = load_mine_data()
    #train dataset
    X,Y = datasets[0]
    Y = Y.astype(np.int32)
    train_dist_X = X_dist.astype(theano.config.floatX)
    train_X = X.astype(theano.config.floatX)
    train_Y = Y
    
    #test data
    test_X = datasets[1]
    for i in range(len(test_dist_X)):
        test_dist_X[i][:] = test_dist_X[i].astype(theano.config.floatX)
        
    #valid data
    valid_X,valid_Y = datasets[2]
    for i in range(len(valid_dist_X)):
        valid_dist_X[i][:] = valid_dist_X[i].astype(theano.config.floatX)
    
    #reshape
    train_X = train_X.transpose(0,-1, 1, 2).astype(theano.config.floatX)
    
    print("train data:",train_X.shape)
    print("valid data:", valid_X.shape)
    print("test data:", test_X.shape)
    print("train dist feas:",train_dist_X.shape)
    #print("valid dist feas:",valid_dist_X.shape)
    #load params
    m_params = []
    if sys.argv[4] == '1':
        m_params = load_params()
    
    y_preds,m_params,scores, errors = run_cnn(train_X, train_dist_X, train_Y, valid_X, valid_dist_X, valid_Y, test_X, test_dist_X, m_params)
    #y_preds = np.array(y_preds,np.int32)

    #save the results
    show_visualized_results(y_preds)
    if sys.argv[4] != '1':
        save_params(m_params)
        save_results(scores,errors)
    #show_performance(scores, errors)
    #end time
    end = timer()
    print("the elapsed time is: %.2f s" %(end - start,))



    
    
